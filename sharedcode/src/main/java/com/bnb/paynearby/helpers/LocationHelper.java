package com.bnb.paynearby.helpers;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.sharedcode.helpers.LogHelper;
import pl.tajchert.nammu.Nammu;

import java.util.UUID;

public class LocationHelper implements ConnectionCallbacks,
        OnConnectionFailedListener, LocationListener {

    private static String REQUEST_IDENTIFIER = "";
    private static int TIME_OUT = 10 * 1000;
    Activity activity;
    Handler timeOutHandler;
    private GoogleApiClient mGoogleApiClient;
    private OnGetLocationListener mOnGetLocationListener = null;
    private LocationRequest mLocationRequest;
    private String requestIdentifier = "";

    public LocationHelper(Activity activity) {
        this.activity = activity;
        mGoogleApiClient = new GoogleApiClient.Builder(activity)
                .addApi(LocationServices.API).addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setInterval(10 * 1000);
        mLocationRequest
                .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        REQUEST_IDENTIFIER = requestIdentifier = UUID.randomUUID().toString();
    }

    /**
     * Returns last known cached location. Returns null if no known location
     * available
     **/
    public static Location getLastKnownLocation(Context context) {

        LocationManager locationManager = (LocationManager) context
                .getSystemService(Context.LOCATION_SERVICE);
        String networkProvider = LocationManager.NETWORK_PROVIDER;
        try {
            Location networkLocation = locationManager
                    .getLastKnownLocation(networkProvider);
            String gpsProvider = LocationManager.GPS_PROVIDER;
            Location gpsLocation = locationManager
                    .getLastKnownLocation(gpsProvider);
            String passiveProvider = LocationManager.PASSIVE_PROVIDER;
            Location passiveLocation = locationManager
                    .getLastKnownLocation(passiveProvider);

            // estimate last known location
            long passiveTime = (passiveLocation == null) ? 0 : passiveLocation
                    .getTime();
            long gpsTime = (gpsLocation == null) ? 0 : gpsLocation.getTime();
            long networkTime = (networkLocation == null) ? 0 : networkLocation
                    .getTime();

            return (passiveTime > gpsTime && passiveTime > networkTime) ? passiveLocation
                    : ((networkTime > gpsTime) ? networkLocation : gpsLocation);
        } catch (SecurityException e) {
            String message = e.getMessage() != null ? e.getMessage() : "";
            LogHelper.e(LocationHelper.class.getName(), message + "Location Permission not granted");
            return null;
        }
    }

    private void setOnGetLocationListener(
            OnGetLocationListener onGetLocationListener) {
        mOnGetLocationListener = onGetLocationListener;
    }

    private boolean isLatestRequest() {
        return REQUEST_IDENTIFIER.equalsIgnoreCase(requestIdentifier);
    }

    private void returnCallback(Location location) {
        synchronized (REQUEST_IDENTIFIER) {
            mGoogleApiClient.disconnect();
            if (timeOutHandler != null)
                timeOutHandler.removeMessages(0);
            if (isLatestRequest() && mOnGetLocationListener != null)
                mOnGetLocationListener.onGetLocation(location);

        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        LogHelper.v("LocationHelper", "connection failed" + "");
        returnCallback(null);

    }

    @Override
    public void onConnected(Bundle connectionHint) {
        if (Nammu.checkPermission(Manifest.permission.ACCESS_FINE_LOCATION)) {
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest, this); // LocationListener
        } else {
            Nammu.askForPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION, null);
        }

    }

    @Override
    public void onConnectionSuspended(int cause) {
        // TODO Auto-generated method stub
        returnCallback(null);
        LogHelper.v("LocationHelper", "connection suspended");

    }

    @Override
    public void onLocationChanged(Location location) {
        LogHelper.v("LocationHelper", "location is null? " + (location == null)
                + "");
        returnCallback(location);
    }

    public void getLocation(OnGetLocationListener onGetLocationListener) {
        setOnGetLocationListener(onGetLocationListener);
        if (onGetLocationListener != null)
            onGetLocationListener.onPreExecute();
        mGoogleApiClient.connect();
        timeOutHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                returnCallback(null);
            }
        };
        timeOutHandler.sendEmptyMessageDelayed(0, TIME_OUT);

    }

    public interface OnGetLocationListener {
        void onGetLocation(Location location);

        void onPreExecute();
    }
}