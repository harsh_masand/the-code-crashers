package com.sharedcode.imagemap;

import android.graphics.Bitmap;
import android.util.Base64;
import com.sharedcode.R;

import java.io.ByteArrayOutputStream;

/**
 * Created by admin on 13/12/16.
 */
public class FingerConstants {
    final public static String RIGHT_THUMB = "6";
    final public static String RIGHT_INDEX = "7";
    final public static String RIGHT_MIDDLE = "8";
    final public static String RIGHT_RING = "9";
    final public static String RIGHT_LITTLE = "10";
    final public static String LEFT_THUMB = "5";
    final public static String LEFT_INDEX = "4";
    final public static String LEFT_MIDDLE = "3";
    final public static String LEFT_RING = "2";
    final public static String LEFT_LITTLE = "1";

    public static String getIndex(int id) {
        if (id == R.id.right_thumb) {
            return RIGHT_THUMB;
        } else if (id == R.id.right_index) {
            return RIGHT_INDEX;
        } else if (id == R.id.right_middle) {
            return RIGHT_MIDDLE;
        } else if (id == R.id.right_ring) {
            return RIGHT_RING;
        } else if (id == R.id.right_little) {
            return RIGHT_LITTLE;
        } else if (id == R.id.left_thumb) {
            return LEFT_THUMB;
        } else if (id == R.id.left_index) {
            return LEFT_INDEX;
        } else if (id == R.id.left_middle) {
            return LEFT_MIDDLE;
        } else if (id == R.id.left_ring) {
            return LEFT_RING;
        } else if (id == R.id.left_little) {
            return LEFT_LITTLE;
        } else {
            return null;
        }

    }

    public static String getFinderString(String fingerIndex) {

        switch (fingerIndex) {
            case RIGHT_THUMB:
                return "RIGHT_THUMB";
            case RIGHT_INDEX:
                return "RIGHT_INDEX";
            case RIGHT_MIDDLE:
                return "RIGHT_MIDDLE";
            case RIGHT_RING:
                return "RIGHT_RING";
            case RIGHT_LITTLE:
                return "RIGHT_LITTLE";
            case LEFT_THUMB:
                return "LEFT_THUMB";
            case LEFT_INDEX:
                return "LEFT_INDEX";
            case LEFT_MIDDLE:
                return "LEFT_MIDDLE";
            case LEFT_RING:
                return "LEFT_RING";
            case LEFT_LITTLE:
                return "LEFT_LITTLE";
            default:
                return "RIGHT_INDEX";
        }
    }

    public static String convertBitmapToBase64(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
        return encoded;
    }
}
