package com.sharedcode.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import androidx.viewpager.widget.ViewPager;

/**
 * Created by admin on 4/21/17.
 */

public class CustomViewPagerNoScroll extends ViewPager {

    private boolean mEnabled;

    public CustomViewPagerNoScroll(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mEnabled = false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (this.mEnabled) {
            return super.onTouchEvent(event);
        }

        return false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        if (this.mEnabled) {
            return super.onInterceptTouchEvent(event);
        }

        return false;
    }

    public void setPagingEnabled(boolean enabled) {
        this.mEnabled = enabled;
    }
}
