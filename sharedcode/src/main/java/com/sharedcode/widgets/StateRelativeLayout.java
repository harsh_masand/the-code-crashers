package com.sharedcode.widgets;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.IntDef;
import com.sharedcode.R;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class StateRelativeLayout extends RelativeLayout {

    public static final int NO_INTERNET_CONNECTION = 0;
    public static final int SOMETHING_WENT_WRONG = 1;
    public static final int NO_CONTENT = 2;
    public static final int LOADING = 3;
    protected ImageView mImageView;
    protected TextView mTitle;
    protected TextView mMessage;
    protected Button mRetryButton;
    private Context mContext;
    @StateType
    private int mStateType = -1;

    public StateRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View mView = inflater.inflate(R.layout.widget_state_relative_layout, this, true);
        mImageView = (ImageView) mView.findViewById(R.id.state_image_view);
        mTitle = (TextView) mView.findViewById(R.id.error_title);
        mMessage = (TextView) mView.findViewById(R.id.error_message);
        mRetryButton = (Button) mView.findViewById(R.id.button_retry);
    }

    public StateRelativeLayout(Context context) {
        super(context, null);
        mContext = context;
    }

    public static String getStateName(@StateType int stateType) {
        switch (stateType) {
            case NO_INTERNET_CONNECTION:
                return String.valueOf(R.string.fragment_error_page_title_no_internet_connection);
            case SOMETHING_WENT_WRONG:
                return String.valueOf(R.string.fragment_error_page_title_something_went_wrong);
            case NO_CONTENT:
                return String.valueOf(R.string.fragment_error_page_title_no_content);
            default:
                return String.valueOf(R.string.no_case_found);
        }
    }

    public Button getRetryButton() {
        return mRetryButton;
    }

    @SuppressLint("WrongConstant")
    public void displayStateView(@StateType int stateType) {
        switch (stateType) {
            case NO_INTERNET_CONNECTION:
                setStateValues(R.drawable.ic_red_close
                        , R.string.fragment_error_page_title_no_internet_connection
                        , R.string.fragment_error_page_title_no_internet_connection
                        , 0);
                break;
            case SOMETHING_WENT_WRONG:
                setStateValues(R.drawable.ic_red_close
                        , R.string.fragment_error_page_title_something_went_wrong
                        , R.string.fragment_error_page_title_something_went_wrong
                        , R.string.fragment_error_page_button_retry);
                break;
            case NO_CONTENT:
                setStateValues(0
                        , 0
                        , 0
                        , 0);
                break;

            case LOADING:
                setStateValues(R.drawable.ic_red_close
                        , R.string.fragment_loading_page_title
                        , 0
                        , 0);
                break;
        }
        this.mStateType = stateType;
    }

    private void setStateValues(int stateImageResourceId, int stateHeadingStringResourceId, int stateSubHeadingStringResourceId, int stateButtonRetryStringResourceId) {
        mImageView.setImageResource(stateImageResourceId);
        if (stateHeadingStringResourceId != 0) {
            mTitle.setVisibility(View.VISIBLE);
            mTitle.setText(stateHeadingStringResourceId);
        } else {
            mTitle.setVisibility(View.GONE);
        }

        if (stateButtonRetryStringResourceId != 0) {
            mRetryButton.setVisibility(View.VISIBLE);
            mRetryButton.setText(stateButtonRetryStringResourceId);
        } else {
            mRetryButton.setVisibility(View.GONE);
        }

        if (stateSubHeadingStringResourceId != 0) {
            mMessage.setVisibility(View.VISIBLE);
            mMessage.setText(stateSubHeadingStringResourceId);
        } else {
            mMessage.setVisibility(View.GONE);
        }
    }

    public void setOnRetryButtonClickListener(OnClickListener clickListener) {
        mRetryButton.setOnClickListener(clickListener);
    }

    public void setRetryButtonVisibility(boolean visibility) {
        if (visibility)
            mRetryButton.setVisibility(VISIBLE);
        else
            mRetryButton.setVisibility(GONE);
    }

    @Retention(RetentionPolicy.CLASS)
    @IntDef({NO_INTERNET_CONNECTION
            , SOMETHING_WENT_WRONG
            , NO_CONTENT})

    public @interface StateType {
    }
}
