package com.sharedcode.widgets;

/**
 * Created by admin on 5/8/17.
 */

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.speech.RecognizerIntent;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import androidx.appcompat.content.res.AppCompatResources;
import com.sharedcode.R;
import com.sharedcode.baseclasses.BaseActivity;
import com.sharedcode.helpers.LogHelper;

import java.util.Locale;


/**
 * To clear icon can be changed via
 * <p>
 * <pre>
 * android:drawable(Right|Left)="@drawable/custom_icon"
 * </pre>
 */
public class ClearableEditText extends CustomEditText implements OnTouchListener, OnFocusChangeListener {

    public static final int REQ_CODE_SPEECH_INPUT = 111;
    int MAX_LEVEL = 10000;
    private boolean mShowVoiceSearch = false;
    private Location mLocation = Location.RIGHT;
    private Drawable mDrawable, mSearchDrawable;
    private Listener mListener;
    private OnTouchListener mOnTouchListener;
    private OnFocusChangeListener mOnFocusChangeListener;

    public ClearableEditText(Context context) {
        super(context);
        init(context, null);
    }

    public ClearableEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public ClearableEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }

    public void setListener(Listener listener) {
        this.mListener = listener;
    }

    /**
     * null disables the icon
     */
    public void setIconLocation(Location loc) {
        this.mLocation = loc;
    }

    @Override
    public void setOnTouchListener(OnTouchListener onTouchListener) {
        this.mOnTouchListener = onTouchListener;
    }

    @Override
    public void setOnFocusChangeListener(OnFocusChangeListener f) {
        this.mOnFocusChangeListener = f;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (getDisplayedDrawable() != null) {
            int x = (int) event.getX();
            int y = (int) event.getY();
            int left = (mLocation == Location.LEFT) ? 0 : getWidth() - getPaddingRight() - mDrawable.getIntrinsicWidth();
            int right = (mLocation == Location.LEFT) ? getPaddingLeft() + mDrawable.getIntrinsicWidth() : getWidth();
            boolean tappedX = x >= left && x <= right && y >= 0 && y <= (getBottom() - getTop());
            if (tappedX) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (mListener != null) {
                        mListener.didClearText();
                    } else {
                        Drawable[] cd = getCompoundDrawables();
                        if (cd[2] == mSearchDrawable) {
                            askSpeechInput();
                        } else {
                            setText("");
                            if (mShowVoiceSearch) {
                                setCompoundDrawables((mLocation == Location.LEFT) ? mSearchDrawable : cd[0], cd[1], (mLocation == Location.RIGHT) ? mSearchDrawable : cd[2],
                                        cd[3]);
                            } else {
                                setCompoundDrawables((mLocation == Location.LEFT) ? null : cd[0], cd[1], (mLocation == Location.RIGHT) ? null : cd[2],
                                        cd[3]);
                            }
                        }
                    }
                }
                return true;
            }
        }
        if (mOnTouchListener != null) {
            return mOnTouchListener.onTouch(v, event);
        }
        return false;
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (hasFocus) {
            setClearIconVisible(!TextUtils.isEmpty(getText()));
            if (TextUtils.isEmpty(getText())) {
                Drawable[] cd = getCompoundDrawables();
                if (mShowVoiceSearch) {
                    setCompoundDrawables((mLocation == Location.LEFT) ? mSearchDrawable : cd[0], cd[1], (mLocation == Location.RIGHT) ? mSearchDrawable : cd[2],
                            cd[3]);
                }
            }
        } else {
            setClearIconVisible(false);
            if (TextUtils.isEmpty(getText())) {
                Drawable[] cd = getCompoundDrawables();
                if (mShowVoiceSearch) {
                    setCompoundDrawables((mLocation == Location.LEFT) ? mSearchDrawable : cd[0], cd[1], (mLocation == Location.RIGHT) ? mSearchDrawable : cd[2],
                            cd[3]);
                }
            }
        }
        if (mOnFocusChangeListener != null) {
            mOnFocusChangeListener.onFocusChange(v, hasFocus);
        }
    }

    @Override
    public void setCompoundDrawables(Drawable left, Drawable top, Drawable right, Drawable bottom) {
        super.setCompoundDrawables(left, top, right, bottom);
    }

    private void init(Context context, AttributeSet attrs) {
        super.setOnTouchListener(this);
        super.setOnFocusChangeListener(this);
        setMaxLines(1);
        addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Drawable[] cd = getCompoundDrawables();
                if (TextUtils.isEmpty(getText())) {
                    if (mShowVoiceSearch) {
                        setCompoundDrawables((mLocation == Location.LEFT) ? mSearchDrawable : cd[0], cd[1], (mLocation == Location.RIGHT) ? mSearchDrawable : cd[2],
                                cd[3]);
                    } else {
                        setCompoundDrawables((mLocation == Location.LEFT) ? null : cd[0], cd[1], (mLocation == Location.RIGHT) ? null : cd[2],
                                cd[3]);
                    }
                } else {
                    setCompoundDrawables((mLocation == Location.LEFT) ? mDrawable : cd[0], cd[1], (mLocation == Location.RIGHT) ? mDrawable : cd[2],
                            cd[3]);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        initIcon(attrs);
        setClearIconVisible(false);

        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.VoiceSearch,
                0, 0);

        try {
            mShowVoiceSearch = a.getBoolean(R.styleable.VoiceSearch_showVoiceSearch, false);
        } finally {
            a.recycle();
        }

        if (mShowVoiceSearch) {
            initVoiceIcon(attrs);
            Drawable[] cd = getCompoundDrawables();
            setCompoundDrawables((mLocation == Location.LEFT) ? mSearchDrawable : cd[0], cd[1], (mLocation == Location.RIGHT) ? mSearchDrawable : cd[2],
                    cd[3]);

            ObjectAnimator anim = ObjectAnimator.ofInt(mSearchDrawable, "alpha", 0, 255)
                    .setDuration(1000);
            anim.setRepeatMode(ValueAnimator.REVERSE);
            anim.setRepeatCount(5);
            anim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    mSearchDrawable.setAlpha(255);
                }
            });
            anim.start();
        } else {

        }
    }

    private void initIcon(AttributeSet attrs) {
        mDrawable = null;
        if (mLocation != null) {
            mDrawable = getCompoundDrawables()[mLocation.idx];
        }
        if (mDrawable == null) {

            TypedArray a = getContext().getTheme().obtainStyledAttributes(
                    attrs,
                    R.styleable.VoiceSearch,
                    0, 0);

            try {
                mDrawable = a.getDrawable(R.styleable.VoiceSearch_clearTextDrawable);
            } finally {
                a.recycle();
            }

            if (mDrawable == null) {
                mDrawable = AppCompatResources.getDrawable(getContext(), R.drawable.ic_cancel_small);
            }
        }
        mDrawable.setBounds(0, 0, mDrawable.getIntrinsicWidth(), mDrawable.getIntrinsicHeight());
        int min = getPaddingTop() + mDrawable.getIntrinsicHeight() + getPaddingBottom();
        if (getSuggestedMinimumHeight() < min) {
            setMinimumHeight(min);
        }
    }

    private void initVoiceIcon(AttributeSet attrs) {
        mSearchDrawable = null;
        if (mLocation != null) {
            mSearchDrawable = getCompoundDrawables()[mLocation.idx];
        }
        if (mSearchDrawable == null) {

            TypedArray a = getContext().getTheme().obtainStyledAttributes(
                    attrs,
                    R.styleable.VoiceSearch,
                    0, 0);

            try {
                mSearchDrawable = a.getDrawable(R.styleable.VoiceSearch_voiceDrawable);
            } finally {
                a.recycle();
            }

            if (mSearchDrawable == null) {
                mSearchDrawable = AppCompatResources.getDrawable(getContext(), R.drawable.ic_mic_grey_24dp);
            }
        }
        mSearchDrawable.setBounds(0, 0, mSearchDrawable.getIntrinsicWidth(), mSearchDrawable.getIntrinsicHeight());
        int min = getPaddingTop() + mSearchDrawable.getIntrinsicHeight() + getPaddingBottom();
        if (getSuggestedMinimumHeight() < min) {
            setMinimumHeight(min);
        }
    }

    private Drawable getDisplayedDrawable() {
        return (mLocation != null) ? getCompoundDrawables()[mLocation.idx] : null;
    }

    protected void setClearIconVisible(boolean visible) {
        Drawable[] cd = getCompoundDrawables();
        Drawable displayed = getDisplayedDrawable();
        boolean wasVisible = (displayed != null);
        if (visible != wasVisible) {
            Drawable x = visible ? mDrawable : null;
            setCompoundDrawables((mLocation == Location.LEFT) ? x : cd[0], cd[1], (mLocation == Location.RIGHT) ? x : cd[2],
                    cd[3]);
            super.setCompoundDrawables((mLocation == Location.LEFT) ? x : cd[0], cd[1], (mLocation == Location.RIGHT) ? x : cd[2],
                    cd[3]);
        }
    }

    public void askSpeechInput() {
        if (getContext() instanceof Activity) {
            Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                    RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
            intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                    getContext().getString(R.string.voice_text_hint));

            ((Activity) getContext()).startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } else {
            LogHelper.e(BaseActivity.class.getCanonicalName(), getContext().getString(R.string.baseactivity_not_set));
        }
    }

    public enum Location {
        LEFT(0), RIGHT(2);

        final int idx;

        Location(int idx) {
            this.idx = idx;
        }
    }

    public interface Listener {
        void didClearText();
    }
}
