package com.sharedcode.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TimePicker;

public class CustomTimePicker extends TimePicker {

    private static String DEBUG_TAG = "CustomTimePicker";

    public CustomTimePicker(Context context) {
        super(context);
    }

    public CustomTimePicker(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomTimePicker(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

}