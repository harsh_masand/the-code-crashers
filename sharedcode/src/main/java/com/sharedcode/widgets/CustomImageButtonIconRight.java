package com.sharedcode.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.sharedcode.R;
import com.sharedcode.utils.UIUtils;


public class CustomImageButtonIconRight extends LinearLayout {
    private ImageView mImageView;
    private CustomTextView mCustomTextView;

    public CustomImageButtonIconRight(Context context) {
        super(context);
    }

    public CustomImageButtonIconRight(Context context, AttributeSet attrs) {
        super(context, attrs);
        setView(context, attrs);
    }

    public CustomImageButtonIconRight(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setView(context, attrs);
    }

    private void setView(Context context, AttributeSet attributeSet) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.custom_image_button_icon_right_layout, this);
        mImageView = (ImageView) view.findViewById(R.id.image_view);
        mCustomTextView = (CustomTextView) view.findViewById(R.id.text_view);
        setDrawableImage(context, attributeSet);
        setTextAttrs(context, attributeSet);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setStateListAnimator(null);
        }
    }

    private void setDrawableImage(Context context, AttributeSet attrs) {
        Drawable drawable;
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs,
                R.styleable.CustomImageButton, 0, 0);
        try {
            drawable = a
                    .getDrawable(R.styleable.CustomImageButton_drawableImage);
        } finally {
            a.recycle();
        }

        if (drawable != null) {
            mImageView.setImageDrawable(drawable);
        }
    }

    private void setTextAttrs(Context context, AttributeSet attrs) {
        TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs,
                R.styleable.CustomImageButton, 0, 0);
        try {
            int textStyle = typedArray
                    .getInteger(R.styleable.CustomImageButton_text_weight, 0);
            mCustomTextView.setTypeface(UIUtils.getFont(getContext(), textStyle));

            int textCase = typedArray.getInteger(R.styleable.CustomImageButton_text_case, 0);
            setText(mCustomTextView.getText(), TextView.BufferType.SPANNABLE, textCase);

            int textColor = typedArray.getColor(
                    R.styleable.CustomImageButton_android_textColor, 0);
            mCustomTextView.setTextColor(textColor);

            float textSize = typedArray.getDimensionPixelSize(R.styleable.CustomImageButton_android_textSize, 15);
            mCustomTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);

            CharSequence charSequence = typedArray.getText(R.styleable.CustomImageButton_android_text);
            if (charSequence != null) {
                mCustomTextView.setText(charSequence);
            }

        } finally {
            typedArray.recycle();
        }
    }

    public void setText(CharSequence ch, TextView.BufferType type, int textCase) {
        switch (textCase) {
            case 0:
                mCustomTextView.setText(ch, type);
                break;
            case 1: {
                String s = ch.toString();
                s = UIUtils.toCamelCase(s);
                mCustomTextView.setText(s, type);
            }
            break;
            case 2: {
                String s = ch.toString();
                s = s.toLowerCase();
                mCustomTextView.setText(s, type);
            }
            break;
            case 3: {
                String s = ch.toString();
                s = s.toUpperCase();
                mCustomTextView.setText(s, type);
            }
            break;
            default:
                break;
        }
    }

    public void setText(String text) {
        mCustomTextView.setText(text);
    }

    public void setIcon(Drawable drawable) {
        mImageView.setImageDrawable(drawable);
    }
}