package com.sharedcode.widgets;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Helper class which loads and returns typeface
 **/
public class FontCache {
    private static Typeface robotoCondensedRegular, robotoCondensedBold, robotoRegular, robotoBold, robotoItalic, robotoLightItalic, robotoMedium;
    private static Typeface quicksandRegular, quicksandLight, quicksandBold, quicksandMedium;
    private static Typeface kohinoorBold;

    public static Typeface getRegularFont(Context context) {
        if (robotoCondensedRegular == null) {
            robotoCondensedRegular = Typeface.createFromAsset(context.getResources()
                    .getAssets(), "fonts/RobotoCondensed-Regular.ttf");
        }
        return robotoCondensedRegular;
    }

    public static Typeface getSemiBoldFont(Context context) {
        if (robotoCondensedBold == null) {
            robotoCondensedBold = Typeface.createFromAsset(context.getResources()
                    .getAssets(), "fonts/RobotoCondensed-Bold.ttf");
        }
        return robotoCondensedBold;
    }

    public static Typeface getRegularHeaderFont(Context context) {
        if (robotoRegular == null) {
            robotoRegular = Typeface.createFromAsset(context.getResources()
                    .getAssets(), "fonts/Roboto-Regular.ttf");
        }
        return robotoRegular;
    }

    public static Typeface getBoldHeaderFont(Context context) {
        if (robotoBold == null) {
            robotoBold = Typeface.createFromAsset(context.getResources()
                    .getAssets(), "fonts/Roboto-Bold.ttf");
        }
        return robotoBold;
    }

    public static Typeface getMediumHeaderFont(Context context) {
        if (robotoMedium == null) {
            robotoMedium = Typeface.createFromAsset(context.getResources()
                    .getAssets(), "fonts/Roboto-Medium.ttf");
        }
        return robotoMedium;
    }

    public static Typeface getLightItalicItalicFont(Context context) {
        if (robotoLightItalic == null) {
            robotoLightItalic = Typeface.createFromAsset(context.getResources()
                    .getAssets(), "fonts/Roboto-LightItalic.ttf");
        }
        return robotoLightItalic;
    }

    public static Typeface getItalicItalicFont(Context context) {
        if (robotoItalic == null) {
            robotoItalic = Typeface.createFromAsset(context.getResources()
                    .getAssets(), "fonts/Roboto-Italic.ttf");
        }
        return robotoItalic;
    }

    public static CustomTypefaceSpan getMediumHeaderSpan(Context context) {
        Typeface tf = getBoldHeaderFont(context);
        return new CustomTypefaceSpan(tf);
    }

    public static CustomTypefaceSpan getSemiBoldSpan(Context context) {
        Typeface tf = getSemiBoldFont(context);
        return new CustomTypefaceSpan(tf);
    }

    public static Typeface getQuicksandRegular(Context context) {
        if (quicksandRegular == null) {
            quicksandRegular = Typeface.createFromAsset(context.getResources()
                    .getAssets(), "fonts/Quicksand-Regular.ttf");
        }
        return quicksandRegular;
    }

    public static Typeface getQuicksandLight(Context context) {
        if (quicksandLight == null) {
            quicksandLight = Typeface.createFromAsset(context.getResources()
                    .getAssets(), "fonts/Quicksand-Light.ttf");
        }
        return quicksandLight;
    }

    public static Typeface getQuicksandBold(Context context) {
        if (quicksandBold == null) {
            quicksandBold = Typeface.createFromAsset(context.getResources()
                    .getAssets(), "fonts/Quicksand-Bold.ttf");
        }
        return quicksandBold;
    }

    public static Typeface getQuicksandMedium(Context context) {
        if (quicksandMedium == null) {
            quicksandMedium = Typeface.createFromAsset(context.getResources()
                    .getAssets(), "fonts/Quicksand-Medium.ttf");
        }
        return quicksandMedium;
    }


    public static Typeface getKohinoorBold(Context context) {
        if (kohinoorBold == null) {
            kohinoorBold = Typeface.createFromAsset(context.getResources()
                    .getAssets(), "fonts/Kohinoor.ttf");
        }
        return kohinoorBold;
    }

}