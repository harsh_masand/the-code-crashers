package com.sharedcode.widgets;

import android.app.TimePickerDialog;
import android.content.Context;
import android.widget.TimePicker;

/**
 * Created by Akhil on 16/08/16.
 */
public class CustomTimePickerDialog extends android.app.TimePickerDialog {

    TimePickerDialog.OnTimeSetListener listener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker timePicker, int i, int i1) {

        }
    };

    public CustomTimePickerDialog(Context context, TimePickerDialog.OnTimeSetListener listener, int hourOfDay, int minute, boolean is24HourView) {
        super(context, listener, hourOfDay, minute, is24HourView);
    }

    public CustomTimePickerDialog(Context context, int themeResId, TimePickerDialog.OnTimeSetListener listener, int hourOfDay, int minute, boolean is24HourView) {
        super(context, themeResId, listener, hourOfDay, minute, is24HourView);
    }

    public CustomTimePickerDialog(Context context, int themeResId, final OnTimeSetListener listener, int hourOfDay, int minute, boolean is24HourView, final int id) {
        super(context, themeResId, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int i, int i1) {
                listener.onTimeSet(timePicker, i, i1, id);
            }
        }, hourOfDay, minute, is24HourView);
    }

    public interface OnTimeSetListener {
        void onTimeSet(TimePicker timePicker, int var2, int var3, int id);

    }

}
