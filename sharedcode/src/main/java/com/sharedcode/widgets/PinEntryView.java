package com.sharedcode.widgets;


import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import com.sharedcode.R;


/**
 * A PIN entry view widget for Android based on the Android 5 Material Theme via the AppCompat v7
 * support library.
 */
public class PinEntryView extends ViewGroup {

    /**
     * Accent types
     */
    public static final int ACCENT_NONE = 0;
    public static final int ACCENT_ALL = 1;
    public static final int ACCENT_CHARACTER = 2;
    public static final int DEFAULT_PIN_LENGTH = 6;

    /**
     * Number of mDigits
     */
    private int mDigits;

    /**
     * Input type
     */
    private int mInputType;

    /**
     * Pin digit dimensions and styles
     */
    private int mDigitWidth;
    private int mDigitHeight;
    private int mDigitBackground;
    private int mDigitSpacing;
    private int mDigitTextSize;
    private int mDigitTextColor;
    private int mDigitElevation;

    /**
     * Accent dimensions and styles
     */
    private int mAccentType;
    private int mAccentWidth;
    private int mAccentColor;
    private int mDigitAccentColor;

    private boolean mIsFocus = false;

    /**
     * Character to use for each digit
     */
    private String mMask = "*";

    private String text = "";
    private boolean isMaskActive = true;

    /**
     * Edit text to handle input
     */
    private EditText mEditText;

    /**
     * Focus change listener to send focus events to
     */
    private OnFocusChangeListener mOnFocusChangeListener;

    /**
     * Pin entered listener used as a callback for when all mDigits have been entered
     */
    private OnPinEnteredListener mOnPinEnteredListener;

    /**
     * If set to false, will always draw accent color if type is CHARACTER or ALL
     * If set to true, will draw accent color only when focussed.
     */
    private boolean mAccentRequiresFocus;

    public PinEntryView(Context context) {
        this(context, null);
    }

    public PinEntryView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PinEntryView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        // Get style information
        TypedArray array = getContext().obtainStyledAttributes(attrs, R.styleable.PinEntryView);
        mDigits = array.getInt(R.styleable.PinEntryView_numDigits, DEFAULT_PIN_LENGTH);
        mInputType = array.getInt(R.styleable.PinEntryView_pinInputType, InputType.TYPE_CLASS_NUMBER);
        mAccentType = array.getInt(R.styleable.PinEntryView_accentType, ACCENT_NONE);

        // Dimensions
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        mDigitWidth = array.getDimensionPixelSize(R.styleable.PinEntryView_digitWidth,
                (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 50, metrics));
        mDigitHeight = array.getDimensionPixelSize(R.styleable.PinEntryView_digitHeight,
                (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 50, metrics));
        mDigitSpacing = array.getDimensionPixelSize(R.styleable.PinEntryView_digitSpacing,
                (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, metrics));
        mDigitTextSize = array.getDimensionPixelSize(R.styleable.PinEntryView_digitTextSize,
                (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 15, metrics));
        mAccentWidth = array.getDimensionPixelSize(R.styleable.PinEntryView_accentWidth,
                (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 3, metrics));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mDigitElevation = array.getDimensionPixelSize(R.styleable.PinEntryView_digitElevation, 0);
        }

        // Get theme to resolve defaults
        Resources.Theme theme = getContext().getTheme();

        // Background colour, default to android:windowBackground from theme
        TypedValue background = new TypedValue();
        theme.resolveAttribute(android.R.attr.windowBackground, background, true);
        mDigitBackground = array.getResourceId(R.styleable.PinEntryView_digitBackground,
                background.resourceId);

        // Text colour, default to android:textColorPrimary from theme
        TypedValue textColor = new TypedValue();
        theme.resolveAttribute(android.R.attr.textColorPrimary, textColor, true);
        mDigitTextColor = array.getColor(R.styleable.PinEntryView_digitTextColor,
                textColor.resourceId > 0 ? getResources().getColor(textColor.resourceId) :
                        textColor.data);

        // Accent colour, default to android:colorAccent from theme
        TypedValue accentColor = new TypedValue();
        theme.resolveAttribute(R.attr.colorAccent, accentColor, true);
        this.mAccentColor = array.getColor(R.styleable.PinEntryView_pinAccentColor,
                accentColor.resourceId > 0 ? getResources().getColor(accentColor.resourceId) :
                        accentColor.data);

        // Mask character
        String maskCharacter = array.getString(R.styleable.PinEntryView_mask);
        if (maskCharacter != null) {
            mMask = maskCharacter;
        }
        String textCharacter = array.getString(R.styleable.PinEntryView_text);
        if (textCharacter != null) {
            text = textCharacter;
        }

        // Accent shown, default to only when focused
        mAccentRequiresFocus = array.getBoolean(R.styleable.PinEntryView_accentRequiresFocus, true);
        mIsFocus = array.getBoolean(R.styleable.PinEntryView_isFocused, true);
        isMaskActive = array.getBoolean(R.styleable.PinEntryView_isMaskActive, true);


        // Recycle the typed array
        array.recycle();

        // Add child views
        addViews();
    }

    @Override
    public boolean shouldDelayChildPressedState() {
        return false;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        // Calculate the size of the view
        int width = (mDigitWidth * mDigits) + (mDigitSpacing * (mDigits - 1));
        setMeasuredDimension(
                width + getPaddingLeft() + getPaddingRight() + (mDigitElevation * 2),
                mDigitHeight + getPaddingTop() + getPaddingBottom() + (mDigitElevation * 2));

        int height = MeasureSpec.makeMeasureSpec(getMeasuredHeight(), MeasureSpec.EXACTLY);

        // Measure children
        for (int i = 0; i < getChildCount(); i++) {
            getChildAt(i).measure(width, height);
        }
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        // Position the text views
        for (int i = 0; i < mDigits; i++) {
            View child = getChildAt(i);
            int left = i * mDigitWidth + (i > 0 ? i * mDigitSpacing : 0);
            child.layout(
                    left + getPaddingLeft() + mDigitElevation,
                    getPaddingTop() + (mDigitElevation / 2),
                    left + getPaddingLeft() + mDigitElevation + mDigitWidth,
                    getPaddingTop() + (mDigitElevation / 2) + mDigitHeight);
        }

        // Add the edit text as a 1px wide view to allow it to focus
        getChildAt(mDigits).layout(0, 0, 1, getMeasuredHeight());
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            // Make sure this view is focused
            mEditText.requestFocus();

            // Show keyboard
            InputMethodManager inputMethodManager = (InputMethodManager) getContext()
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.showSoftInput(mEditText, 0);
            return true;
        }
        return super.onTouchEvent(event);
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        Parcelable parcelable = super.onSaveInstanceState();
        SavedState savedState = new SavedState(parcelable);
        savedState.editTextValue = mEditText.getText().toString();
        return savedState;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        SavedState savedState = (SavedState) state;
        super.onRestoreInstanceState(savedState.getSuperState());
        mEditText.setText(savedState.editTextValue);
        mEditText.setSelection(savedState.editTextValue.length());
    }

    @Override
    public OnFocusChangeListener getOnFocusChangeListener() {
        return mOnFocusChangeListener;
    }

    @Override
    public void setOnFocusChangeListener(OnFocusChangeListener l) {
        mOnFocusChangeListener = l;
    }

    /**
     * Add a TextWatcher to the EditText
     *
     * @param watcher
     */
    public void addTextChangedListener(TextWatcher watcher) {
        mEditText.addTextChangedListener(watcher);
    }

    /**
     * Remove a TextWatcher from the EditText
     *
     * @param watcher
     */
    public void removeTextChangedListener(TextWatcher watcher) {
        mEditText.removeTextChangedListener(watcher);
    }

    /**
     * Get current pin entered listener
     *
     * @return
     */
    public OnPinEnteredListener getmOnPinEnteredListener() {
        return mOnPinEnteredListener;
    }

    /**
     * Set pin entered listener
     *
     * @param mOnPinEnteredListener
     */
    public void setmOnPinEnteredListener(OnPinEnteredListener mOnPinEnteredListener) {
        this.mOnPinEnteredListener = mOnPinEnteredListener;
    }

    /**
     * Get the {@link Editable} from the EditText
     *
     * @return
     */
    public Editable getmText() {
        return mEditText.getText();
    }

    /**
     * Set text to the EditText
     *
     * @param text
     */
    public void setmText(CharSequence text) {
        if (text.length() > mDigits) {
            text = text.subSequence(0, mDigits);
        }
        mEditText.setText(text);
    }

    /**
     * Clear pin input
     */

    public void setFocuseed(Context mContext, boolean isFocused) {
        if (isFocused) {
            InputMethodManager inputMethodManager = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.toggleSoftInputFromWindow(mEditText.getApplicationWindowToken(), InputMethodManager.SHOW_FORCED, 0);
            mEditText.requestFocus();
        }

        mIsFocus = isFocused;
    }

    public void clearText() {
        mEditText.setText("");
    }

    public int getmDigits() {
        return mDigits;
    }

    public int getmInputType() {
        return mInputType;
    }

    public int getmAccentType() {
        return mAccentType;
    }

    public int getDigitWidth() {
        return mDigitWidth;
    }

    public int getDigitHeight() {
        return mDigitHeight;
    }

    public int getDigitSpacing() {
        return mDigitSpacing;
    }

    public int getmDigitTextSize() {
        return mDigitTextSize;
    }

    public int getmAccentWidth() {
        return mAccentWidth;
    }

    public int getmDigitElevation() {
        return mDigitElevation;
    }

    public int getmDigitBackground() {
        return mDigitBackground;
    }

    public int getmDigitTextColor() {
        return mDigitTextColor;
    }

    public int getmAccentColor() {
        return mAccentColor;
    }

    public String getmMask() {
        return mMask;
    }

    public boolean getmAccentRequiresFocus() {
        return mAccentRequiresFocus;
    }

    /**
     * Create views and add them to the view group
     */
    private void addViews() {
        // Add a digit view for each digit
        for (int i = 0; i < mDigits; i++) {
            DigitView digitView = new DigitView(getContext());
            digitView.setWidth(mDigitWidth);
            digitView.setHeight(mDigitHeight);
            digitView.setHint(mMask);
            digitView.setBackgroundResource(mDigitBackground);
            digitView.setTextColor(getResources().getColor(R.color.c_black_33));
            digitView.setHintTextColor(getResources().getColor(R.color.c_dim_grey));
            digitView.setTextSize(TypedValue.COMPLEX_UNIT_PX, mDigitTextSize);
            digitView.setGravity(Gravity.CENTER);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                digitView.setElevation(mDigitElevation);
            }
            addView(digitView);
        }

        // Add an "invisible" edit text to handle input
        mEditText = new EditText(getContext());
        //mEditText.setVisibility(INVISIBLE);
//        mEditText.setFocusable(false);
//        mEditText.setFocusableInTouchMode(false);

        mEditText.setBackgroundColor(getResources().getColor(android.R.color.transparent));
        LayoutParams layoutParams = new LayoutParams(0, 0);
        mEditText.setLayoutParams(layoutParams);
        mEditText.setTextColor(getResources().getColor(android.R.color.transparent));
        mEditText.setCursorVisible(false);
        mEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(mDigits)});
        mEditText.setInputType(mInputType);
        mEditText.setHint(mMask);
        mEditText.setText(text);
        mEditText.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI);
        mEditText.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                // Update the selected state of the views
                mIsFocus = hasFocus;

                int length = mEditText.getText().length();
                for (int i = 0; i < mDigits; i++) {
                    getChildAt(i).setSelected(hasFocus && (mAccentType == ACCENT_ALL ||
                            (mAccentType == ACCENT_CHARACTER && (i == length ||
                                    (i == mDigits - 1 && length == mDigits)))));
                }

                // Make sure the cursor is at the end
                mEditText.setSelection(length);

                // Provide focus change events to any listener
                if (mOnFocusChangeListener != null) {
                    mOnFocusChangeListener.onFocusChange(PinEntryView.this, hasFocus);
                }
            }
        });
        mEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                int length = s.length();
                for (int i = 0; i < mDigits; i++) {
                    if (s.length() > i) {
                        String mask = PinEntryView.this.mMask == null || PinEntryView.this.mMask.length() == 0 || !isMaskActive ?
                                String.valueOf(s.charAt(i)) : PinEntryView.this.mMask;
                        ((TextView) getChildAt(i)).setText(String.valueOf(s.charAt(i)));
                    } else {
                        ((TextView) getChildAt(i)).setText("");
                    }
                    if (mEditText.hasFocus()) {
                        getChildAt(i).setSelected(mAccentType == ACCENT_ALL ||
                                (mAccentType == ACCENT_CHARACTER && (i == length ||
                                        (i == mDigits - 1 && length == mDigits))));
                    }
                }

                if (mOnPinEnteredListener != null) {
                    mOnPinEnteredListener.onPinEntered(s.toString());
                }
            }
        });
        addView(mEditText);
    }

    public void setText(String text) {
        mEditText.setText(text);
    }

    public interface OnPinEnteredListener {
        void onPinEntered(String pin);
    }

    /**
     * Save state of the view
     */
    static class SavedState extends BaseSavedState {

        public static final Creator<SavedState> CREATOR =
                new Creator<SavedState>() {
                    @Override
                    public SavedState createFromParcel(Parcel in) {
                        return new SavedState(in);
                    }

                    @Override
                    public SavedState[] newArray(int size) {
                        return new SavedState[size];
                    }
                };
        String editTextValue;

        public SavedState(Parcelable superState) {
            super(superState);
        }

        private SavedState(Parcel source) {
            super(source);
            editTextValue = source.readString();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            super.writeToParcel(dest, flags);
            dest.writeString(editTextValue);
        }

    }

    /**
     * Custom text view that adds a coloured accent when selected
     */
    private class DigitView extends TextView {

        /**
         * Paint used to draw accent
         */
        private Paint paint;

        public DigitView(Context context) {
            this(context, null);
        }

        public DigitView(Context context, AttributeSet attrs) {
            this(context, attrs, 0);
        }

        public DigitView(Context context, AttributeSet attrs, int defStyleAttr) {
            super(context, attrs, defStyleAttr);

            // Setup paint to keep onDraw as lean as possible
            paint = new Paint();
            paint.setStyle(Paint.Style.FILL);

        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);

            // If selected draw the accent
            if (isSelected()) {
                paint.setColor(mAccentColor);
                canvas.drawRect(0, getHeight() - mAccentWidth, getWidth(), getHeight(), paint);
            } else {
                paint.setColor(getResources().getColor(R.color.c_dim_grey));
                canvas.drawRect(0, getHeight() - mAccentWidth, getWidth(), getHeight(), paint);
            }
        }

    }

}
