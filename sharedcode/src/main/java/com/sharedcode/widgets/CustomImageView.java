package com.sharedcode.widgets;

import android.content.Context;
import android.util.AttributeSet;
import androidx.appcompat.widget.AppCompatImageView;

public class CustomImageView extends AppCompatImageView {
    /**
     * Note: You must add android:indeterminateOnly="false" in ProgressBar xml
     * for this to work properly
     */

    private Context mContext;

    public CustomImageView(Context context) {
        super(context);
        mContext = context;
    }

    public CustomImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    public CustomImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mContext = context;
    }
}
