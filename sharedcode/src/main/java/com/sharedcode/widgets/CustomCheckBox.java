package com.sharedcode.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import androidx.appcompat.widget.AppCompatCheckBox;
import com.sharedcode.R;
import com.sharedcode.utils.UIUtils;

public class CustomCheckBox extends AppCompatCheckBox {

    private static String DEBUG_TAG = "CustomCheckBox";

    public CustomCheckBox(Context context) {
        super(context);
    }

    public CustomCheckBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public CustomCheckBox(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    public void init(AttributeSet attrs) {
        setTypeface(getNormalFont());

        int mTextStyle = 0;
        TypedArray a = getContext().getTheme().obtainStyledAttributes(attrs,
                R.styleable.CustomCheckBox, 0, 0);
        try {
            mTextStyle = a
                    .getInteger(R.styleable.CustomCheckBox_text_weight, 0);
        } finally {
            a.recycle();
        }

        setTypeface(UIUtils.getFont(getContext(), mTextStyle));
    }

    private Typeface getNormalFont() {
        return FontCache.getRegularFont(getContext());
    }
}