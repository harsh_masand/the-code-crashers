package com.sharedcode.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.InputType;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import com.google.android.material.textfield.TextInputEditText;
import com.sharedcode.R;
import com.sharedcode.utils.UIUtils;

public class CustomEditText extends TextInputEditText {

    private static String DEBUG_TAG = "CustomEditText";
    Rect mTextBounds;
    TextPaint mFixedTextPaint;
    int actionX, actionY;
    private String mFixedText;
    private int mFixedTextPadding;
    private int mTextColor;
    private int mTextColorError;
    private boolean enableImeOptionsForMultiline = false;
    private Drawable drawableRight;
    private Drawable drawableLeft;
    private Drawable drawableTop;
    private Drawable drawableBottom;
    private DrawableClickListener clickListener;

    public CustomEditText(Context context) {
        super(context);
        // TODO Auto-generated constructor stub
        setInputType(getInputType() | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
    }

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        setStyle(context, attrs);
        initPaint();
    }

    public CustomEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setStyle(context, attrs);
        initPaint();
    }

    void initPaint() {
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingRight = getPaddingRight();
        int paddingBottom = getPaddingBottom();

        setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);

        mFixedTextPaint = new TextPaint();
        mFixedTextPaint.setAntiAlias(true);
        mFixedTextPaint.setTypeface(FontCache.getSemiBoldFont(getContext()));
        mFixedTextPaint.setTextSize(getTextSize());
        mTextBounds = new Rect();
    }

    private void setStyle(Context context, AttributeSet attrs) {
        int mTextStyle = 0;
        boolean suppressInputType = false;

        TypedArray a = context.getTheme().obtainStyledAttributes(attrs,
                R.styleable.CustomEditText, 0, 0);
        try {
            mTextStyle = a.getInteger(
                    R.styleable.CustomEditText_text_weight, 0);
            suppressInputType = a.getBoolean(
                    R.styleable.CustomEditText_suppress_inputType, false);

            mFixedText = a.getString(R.styleable.CustomEditText_fixedText);
            mFixedTextPadding = a.getDimensionPixelSize(R.styleable.CustomEditText_fixedTextPadding, 0);
            mTextColor = a.getColor(
                    R.styleable.CustomEditText_android_textColor, 0);
            enableImeOptionsForMultiline = a.getBoolean(
                    R.styleable.CustomEditText_enableImeForMultiline, false);

        } finally {
            a.recycle();
        }

        mTextColorError = getResources().getColor(R.color.c_edit_text_error);

        setTypeface(UIUtils.getFont(getContext(), mTextStyle));

        if (!suppressInputType)
            setInputType(getInputType()
                    | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);

    }

    @Override
    protected void onDraw(Canvas canvas) {
        Drawable[] drawables = getCompoundDrawables();
        Drawable leftDrawable = drawables[0];
        if (mFixedText != null) {
            if (leftDrawable != null) {
                int drawableWidth = leftDrawable.getIntrinsicWidth();
                int bottomPadding = getPaddingBottom();
                int leftPadding = getPaddingLeft();

                int drawablePadding = mFixedTextPadding;
                mFixedTextPaint.getTextBounds(mFixedText, 0,
                        mFixedText.length(), mTextBounds);
                mFixedTextPaint.measureText(mFixedText);
                int textDrawY = canvas.getHeight() / 2;
                /**
                 * Adding adjustment for text height below lollipop
                 * Checked it on two emulator nexus 5 with different API level
                 *
                 * On API level 21 no addition of extra text bounds required
                 * The screen shots are attached at
                 * https://app.asana.com/0/29791038949580/36789643436770
                 */
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                    textDrawY += mTextBounds.height() / 2;
                }
                canvas.drawText(mFixedText, drawableWidth + leftPadding
                        + drawablePadding, textDrawY, mFixedTextPaint);

            }
        }
        super.onDraw(canvas);
    }

    public void showError() {
        startAnimation(AnimationUtils.loadAnimation(getContext(),
                R.anim.shake_invalid));
        setTextColor(mTextColorError);
    }

    public void removeError() {
        setTextColor(mTextColor);
    }

    @Override
    protected void onTextChanged(CharSequence text, int start,
                                 int lengthBefore, int lengthAfter) {
        setTextColor(mTextColor);
        super.onTextChanged(text, start, lengthBefore, lengthAfter);
    }

    public boolean isEmpty() {
        return getText().toString().trim().length() == 0;
    }

    @Override
    public InputConnection onCreateInputConnection(EditorInfo outAttrs) {
        InputConnection connection = super.onCreateInputConnection(outAttrs);
        if (enableImeOptionsForMultiline) {
            int imeActions = outAttrs.imeOptions & EditorInfo.IME_MASK_ACTION;
            if ((imeActions & EditorInfo.IME_ACTION_DONE) != 0) {
                // clear the existing action
                outAttrs.imeOptions ^= imeActions;
                // set the DONE action
                outAttrs.imeOptions |= EditorInfo.IME_ACTION_DONE;
            }
            if ((outAttrs.imeOptions & EditorInfo.IME_FLAG_NO_ENTER_ACTION) != 0) {
                outAttrs.imeOptions &= ~EditorInfo.IME_FLAG_NO_ENTER_ACTION;
            }
        }
        return connection;
    }

    @Override
    public void setCompoundDrawables(Drawable left, Drawable top,
                                     Drawable right, Drawable bottom) {
        if (left != null) {
            drawableLeft = left;
        }
        if (right != null) {
            drawableRight = right;
        }
        if (top != null) {
            drawableTop = top;
        }
        if (bottom != null) {
            drawableBottom = bottom;
        }
        super.setCompoundDrawables(left, top, right, bottom);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Rect bounds;
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            actionX = (int) event.getX();
            actionY = (int) event.getY();
            if (drawableBottom != null
                    && drawableBottom.getBounds().contains(actionX, actionY)) {
                clickListener.onClick(DrawableClickListener.DrawablePosition.BOTTOM);
                return super.onTouchEvent(event);
            }

            if (drawableTop != null
                    && drawableTop.getBounds().contains(actionX, actionY)) {
                clickListener.onClick(DrawableClickListener.DrawablePosition.TOP);
                return super.onTouchEvent(event);
            }

            // this works for left since container shares 0,0 origin with bounds
            if (drawableLeft != null) {
                bounds = null;
                bounds = drawableLeft.getBounds();

                int x, y;
                int extraTapArea = (int) (13 * getResources().getDisplayMetrics().density + 0.5);

                x = actionX;
                y = actionY;

                if (!bounds.contains(actionX, actionY)) {
                    /** Gives the +20 area for tapping. */
                    x = (int) (actionX - extraTapArea);
                    y = (int) (actionY - extraTapArea);

                    if (x <= 0)
                        x = actionX;
                    if (y <= 0)
                        y = actionY;

                    /** Creates square from the smallest value */
                    if (x < y) {
                        y = x;
                    }
                }

                if (bounds.contains(x, y) && clickListener != null) {
                    clickListener
                            .onClick(DrawableClickListener.DrawablePosition.LEFT);
                    event.setAction(MotionEvent.ACTION_CANCEL);
                    return false;

                }
            }

            if (drawableRight != null) {

                bounds = null;
                bounds = drawableRight.getBounds();

                int x, y;
                int extraTapArea = 13;

                /**
                 * IF USER CLICKS JUST OUT SIDE THE RECTANGLE OF THE DRAWABLE
                 * THAN ADD X AND SUBTRACT THE Y WITH SOME VALUE SO THAT AFTER
                 * CALCULATING X AND Y CO-ORDINATE LIES INTO THE DRAWBABLE
                 * BOUND. - this process help to increase the tappable area of
                 * the rectangle.
                 */
                x = (int) (actionX + extraTapArea);
                y = (int) (actionY - extraTapArea);

                /**Since this is right drawable subtract the value of x from the width
                 * of view. so that width - tappedarea will result in x co-ordinate in drawable bound.
                 */
                x = getWidth() - x;

                /*x can be negative if user taps at x co-ordinate just near the width.
                 * e.g views width = 300 and user taps 290. Then as per previous calculation
                 * 290 + 13 = 303. So subtract X from getWidth() will result in negative value.
                 * So to avoid this add the value previous added when x goes negative.
                 */

                if (x <= 0) {
                    x += extraTapArea;
                }

                /* If result after calculating for extra tappable area is negative.
                 * assign the original value so that after subtracting
                 * extratapping area value doesn't go into negative value.
                 */

                if (y <= 0)
                    y = actionY;

                /**If drawble bounds contains the x and y points then move ahead.*/
                if (bounds.contains(x, y) && clickListener != null) {
                    clickListener
                            .onClick(DrawableClickListener.DrawablePosition.RIGHT);
                    event.setAction(MotionEvent.ACTION_CANCEL);
                    return false;
                }
                return super.onTouchEvent(event);
            }

        }
        return super.onTouchEvent(event);
    }

    @Override
    protected void finalize() throws Throwable {
        drawableRight = null;
        drawableBottom = null;
        drawableLeft = null;
        drawableTop = null;
        super.finalize();
    }

    public void setDrawableClickListener(DrawableClickListener listener) {
        this.clickListener = listener;
    }

    public interface DrawableClickListener {
        void onClick(DrawablePosition target);

        enum DrawablePosition {TOP, BOTTOM, LEFT, RIGHT}
    }
}
