package com.sharedcode.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;
import com.sharedcode.R;
import com.sharedcode.helpers.LogHelper;
import com.sharedcode.utils.NumberToCurrency;

public class CustomAmountView extends CustomTextView {

    private static String DEBUG_TAG = "CustomAmountView";

    public CustomAmountView(Context context) {
        super(context);
    }

    public CustomAmountView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            setTextStyle(context, attrs);
        }
    }

    public CustomAmountView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setTextStyle(context, attrs);
    }

    private void setTextStyle(Context context, AttributeSet attrs) {

        String text;
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs,
                R.styleable.CustomAmountView, 0, 0);
        try {
            text = a.getString(
                    R.styleable.CustomAmountView_android_text);
            setText(text);
        } finally {
            a.recycle();
        }
    }

    @Override
    public void setText(CharSequence ch, BufferType type) {
        String amountText = null;
        String firstPart, secondPart;
        if (!TextUtils.isEmpty(ch)) {
            try {
                if ((String.valueOf(ch)).contains(".")) {
                    String[] strings = (String.valueOf(ch)).split("\\.");
                    if (strings.length == 2) {

                        String firstDigits = strings[0];
                        if (TextUtils.isEmpty(firstDigits)) {
                            firstDigits = "0";
                        }
                        firstPart = NumberToCurrency.convertNumberToWords(Integer.parseInt(firstDigits));
                        if (strings[1].length() == 2) {
                            String secondDigits = strings[1];
                            if (TextUtils.isEmpty(secondDigits)) {
                                secondDigits = "0";
                            }
                            secondPart = NumberToCurrency.convertNumberToWords(Integer.parseInt(secondDigits));
                            if (!TextUtils.isEmpty(firstPart) && !TextUtils.isEmpty(secondPart)) {
                                amountText = firstPart + " Rupees and " + secondPart + " Paise ";
                            } else if (!TextUtils.isEmpty(firstPart) && TextUtils.isEmpty(secondPart)) {
                                amountText = firstPart;
                            } else {
                                amountText = secondPart + " Paise ";
                            }
                        } else {
                            secondPart = NumberToCurrency.convertNumberToWords(Integer.parseInt(strings[1] + "0"));
                            if (!TextUtils.isEmpty(firstPart) && !TextUtils.isEmpty(secondPart)) {
                                amountText = firstPart + " Rupees and " + secondPart + " Paise ";
                            } else if (!TextUtils.isEmpty(firstPart) && TextUtils.isEmpty(secondPart)) {
                                amountText = firstPart;
                            } else {
                                amountText = secondPart + " Paise ";
                            }
                        }
                    }
                } else {
                    int amountValue = Integer.parseInt(String.valueOf(ch));
                    amountText = NumberToCurrency.convertNumberToWords(amountValue);
                }
            } catch (NumberFormatException nfe) {
                LogHelper.e(DEBUG_TAG, nfe.toString());
                amountText = getResources().getString(R.string.number_format_exception);
            }
        }
        super.setText(amountText, type);
    }
}
