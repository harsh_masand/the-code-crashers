package com.sharedcode.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.TextView;
import androidx.appcompat.widget.SwitchCompat;
import com.sharedcode.R;
import com.sharedcode.utils.UIUtils;


public class CustomSwitch extends SwitchCompat {

    private static String DEBUG_TAG = "SwitchCompat";
    private int mTextCase = 0;

    public CustomSwitch(Context context) {
        super(context);
    }

    public CustomSwitch(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTextStyle(context, attrs);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setStateListAnimator(null);
        }
    }

    public CustomSwitch(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setTextStyle(context, attrs);
    }

    private void setTextStyle(Context context, AttributeSet attrs) {
        int mTextStyle = 0;
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs,
                R.styleable.CustomSwitch, 0, 0);
        try {
            mTextStyle = a
                    .getInteger(R.styleable.CustomSwitch_text_weight, 0);
        } finally {
            a.recycle();
        }

        setTypeface(UIUtils.getFont(getContext(), mTextStyle));
    }

    @Override
    public void setText(CharSequence ch, TextView.BufferType type) {

        switch (mTextCase) {
            case 0:
                super.setText(ch, type);
                break;
            case 1: {
                String s = ch.toString();
                s = UIUtils.toCamelCase(s);
                super.setText(s, type);
            }
            break;
            case 2: {
                String s = ch.toString();
                s = s.toLowerCase();
                super.setText(s, type);
            }
            break;
            case 3: {
                String s = ch.toString();
                s = s.toUpperCase();
                super.setText(s, type);
            }
            break;
            default:
                break;
        }

    }
}
