package com.sharedcode.widgets;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ScaleDrawable;
import android.text.Spanned;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import androidx.appcompat.widget.AppCompatTextView;
import com.sharedcode.R;
import com.sharedcode.utils.UIUtils;

public class CustomTextView extends AppCompatTextView {
    static final int Y_MIN_DISTANCE = 3;
    static final int X_MIN_DISTANCE = 15;
    protected int mTextCase = 0;
    Resources mResources;
    private int mDrawableWidth = -1;
    private int mDrawableHeight = -1;
    private float initialX;
    private float initialY;
    private int xMinDistance;
    private int yMinDistance;

    public CustomTextView(Context context) {
        super(context);
        distanceBaseOnDevice();
    }

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            setTextStyle(context, attrs);
            setTextCase(context, attrs);
        }
        distanceBaseOnDevice();
    }

    public CustomTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setTextStyle(context, attrs);
        setTextCase(context, attrs);
        distanceBaseOnDevice();
    }

    private void setTextStyle(Context context, AttributeSet attrs) {
        mResources = getResources();

        int mTextStyle = 0;
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs,
                R.styleable.CustomTextView, 0, 0);
        try {
            //default is roboto regular
            mTextStyle = a
                    .getInteger(R.styleable.CustomTextView_text_weight, 2);
            mDrawableWidth = a.getDimensionPixelSize(R.styleable.CustomTextView_drawableWidth, -1);
            mDrawableHeight = a.getDimensionPixelSize(R.styleable.CustomTextView_drawableHeight, -1);
        } finally {
            a.recycle();
        }
        if (mDrawableWidth != -1 && mDrawableHeight != -1) {
            scaleDrawable(mDrawableWidth, mDrawableHeight);
        }
        setTypeface(UIUtils.getFont(getContext(), mTextStyle));
    }

    protected void setTextCase(Context context, AttributeSet attrs) {
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs,
                R.styleable.CustomTextView, 0, 0);
        try {
            mTextCase = a.getInteger(R.styleable.CustomTextView_text_case, 0);
        } finally {
            a.recycle();
        }
        CharSequence text = getText();
        setText(text, text instanceof Spanned ? BufferType.SPANNABLE : BufferType.NORMAL);
    }

    @Override
    public void setText(CharSequence ch, BufferType type) {

        switch (mTextCase) {
            case 0:
                super.setText(ch, type);
                break;
            case 1: {
                String s = ch.toString();
                s = UIUtils.toCamelCase(s);
                super.setText(s, type);
            }
            break;
            case 2: {
                String s = ch.toString();
                s = s.toLowerCase();
                super.setText(s, type);
            }
            break;
            case 3: {
                String s = ch.toString();
                s = s.toUpperCase();
                super.setText(s, type);
            }
            break;
            default:
                break;
        }

    }


    /**
     * mehtod added to handle scrollable textview
     *
     * @param event
     * @return
     */
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                initialX = event.getX();
                initialY = event.getY();
                break;
            case MotionEvent.ACTION_UP:
                this.getParent().requestDisallowInterceptTouchEvent(false);
                break;
            case MotionEvent.ACTION_MOVE:
                float currentX = event.getX();
                float currentY = event.getY();
                float deltaX = currentX - initialX;
                float deltaY = currentY - initialY;
                if (Math.abs(deltaX) > xMinDistance) {
                    this.getParent().requestDisallowInterceptTouchEvent(false);
                } else if (Math.abs(deltaY) > yMinDistance) {
                    this.getParent().requestDisallowInterceptTouchEvent(true);
                }
                break;
        }

        return super.onTouchEvent(event);
    }

    /* Scale compound drawables
     *
     * @param widthInPx width in Pixels
     * @param heightInPx Height in Pixels
     */
    public void scaleDrawable(int widthInPx, int heightInPx) {
        Drawable[] drawables = getCompoundDrawables();
        ScaleDrawable[] sd = new ScaleDrawable[4];
        for (int i = 0; i < 4; i++) {
            Drawable drawable = drawables[i];
            if (drawable != null) {
                drawable.setBounds(0, 0, (int) (widthInPx), (int) (heightInPx));
            }
            sd[i] = new ScaleDrawable(drawable, 0, widthInPx, heightInPx);
        }
        setCompoundDrawables(sd[0].getDrawable(), sd[1].getDrawable(), sd[2].getDrawable(), sd[3].getDrawable());
    }

    private float getDpi() {
        Resources resources = getContext().getResources();
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        return displayMetrics.density;
    }

    private void distanceBaseOnDevice() {
        xMinDistance = (int) (X_MIN_DISTANCE * getDpi());
        yMinDistance = (int) (Y_MIN_DISTANCE * getDpi());
    }
}