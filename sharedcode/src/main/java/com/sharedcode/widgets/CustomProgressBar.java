package com.sharedcode.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.ProgressBar;
import com.sharedcode.R;

public class CustomProgressBar extends ProgressBar {
    AttributeSet attrs;
    /**
     * Note: You must add android:indeterminateOnly="false" in ProgressBar xml
     * for this to work properly
     */

    private Context mContext;

    public CustomProgressBar(Context context) {
        super(context);
        mContext = context;
    }

    public CustomProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        this.attrs = attrs;
    }

    public CustomProgressBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mContext = context;
        this.attrs = attrs;
    }

    public void initColor(ProgressBar progressBar) {
        int mProgressColor = -1;
        if (attrs != null) {
            TypedArray a = getContext().getTheme().obtainStyledAttributes(attrs,
                    R.styleable.CustomProgressBar, 0, 0);
            try {
                mProgressColor = a
                        .getColor(R.styleable.CustomProgressBar_progressColor, -1);
            } finally {
                a.recycle();
            }

            PorterDuff.Mode mode = PorterDuff.Mode.SRC_IN;
            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1) {
                mode = PorterDuff.Mode.MULTIPLY;
            }

            if (mProgressColor != -1 && progressBar.getIndeterminateDrawable() != null)
                progressBar.getIndeterminateDrawable().setColorFilter(mProgressColor, mode);
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        initColor(this);
    }
}
