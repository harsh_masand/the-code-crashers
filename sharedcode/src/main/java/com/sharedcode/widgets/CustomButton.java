package com.sharedcode.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import androidx.appcompat.widget.AppCompatButton;
import com.sharedcode.R;
import com.sharedcode.utils.UIUtils;


public class CustomButton extends AppCompatButton {

    private static String DEBUG_TAG = "CustomButton";
    private int mTextCase = 0;

    public CustomButton(Context context) {
        super(context);
    }

    public CustomButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTextStyle(context, attrs);
        setTextCase(context, attrs);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setStateListAnimator(null);
        }
    }

    public CustomButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setTextStyle(context, attrs);
        setTextCase(context, attrs);
    }

    private void setTextStyle(Context context, AttributeSet attrs) {
        int mTextStyle = 0;
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs,
                R.styleable.CustomButton, 0, 0);
        try {
            mTextStyle = a
                    .getInteger(R.styleable.CustomButton_text_weight, 0);
        } finally {
            a.recycle();
        }

        setTypeface(UIUtils.getFont(getContext(), mTextStyle));
    }

    private void setTextCase(Context context, AttributeSet attrs) {
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs,
                R.styleable.CustomButton, 0, 0);
        try {
            mTextCase = a.getInteger(R.styleable.CustomButton_text_case, 0);
        } finally {
            a.recycle();
        }
        setText(getText(), BufferType.SPANNABLE);
    }

    @Override
    public void setText(CharSequence ch, BufferType type) {

        switch (mTextCase) {
            case 0:
                super.setText(ch, type);
                break;
            case 1: {
                String s = ch.toString();
                s = UIUtils.toCamelCase(s);
                super.setText(s, type);
            }
            break;
            case 2: {
                String s = ch.toString();
                s = s.toLowerCase();
                super.setText(s, type);
            }
            break;
            case 3: {
                String s = ch.toString();
                s = s.toUpperCase();
                super.setText(s, type);
            }
            break;
            default:
                break;
        }
    }
}
