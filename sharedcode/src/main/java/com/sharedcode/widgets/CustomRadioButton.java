package com.sharedcode.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import androidx.appcompat.widget.AppCompatRadioButton;
import com.sharedcode.R;
import com.sharedcode.utils.UIUtils;

public class CustomRadioButton extends AppCompatRadioButton {

    private static String DEBUG_TAG = "CustomRadioButton";

    public CustomRadioButton(Context context) {
        super(context);
    }

    public CustomRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public CustomRadioButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    public void init(AttributeSet attrs) {
        setTypeface(getNormalFont());

        int mTextStyle = 0;
        TypedArray a = getContext().getTheme().obtainStyledAttributes(attrs,
                R.styleable.CustomRadioButton, 0, 0);
        try {
            mTextStyle = a
                    .getInteger(R.styleable.CustomRadioButton_text_weight, 0);
        } finally {
            a.recycle();
        }

        setTypeface(UIUtils.getFont(getContext(), mTextStyle));
    }

    private Typeface getNormalFont() {
        return FontCache.getRegularFont(getContext());
    }
}