package com.sharedcode.widgets;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import androidx.appcompat.widget.AppCompatAutoCompleteTextView;
import com.sharedcode.R;
import com.sharedcode.utils.UIUtils;

/**
 * Created by Prashant on 30/11/16.
 */

public class CustomAutoCompleteTextView extends AppCompatAutoCompleteTextView {
    private Resources mResources;
    private Drawable drawableRight;
    private Drawable drawableLeft;
    private Drawable drawableTop;
    private Drawable drawableBottom;

    public CustomAutoCompleteTextView(Context context) {
        super(context);

    }

    public CustomAutoCompleteTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTextStyle(context, attrs);
    }

    public CustomAutoCompleteTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setTextStyle(context, attrs);
    }

    private void setTextStyle(Context context, AttributeSet attrs) {
        mResources = getResources();

        int mTextStyle = 0;
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs,
                R.styleable.CustomAutoCompleteTextView, 0, 0);
        try {
            mTextStyle = a
                    .getInteger(R.styleable.CustomAutoCompleteTextView_android_text, 0);
        } finally {
            a.recycle();
        }

        setTypeface(UIUtils.getFont(getContext(), mTextStyle));
    }

    @Override
    public void setCompoundDrawables(Drawable left, Drawable top, Drawable right, Drawable bottom) {
        super.setCompoundDrawables(left, top, right, bottom);
        if (right != null) {
            drawableRight = right;
        }
        if (left != null) {
            drawableLeft = left;
        }
        if (top != null) {
            drawableTop = top;
        }
        if (bottom != null) {
            drawableBottom = bottom;
        }
        super.setCompoundDrawables(left, top, right, bottom);
    }

    @Override
    protected void onTextChanged(CharSequence text, int start, int lengthBefore, int lengthAfter) {
        super.onTextChanged(text, start, lengthBefore, lengthAfter);
        if (this.getText().toString().length() > 0)
            this.setCompoundDrawablesWithIntrinsicBounds(drawableLeft, drawableTop, drawableRight, drawableBottom);
        else
            this.setCompoundDrawablesWithIntrinsicBounds(drawableLeft, drawableTop, null, drawableBottom);
    }
}
