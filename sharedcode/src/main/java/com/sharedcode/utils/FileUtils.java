package com.sharedcode.utils;

import android.content.Context;
import android.os.Environment;
import com.sharedcode.helpers.StorageHelper;

import java.io.File;

/**
 * Created by Akhil on 23/08/16.
 */
public class FileUtils {
    //:TODO Refine storage helper methods
    public static void deleteExternalStoragePrivatePicture(Context context, String fileName) {
        // Create a path where we will place our picture in the user's
        // public pictures directory and delete the file.  If external
        // storage is not currently mounted this will fail.
        File path = getPictureStorageLocation(context);
        if (path != null) {
            File file = new File(path, fileName);
            file.delete();
        }
    }

    public static File getStoredPicture(Context context, String fileName) {
        // Create a path where we will place our picture in the user's
        // public pictures directory and delete the file.  If external
        // storage is not currently mounted this will fail.
        File path = getPictureStorageLocation(context);
        File file = null;
        if (path != null) {
            file = new File(path, fileName);
        }
        return file;
    }

    public static File getPictureStorageLocation(Context context) {
        File storageDir = null;
        if (StorageHelper.isExternalStorageReadableAndWritable()) {
            storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        }
        if (storageDir == null) {
            storageDir = context.getFilesDir();
        }
        return storageDir;
    }

}
