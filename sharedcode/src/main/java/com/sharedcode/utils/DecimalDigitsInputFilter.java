package com.sharedcode.utils;

import android.text.InputFilter;
import android.text.Spanned;

/**
 * Created by Prashant on 03/02/17.
 */

public class DecimalDigitsInputFilter implements InputFilter {
    private final int decimalDigits;

    private final int maxDigits;

    /**
     * Constructor.
     *
     * @param decimalDigits maximum decimal digits
     */
    public DecimalDigitsInputFilter(int decimalDigits, int maxDigits) {
        this.decimalDigits = decimalDigits;
        this.maxDigits = maxDigits;
    }

    @Override
    public CharSequence filter(CharSequence source,
                               int start,
                               int end,
                               Spanned dest,
                               int dstart,
                               int dend) {


        if (maxDigits != 0 && dest.length() == maxDigits) {
            return "";
        }

        int dotPos = -1;
        int len = dest.length();
        for (int i = 0; i < len; i++) {
            char c = dest.charAt(i);
            if (c == '.' || c == ',') {
                dotPos = i;
                break;
            }
        }
        if (dotPos >= 0) {

            // protects against many dots
            if (source.equals(".") || source.equals(",")) {
                return "";
            }
            // if the text is entered before the dot
            if (dend <= dotPos) {
                return null;
            }
            if (len - dotPos > decimalDigits) {
                return "";
            }
        }

        return null;
    }

}
