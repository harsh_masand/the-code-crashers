package com.sharedcode.utils;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import com.sharedcode.widgets.FontCache;

import java.util.Arrays;

public class UIUtils {

    public static void removeFocus(View view) {

        view.setFocusableInTouchMode(false);
        view.setFocusable(false);
        view.setFocusableInTouchMode(true);
        view.setFocusable(true);
    }

    /**
     * Method to hide keyboard
     *
     * @param context
     * @param view
     * @return result of hide keyboard
     */
    public static boolean hideSoftKeyboard(Context context, View view) {
        if (view == null) {
            return false;
        }
        return ((InputMethodManager) context
                .getSystemService(Activity.INPUT_METHOD_SERVICE))
                .hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    /**
     * Method to show keyboard
     *
     * @param context
     * @param view
     * @return result of show keyboard
     */
    public static boolean showSoftKeyboard(Context context, View view) {
        if (view == null) {
            return false;
        }
        return ((InputMethodManager) context
                .getSystemService(Activity.INPUT_METHOD_SERVICE))
                .showSoftInput(view, 0);
    }

    /**
     * This method converts device specific pixels to density independent pixels.
     *
     * @param px      A value in px (pixels) unit. Which we need to convert into db
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent dp equivalent to px value
     */
    public static float convertPixelsToDp(float px, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / (metrics.densityDpi / 160f);
        return dp;
    }

    public static Typeface getFont(Context context, int mTextStyle) {
        switch (mTextStyle) {
            case 0:
                return FontCache.getRegularFont(context);
            case 1:
                return FontCache.getSemiBoldFont(context);
            case 2:
                return FontCache.getRegularHeaderFont(context);
            case 3:
                return FontCache.getBoldHeaderFont(context);
            case 4:
                return FontCache.getMediumHeaderFont(context);
            case 5:
                return FontCache.getLightItalicItalicFont(context);
            case 6:
                return FontCache.getItalicItalicFont(context);
            case 7:
                return FontCache.getQuicksandBold(context);
            case 8:
                return FontCache.getQuicksandLight(context);
            case 9:
                return FontCache.getQuicksandMedium(context);
            case 10:
                return FontCache.getQuicksandRegular(context);
            case 11:
                return FontCache.getKohinoorBold(context);
            default:
                return FontCache.getRegularFont(context);
        }
    }

    /***
     * @param str
     * @param n   no of last n characters to mask
     * @return
     */
    public static String mask(String str, int n) {
        if (!TextUtils.isEmpty(str)) {
            if (str.length() >= n) {
                int strLen = str.length();
                String unMasked = str.substring(strLen - n);
                char[] chars = new char[strLen - n];
                Arrays.fill(chars, 'X');
                String masked = String.valueOf(chars);
                return masked + unMasked;
            } else {
                return str;
            }
        } else
            return "";
    }

    public static String toCamelCase(String s) {
        if (s == null) {
            return s;
        }

        s = s.toUpperCase();
        int ns = s.length();
        char[] sa = s.toCharArray();
        if (ns > 0)
            sa[0] = Character.toUpperCase(sa[0]);
        for (int i = 1, n = ns; i < n; i++) {
            // 39 is apostrophe
            if (Character.isLetter(sa[i - 1]) == false && sa[i - 1] != 39)
                sa[i] = Character.toUpperCase(sa[i]);
            else
                sa[i] = Character.toLowerCase(sa[i]);
        }
        s = String.valueOf(sa);
        return s;
    }

    public static Bitmap drawableToBitmap(Drawable drawable) {
        if (drawable == null)
            return null;
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if (bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if (drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    public static void openCloseKeyBord(Activity activity, View view, boolean open) {
        if (open) {
            ((InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE)).showSoftInput(view, InputMethodManager.SHOW_FORCED);
        } else {
            ((InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
