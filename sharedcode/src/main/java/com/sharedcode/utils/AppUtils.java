package com.sharedcode.utils;

/**
 * Created by Akhil on 10/08/16.
 */

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AppOpsManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;
import com.sharedcode.R;
import com.sharedcode.helpers.LogHelper;

import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@SuppressWarnings("unchecked")
public class AppUtils {

    public static final String ACTION_SCANNER = "com.bnb.scannerConnected";
    public static final String SCANNER_TPYE = "SCANNER_TPYE";
    private static Toast mToast;

    public static String getAppVersion(Context context) {
        try {
            PackageManager manager = context.getPackageManager();
            PackageInfo info = manager.getPackageInfo(context.getPackageName(),
                    0);
            return info.versionName;
        } catch (Exception e) {
            return null;
        }
    }

    public static String getPhoneModelAndOs(Context context) {
        return "Device: " + Build.MANUFACTURER + " " + Build.MODEL
                + "\nOS: " + Build.VERSION.RELEASE + Build.VERSION_CODES.class.getFields()[android.os.Build.VERSION.SDK_INT].getName()
                + "\nVersion: " + getAppVersion(context);
    }

    public static boolean isOnline(Context context) {
        try {
            final ConnectivityManager cm = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = cm.getActiveNetworkInfo();
            return networkInfo != null && networkInfo.isConnectedOrConnecting();
        } catch (final SecurityException e) {
            return false;
        }
    }

    public static String getRupeeSymbol() {
        return "\u20b9";
    }

    public static String getDeviceId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static void showToast(final Context context, final String message, final boolean longDuration) {
        if (TextUtils.isEmpty(message))
            return;
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            public void run() {
                LayoutInflater inflater = LayoutInflater.from(context);
                View layout = inflater.inflate(R.layout.toast_layout, null);
                TextView text = (TextView) layout.findViewById(R.id.text);
                text.setText(message);
                Toast toast = mToast;
                if (toast == null)
                    mToast = toast = new Toast(context.getApplicationContext());
                toast.setDuration(longDuration ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT);
                toast.setView(layout);
                toast.show();
            }
        });

    }

    public static void showToast(Context context, int message,
                                 boolean longDuration) {
        if (message == 0)
            return;
        showToast(context, context.getString(message), longDuration);
    }

    public static int getScreenWidth(Context context) {
        Display display = ((Activity) context).getWindowManager()
                .getDefaultDisplay();
        return display.getWidth();
    }


    public static int getScreenHeight(Context context) {
        Display display = ((Activity) context).getWindowManager()
                .getDefaultDisplay();
        return display.getHeight();
    }

    public static int getStatusBarHeight(Context context) {
        int height = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            height = context.getResources().getDimensionPixelSize(resourceId);
        }
        return height;
    }

    public static int getNetworkTypeInt(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        if (info == null || !info.isConnected())
            return -1; // not connected
        if (info.getType() == ConnectivityManager.TYPE_WIFI)
            return -2;
        if (info.getType() == ConnectivityManager.TYPE_MOBILE) {
            info.getSubtype();
        }
        return -1;
    }

    public static String getNetworkTypeString(Context context) {

        switch (getNetworkTypeInt(context)) {
            case -2:
                return "WIFI";
            case -1:
                return "NA";
            case TelephonyManager.NETWORK_TYPE_GPRS:
            case TelephonyManager.NETWORK_TYPE_EDGE:
            case TelephonyManager.NETWORK_TYPE_CDMA:
            case TelephonyManager.NETWORK_TYPE_1xRTT:
            case TelephonyManager.NETWORK_TYPE_IDEN:
                return "2G";
            case TelephonyManager.NETWORK_TYPE_UMTS:
            case TelephonyManager.NETWORK_TYPE_EVDO_0:
            case TelephonyManager.NETWORK_TYPE_EVDO_A:
            case TelephonyManager.NETWORK_TYPE_HSDPA:
            case TelephonyManager.NETWORK_TYPE_HSUPA:
            case TelephonyManager.NETWORK_TYPE_HSPA:
            case TelephonyManager.NETWORK_TYPE_EVDO_B:
            case TelephonyManager.NETWORK_TYPE_EHRPD:
            case TelephonyManager.NETWORK_TYPE_HSPAP:
                return "3G";
            case TelephonyManager.NETWORK_TYPE_LTE:
                return "4G";
            default:
                return "NA";
        }
    }


    public static int convertDpToPixel(int dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    public static boolean isSupportedByCurrentVersion(int minSupportedVersion) {
        if (Build.VERSION.SDK_INT >= minSupportedVersion) {
            return true;
        }
        return false;
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static boolean isNotificationDisabled(Context context) {

        String CHECK_OP_NO_THROW = "checkOpNoThrow";
        String OP_POST_NOTIFICATION = "OP_POST_NOTIFICATION";
        String AppOpsManagerServiceName = "appops";
        int modeAllowed;
        if (isSupportedByCurrentVersion(Build.VERSION_CODES.KITKAT)) {
            modeAllowed = AppOpsManager.MODE_ALLOWED;
        } else {
            modeAllowed = 0;
        }

        @SuppressLint("WrongConstant") AppOpsManager mAppOps = (AppOpsManager) context.getSystemService(AppOpsManagerServiceName);

        ApplicationInfo appInfo = context.getApplicationInfo();


        String pkg = context.getApplicationContext().getPackageName();

        int uid = appInfo.uid;

        Class appOpsClass = null; /* Context.APP_OPS_MANAGER */

        try {

            appOpsClass = Class.forName(AppOpsManager.class.getName());

            Method checkOpNoThrowMethod = appOpsClass.getMethod(CHECK_OP_NO_THROW, Integer.TYPE, Integer.TYPE, String.class);

            Field opPostNotificationValue = appOpsClass.getDeclaredField(OP_POST_NOTIFICATION);
            int value = (int) opPostNotificationValue.get(Integer.class);

            return !((int) checkOpNoThrowMethod.invoke(mAppOps, value, uid, pkg) == modeAllowed);

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoClassDefFoundError e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean equals(String s1, String s2) {
        return s1 == s2 || (s1 != null && s1.equals(s2));
    }

    /**
     * Returns -1 if lhs < rhs. Returns 1 if lhs > rhs. Returns 0 if equal *
     **/
    public static int compareVersions(String lhs, String rhs) {
        try {
            if (lhs == null || lhs.length() == 0)
                return 1;
            if (rhs == null || rhs.length() == 0)
                return -1;
            String[] lhsParts = lhs.split("\\.");
            String[] rhsParts = rhs.split("\\.");
            int length = Math.max(lhsParts.length, rhsParts.length);
            for (int i = 0; i < length; i++) {
                int lhsPart = i < lhsParts.length ? Integer
                        .parseInt(lhsParts[i]) : 0;
                int rhsPart = i < rhsParts.length ? Integer
                        .parseInt(rhsParts[i]) : 0;
                if (lhsPart < rhsPart)
                    return -1;
                if (lhsPart > rhsPart)
                    return 1;
            }
            return 0;
        } catch (Exception ex) {
            return 0;
        }
    }

    public static void hideKeyboardImplicitly(Activity activity) {
        InputMethodManager keyboard = (InputMethodManager)
                activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        keyboard.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
    }

    public static void hideSoftKeyboard(Activity activity) {
        if (activity != null) {
            InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (activity.getCurrentFocus() != null && inputManager != null) {
                inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
                inputManager.hideSoftInputFromInputMethod(activity.getCurrentFocus().getWindowToken(), 0);
            }
        }
    }

    public static void closeKeyboard(Activity activity) {
        try {
            InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void openKeyboard(Activity activity) {
        try {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void dispatchTakePictureIntent(Activity activity, int type, String fileName) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Create the File where the photo should go
        File photoFile = null;
        try {
            photoFile = createImageFile(activity, fileName);
        } catch (IOException ex) {
            // Error occurred while creating the File
        }
        // Continue only if the File was successfully created
        if (photoFile != null) {
            Uri photoUri = Uri.fromFile(photoFile);
            String defaultCameraPackage = getDefaultCameraPackage(activity);
            // Forcing app to use the default camera app by specifying package of the application
            if (defaultCameraPackage != null) {
                takePictureIntent.setPackage(defaultCameraPackage);
            }
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
        }
        if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null) {
            activity.startActivityForResult(takePictureIntent, type);
        }
    }

    private static File createImageFile(Context context, String fileName) throws IOException {
        File storageDir = FileUtils.getPictureStorageLocation(context);

        return new File(storageDir, fileName);
    }

    public static Bitmap decodeFile(File f, int size) {
        try {
            //Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);

            //Find the correct scale value. It should be the power of 2.
            int width_tmp = o.outWidth, height_tmp = o.outHeight;
            int scale = 1;

            while (true) {
                if (width_tmp / 2 < size)
                    break;
                width_tmp /= 2;
                height_tmp /= 2;
                scale *= 2;
            }

            //Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }


    public static Bitmap compress(Context context, Bitmap bitmap, int size) {
        return decodeFile(convertBitmapToFile(context, bitmap), size);
    }


    public static File convertToFile(Context context, Bitmap bitmap, String name) {
        File filesDir = context.getFilesDir();
        File imageFile = new File(filesDir, name);

        OutputStream os;
        try {
            os = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
            os.flush();
            os.close();
        } catch (Exception e) {
            LogHelper.e("err", "Error writing bitmap", e);
        }
        return imageFile;
    }

    public static void showKeyboardImplicitly(Activity activity) {
        InputMethodManager keyboard = (InputMethodManager)
                activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        keyboard.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
    }


    private static String getDefaultCameraPackage(Activity activity) {
        String defaultCameraPackage = null;
        PackageManager packageManager = activity.getPackageManager();
        List<ApplicationInfo> list = packageManager.getInstalledApplications(PackageManager.GET_UNINSTALLED_PACKAGES);
        for (int n = 0; n < list.size(); n++) {
            if ((list.get(n).flags & ApplicationInfo.FLAG_SYSTEM) == 1) {
                LogHelper.d("TAG", "Installed Applications  : " + list.get(n).loadLabel(packageManager).toString());
                LogHelper.d("TAG", "package name  : " + list.get(n).packageName);
                if (list.get(n).loadLabel(packageManager).toString().equalsIgnoreCase("Camera")) {
                    defaultCameraPackage = list.get(n).packageName;
                    break;
                }
            }
        }
        return defaultCameraPackage;
    }


    public static File convertBitmapToFile(Context context, Bitmap bitmaps) {
        //create a file to write bitmap data
        // TODO: 16/01/17  Get timestamp from DateTimeUtils class
        File f = new File(context.getCacheDir(), String.valueOf(System.currentTimeMillis()));
        try {
            f.createNewFile();
            Bitmap bitmap = bitmaps;
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 40 /*ignored for PNG*/, bos);
            byte[] bitmapdata = bos.toByteArray();

            FileOutputStream fos = null;

            fos = new FileOutputStream(f);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return f;
    }

    /**
     * method use to make a call just need to pass number.
     *
     * @param context
     * @param number
     */
    public static void makeCall(Context context, String number) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + number));
        context.startActivity(intent);
    }


    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String string) {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(string, Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(string);
        }
        return result;
    }

    public static boolean isProxyEnable(Context context) {
        final boolean IS_ICS_OR_LATER = Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH;
        String proxyAddress = "";
        if (IS_ICS_OR_LATER) {
            proxyAddress = System.getProperty("http.proxyHost");
        } else {
            proxyAddress = android.net.Proxy.getHost(context);
        }
        return !TextUtils.isEmpty(proxyAddress);
    }

    public static String bitmapToBase64(Bitmap bitmap) {
        String encoded = null;
        if (bitmap != null) {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
        }

        return encoded;
    }

    public static boolean isUsbHost(Context context) {
        return context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_USB_HOST);
    }

    /***
     *
     * @return returns a six digit unique number for the day
     * using hhmmss here;
     */
    public static String getStan() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("HHmmss");
        Date now = new Date();

        return dateFormat.format(now);
    }

    public static boolean hasCoordinates(Location location) {
        return location != null && (location.getLatitude() != 0.0d || location.getLongitude() != 0.0d);
    }

    public static Intent getScannerIntent(int deviceType) {
        Intent intent = new Intent();
        intent.setAction(ACTION_SCANNER);
        intent.putExtra(SCANNER_TPYE, deviceType);
        return intent;
    }

    public static byte[] trim(byte[] bytes) {
        int i = bytes.length - 1;
        while (i >= 0 && bytes[i] == 0) {
            --i;
        }

        return Arrays.copyOf(bytes, i + 1);
    }

    public static boolean isGPSActive(Context context) {
        LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }
}
