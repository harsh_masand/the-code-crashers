package com.sharedcode.constants;


import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Akhil on 21/02/17.
 */

public class ScannerType {
    public static final int MORPHO = 1;
    public static final int STARTEK = 2;

    @ScannerTypes
    public static Integer getScanType(int type) {
        Integer scanType = null;

        switch (type) {
            case MORPHO:
                scanType = MORPHO;
                break;
            case STARTEK:
                scanType = STARTEK;
                break;
        }
        return scanType;
    }

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({MORPHO, STARTEK})
    public @interface ScannerTypes {
    }
}
