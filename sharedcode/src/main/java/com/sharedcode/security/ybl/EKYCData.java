package com.sharedcode.security.ybl;


public class EKYCData {

    public String AadhaarNo;
    public String DeviceCertExpiryDate;
    public String DeviceDataXml;
    public String DeviceFingerData;
    public String DeviceHmacXml;
    public String DeviceSerialNumber;
    public String DeviceSessionKey;
    public String DeviceTimeStamp;
    public String DeviceType = "1"; // Morpho Device
    public String DeviceVersionNumber;

    public String id;
    public boolean complete;
    public boolean hasError;
    public String errorDescription;
    //public EKYCResponse ekycResponse;

}
