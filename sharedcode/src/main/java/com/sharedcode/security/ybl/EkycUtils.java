package com.sharedcode.security.ybl;


import android.content.Context;
import org.bouncycastle.crypto.engines.AESEngine;
import org.bouncycastle.crypto.paddings.PKCS7Padding;
import org.bouncycastle.crypto.paddings.PaddedBufferedBlockCipher;
import org.bouncycastle.crypto.params.KeyParameter;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;


public class EkycUtils {


    //private static final String CERTIFICATE_FILE_NAME = "uidai_auth_stage.cer";

    static int[][] d = new int[][]
            {
                    {0, 1, 2, 3, 4, 5, 6, 7, 8, 9},
                    {1, 2, 3, 4, 0, 6, 7, 8, 9, 5},
                    {2, 3, 4, 0, 1, 7, 8, 9, 5, 6},
                    {3, 4, 0, 1, 2, 8, 9, 5, 6, 7},
                    {4, 0, 1, 2, 3, 9, 5, 6, 7, 8},
                    {5, 9, 8, 7, 6, 0, 4, 3, 2, 1},
                    {6, 5, 9, 8, 7, 1, 0, 4, 3, 2},
                    {7, 6, 5, 9, 8, 2, 1, 0, 4, 3},
                    {8, 7, 6, 5, 9, 3, 2, 1, 0, 4},
                    {9, 8, 7, 6, 5, 4, 3, 2, 1, 0}
            };
    // The permutation table
    static int[][] p = new int[][]
            {
                    {0, 1, 2, 3, 4, 5, 6, 7, 8, 9},
                    {1, 5, 7, 6, 2, 8, 3, 0, 9, 4},
                    {5, 8, 0, 3, 7, 9, 6, 1, 4, 2},
                    {8, 9, 1, 6, 0, 4, 3, 5, 2, 7},
                    {9, 4, 5, 3, 1, 2, 6, 8, 7, 0},
                    {4, 2, 8, 6, 5, 7, 3, 9, 0, 1},
                    {2, 7, 9, 3, 8, 0, 6, 4, 1, 5},
                    {7, 0, 4, 6, 9, 1, 3, 2, 5, 8}
            };
    // The inverse table
    static int[] inv = {0, 4, 3, 2, 1, 5, 6, 7, 8, 9};
    static HashMap<String, String> hmap = new HashMap<>();
    private static X509Certificate certFile;

    static {
        hmap.put("RIGHT_LITTLE", "10~RightLittle");
        hmap.put("RIGHT_RING", "9~RightRing");

        hmap.put("RIGHT_MIDDLE", "8~RightMiddle");

        hmap.put("RIGHT_INDEX", "7~RightIndex");

        hmap.put("RIGHT_THUMB", "6~RightThumb");

        hmap.put("LEFT_THUMB", "5~LeftThumb");

        hmap.put("LEFT_INDEX", "4~LeftIndex");

        hmap.put("LEFT_MIDDLE", "3~LeftMiddle");

        hmap.put("LEFT_RING", "2~LeftRing");

        hmap.put("LEFT_LITTLE", "1~LeftLittle");

    }

    /*
     * For a given number generates a EkycUtils digit
     *
     */
    public static String generateVerhoeff(String num) {

        int c = 0;
        int[] myArray = stringToReversedIntArray(num);

        for (int i = 0; i < myArray.length; i++) {
            c = d[c][p[((i + 1) % 8)][myArray[i]]];
        }

        return Integer.toString(inv[c]);
    }

    /*
     * Validates that an entered number is EkycUtils compliant.
     * NB: Make sure the check digit is the last one.
     */
    public static boolean validateVerhoeff(String num) {

        int c = 0;
        int[] myArray = stringToReversedIntArray(num);

        for (int i = 0; i < myArray.length; i++) {
            c = d[c][p[(i % 8)][myArray[i]]];
        }

        return (c == 0);
    }

    /*
     * Converts a string to a reversed integer array.
     */
    private static int[] stringToReversedIntArray(String num) {

        int[] myArray = new int[num.length()];

        for (int i = 0; i < num.length(); i++) {
            myArray[i] = Integer.parseInt(num.substring(i, i + 1));
        }

        myArray = reverse(myArray);

        return myArray;

    }

    /*
     * Reverses an int array
     */
    private static int[] reverse(int[] myArray) {
        int[] reversed = new int[myArray.length];

        for (int i = 0; i < myArray.length; i++) {
            reversed[i] = myArray[myArray.length - (i + 1)];
        }

        return reversed;
    }

    public static String getFingerNameForPosition(String posh) {


        return hmap.get(posh);
    }

    public static EKYCData getFormattedData(String aadharNumber, String templateData, String fingerName, Context context, int resourceId) {
        if (certFile == null) {
            try {
                certFile = readCertFile(context, resourceId);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (CertificateException e) {
                e.printStackTrace();
            }
        }


        try {

            /*String encodedTemplate = null;
            encodedTemplate = Base64.encode(templateData);*/

            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            String deviceTimeStamp = dateFormatter.format(new Date());

            String pidBlockXML = getPid(templateData, deviceTimeStamp, fingerName);

            KeyGenerator keyGen = KeyGenerator.getInstance("AES");
            keyGen.init(256);
            SecretKey sessionKey = keyGen.generateKey();


            Cipher rsaCipherInstance = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            rsaCipherInstance.init(Cipher.ENCRYPT_MODE, certFile.getPublicKey());

            String ecrryptedEncodedSessionKey = Base64.encode(rsaCipherInstance.doFinal(sessionKey.getEncoded()));

            PaddedBufferedBlockCipher aesCipherInstance = new PaddedBufferedBlockCipher(new AESEngine(), new PKCS7Padding());
            //Cipher aesCipherInstance = Cipher.getInstance("AES/ECB/PKCS5Padding");//AES/ECB/PKCS7PADDING
            //aesCipherInstance.init(Cipher.ENCRYPT_MODE, sessionKey);
            aesCipherInstance.init(true, new KeyParameter(sessionKey.getEncoded()));
            byte[] data = pidBlockXML.getBytes("UTF-8");
            int outputSize = aesCipherInstance.getOutputSize(data.length);
            //byte[] sessionkeyEncryptedPID = aesCipherInstance.doFinal(pidBlockXML.getBytes("UTF-8"));
            byte[] tempOP = new byte[outputSize];
            int processLen = aesCipherInstance.processBytes(data, 0, data.length, tempOP, 0);
            int outputLen = aesCipherInstance.doFinal(tempOP, processLen);

            byte[] result = new byte[processLen + outputLen];
            System.arraycopy(tempOP, 0, result, 0, result.length);


            String encryptedPID = Base64.encode(result);


            // re-set Cipher
            //aesCipherInstance.init(Cipher.ENCRYPT_MODE, sessionKey);
            aesCipherInstance.init(true, new KeyParameter(sessionKey.getEncoded()));
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(pidBlockXML.getBytes("UTF-8"));
            byte[] hmacBytes = md.digest();
            outputSize = aesCipherInstance.getOutputSize(hmacBytes.length);

            tempOP = new byte[outputSize];
            processLen = aesCipherInstance.processBytes(hmacBytes, 0, hmacBytes.length, tempOP, 0);
            outputLen = aesCipherInstance.doFinal(tempOP, processLen);

            result = new byte[processLen + outputLen];
            System.arraycopy(tempOP, 0, result, 0, result.length);

            // byte[] sessionKeyEncryptedhmac = aesCipherInstance.doFinal(hmacBytes);
            // String hmacValue = Base64.encode(sessionKeyEncryptedhmac);
            String hmacValue = Base64.encode(result);

            String deviceVersion = "11";//fpSensorDevice.getDeviceVirsion();

            String deviceSerial = "22";//fpSensorDevice.getDeviceSerialNum();

            String certExpDate = new SimpleDateFormat("yyyyMMdd").format(certFile.getNotAfter());


            final EKYCData ekycRequest = new EKYCData();
            ekycRequest.AadhaarNo = aadharNumber;
            ekycRequest.DeviceCertExpiryDate = certExpDate;
            ekycRequest.DeviceDataXml = encryptedPID;
            ekycRequest.DeviceHmacXml = hmacValue;
            ekycRequest.DeviceSerialNumber = deviceSerial;
            ekycRequest.DeviceSessionKey = ecrryptedEncodedSessionKey;
            ekycRequest.DeviceVersionNumber = deviceVersion;
            ekycRequest.DeviceTimeStamp = deviceTimeStamp;
            ekycRequest.DeviceFingerData = EkycUtils.getFingerNameForPosition(fingerName);
            return ekycRequest;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static X509Certificate readCertFile(String assetName) throws IOException, NoSuchAlgorithmException, CertificateException {

        InputStream fileInputStream = new FileInputStream(new File(assetName));
        //InputStream inputStream = ctx.getAssets().open(assetName);
        CertificateFactory f = CertificateFactory.getInstance("X.509");
        X509Certificate certificate = (X509Certificate) f.generateCertificate(fileInputStream);
        System.out.println("********* Exp DATE   " + certificate.getNotAfter());
        return certificate;
    }

    private static X509Certificate readCertFile(Context context, int fileResId) throws IOException, NoSuchAlgorithmException, CertificateException {

        InputStream fileInputStream = context.getResources().openRawResource(fileResId);
        //InputStream inputStream = ctx.getAssets().open(assetName);
        CertificateFactory f = CertificateFactory.getInstance("X.509");
        X509Certificate certificate = (X509Certificate) f.generateCertificate(fileInputStream);
        System.out.println("********* Exp DATE   " + certificate.getNotAfter());
        return certificate;
    }

    private static String getPid(String encodedTemplate, String dateTime, String fingerPosition) {

        String pid = "<Pid ts=\"" + dateTime + "\" ver=\"1.0\">";
        pid += "<Bios>";

        pid += "<Bio type=\"FMR\" posh=\"" + fingerPosition + "\">" + encodedTemplate + "</Bio>";
        pid += "</Bios>";
        pid += "</Pid>";
        return pid;
    }

}
