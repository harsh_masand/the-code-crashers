package com.sharedcode.security;

/**
 * Created by Akhil on 06/01/17.
 */

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.MessageDigest;
import java.util.Arrays;

public class AESMobile {
    public AESMobile() {
    }

    public static void main(String[] args) throws Exception {
        String text = "{\"service_id\":0,\"lat\":0,\"lng\":0,\"user_id\":\"1\",\"retailer_mobile_number\":\"5695665566\",\"retailer_aadhar_number\":\"2222\",\"finger_index\":\"3333\",\"finger_data\":\"8748294hdwkjh289u823u\"}";
        String s = (new AESMobile()).encrypt(text, "HG58YZ3CR9");
        String decodedtext = (new AESMobile()).decrypt(Base64.decode(s.getBytes()), "HG58YZ3CR9");
        System.out.println(s);
        System.out.println(decodedtext);
    }

    public String encrypt(String message, String password) throws Exception {
        MessageDigest md = MessageDigest.getInstance("md5");
        byte[] digestOfPassword = md.digest(password.getBytes("utf-8"));
        byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
        int key = 0;

        for (int iv = 16; key < 8; keyBytes[iv++] = keyBytes[key++]) {
            ;
        }

        SecretKeySpec var11 = new SecretKeySpec(keyBytes, "DESede");
        IvParameterSpec var12 = new IvParameterSpec(new byte[8]);
        Cipher cipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
        cipher.init(1, var11, var12);
        byte[] plainTextBytes = message.getBytes("utf-8");
        byte[] cipherText = cipher.doFinal(plainTextBytes);
        return Base64.encode(cipherText);
    }

    public String decrypt(byte[] message, String pass) throws Exception {
        MessageDigest md = MessageDigest.getInstance("md5");
        byte[] digestOfPassword = md.digest(pass.getBytes("utf-8"));
        byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
        int key = 0;

        for (int iv = 16; key < 8; keyBytes[iv++] = keyBytes[key++]) {
            ;
        }

        SecretKeySpec var10 = new SecretKeySpec(keyBytes, "DESede");
        IvParameterSpec var11 = new IvParameterSpec(new byte[8]);
        Cipher decipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
        decipher.init(2, var10, var11);
        byte[] plainText = decipher.doFinal(message);
        return new String(plainText, "UTF-8");
    }
}
