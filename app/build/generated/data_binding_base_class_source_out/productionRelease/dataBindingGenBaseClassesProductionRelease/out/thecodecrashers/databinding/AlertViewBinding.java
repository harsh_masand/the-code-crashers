package thecodecrashers.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.pnb.thecodecrashers.widgets.alert.AlertViewModel;
import com.sharedcode.widgets.CustomTextView;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class AlertViewBinding extends ViewDataBinding {
  @NonNull
  public final RelativeLayout bottomLayout;

  @NonNull
  public final CustomTextView leftActionButton;

  @NonNull
  public final CustomTextView rightActionButton;

  @NonNull
  public final CustomTextView subTitleRight;

  @NonNull
  public final LinearLayout title;

  @Bindable
  protected AlertViewModel mVm;

  protected AlertViewBinding(Object _bindingComponent, View _root, int _localFieldCount,
      RelativeLayout bottomLayout, CustomTextView leftActionButton,
      CustomTextView rightActionButton, CustomTextView subTitleRight, LinearLayout title) {
    super(_bindingComponent, _root, _localFieldCount);
    this.bottomLayout = bottomLayout;
    this.leftActionButton = leftActionButton;
    this.rightActionButton = rightActionButton;
    this.subTitleRight = subTitleRight;
    this.title = title;
  }

  public abstract void setVm(@Nullable AlertViewModel vm);

  @Nullable
  public AlertViewModel getVm() {
    return mVm;
  }

  @NonNull
  public static AlertViewBinding inflate(@NonNull LayoutInflater inflater, @Nullable ViewGroup root,
      boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.alert_view, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static AlertViewBinding inflate(@NonNull LayoutInflater inflater, @Nullable ViewGroup root,
      boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<AlertViewBinding>inflateInternal(inflater, thecodecrashers.R.layout.alert_view, root, attachToRoot, component);
  }

  @NonNull
  public static AlertViewBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.alert_view, null, false, component)
   */
  @NonNull
  @Deprecated
  public static AlertViewBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<AlertViewBinding>inflateInternal(inflater, thecodecrashers.R.layout.alert_view, null, false, component);
  }

  public static AlertViewBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static AlertViewBinding bind(@NonNull View view, @Nullable Object component) {
    return (AlertViewBinding)bind(component, view, thecodecrashers.R.layout.alert_view);
  }
}
