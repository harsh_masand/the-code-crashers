package thecodecrashers.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.pnb.thecodecrashers.errorscreens.ErrorScreenViewModel;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class LayoutErrorsBinding extends ViewDataBinding {
  @Bindable
  protected ErrorScreenViewModel mVm;

  protected LayoutErrorsBinding(Object _bindingComponent, View _root, int _localFieldCount) {
    super(_bindingComponent, _root, _localFieldCount);
  }

  public abstract void setVm(@Nullable ErrorScreenViewModel vm);

  @Nullable
  public ErrorScreenViewModel getVm() {
    return mVm;
  }

  @NonNull
  public static LayoutErrorsBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.layout_errors, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static LayoutErrorsBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<LayoutErrorsBinding>inflateInternal(inflater, thecodecrashers.R.layout.layout_errors, root, attachToRoot, component);
  }

  @NonNull
  public static LayoutErrorsBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.layout_errors, null, false, component)
   */
  @NonNull
  @Deprecated
  public static LayoutErrorsBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<LayoutErrorsBinding>inflateInternal(inflater, thecodecrashers.R.layout.layout_errors, null, false, component);
  }

  public static LayoutErrorsBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static LayoutErrorsBinding bind(@NonNull View view, @Nullable Object component) {
    return (LayoutErrorsBinding)bind(component, view, thecodecrashers.R.layout.layout_errors);
  }
}
