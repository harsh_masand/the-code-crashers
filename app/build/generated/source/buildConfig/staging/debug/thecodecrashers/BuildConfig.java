/**
 * Automatically generated file. DO NOT MODIFY
 */
package thecodecrashers;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.pnb.thecodecrashers.staging";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "staging";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "1.0";
  // Fields from product flavor: staging
  public static final String HOST = "http://13.232.107.221:82";
  public static final boolean IS_PROD = false;
}
