package thecodecrashers.databinding;
import thecodecrashers.R;
import thecodecrashers.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class AlertViewBindingImpl extends AlertViewBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.title, 18);
    }
    // views
    @NonNull
    private final android.widget.LinearLayout mboundView0;
    @NonNull
    private final android.widget.RelativeLayout mboundView1;
    @NonNull
    private final com.sharedcode.widgets.CustomTextView mboundView10;
    @NonNull
    private final com.sharedcode.widgets.CustomTextView mboundView11;
    @NonNull
    private final android.widget.LinearLayout mboundView12;
    @NonNull
    private final com.sharedcode.widgets.CustomTextView mboundView13;
    @NonNull
    private final com.sharedcode.widgets.CustomButton mboundView14;
    @NonNull
    private final com.sharedcode.widgets.CustomImageView mboundView2;
    @NonNull
    private final com.sharedcode.widgets.CustomImageView mboundView4;
    @NonNull
    private final com.sharedcode.widgets.CustomTextView mboundView5;
    @NonNull
    private final com.sharedcode.widgets.CustomTextView mboundView6;
    @NonNull
    private final android.widget.RelativeLayout mboundView7;
    @NonNull
    private final android.widget.LinearLayout mboundView8;
    @NonNull
    private final android.widget.ProgressBar mboundView9;
    // variables
    // values
    // listeners
    private OnClickListenerImpl mVmOnCloseAndroidViewViewOnClickListener;
    private OnClickListenerImpl1 mVmOnRightClickAndroidViewViewOnClickListener;
    private OnClickListenerImpl2 mVmOnLoadingErrorButtonClickAndroidViewViewOnClickListener;
    private OnClickListenerImpl3 mVmOnLeftClickAndroidViewViewOnClickListener;
    // Inverse Binding Event Handlers

    public AlertViewBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 19, sIncludes, sViewsWithIds));
    }
    private AlertViewBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 18
            , (android.widget.RelativeLayout) bindings[15]
            , (com.sharedcode.widgets.CustomTextView) bindings[16]
            , (com.sharedcode.widgets.CustomTextView) bindings[17]
            , (com.sharedcode.widgets.CustomTextView) bindings[3]
            , (android.widget.LinearLayout) bindings[18]
            );
        this.bottomLayout.setTag(null);
        this.leftActionButton.setTag(null);
        this.mboundView0 = (android.widget.LinearLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (android.widget.RelativeLayout) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView10 = (com.sharedcode.widgets.CustomTextView) bindings[10];
        this.mboundView10.setTag(null);
        this.mboundView11 = (com.sharedcode.widgets.CustomTextView) bindings[11];
        this.mboundView11.setTag(null);
        this.mboundView12 = (android.widget.LinearLayout) bindings[12];
        this.mboundView12.setTag(null);
        this.mboundView13 = (com.sharedcode.widgets.CustomTextView) bindings[13];
        this.mboundView13.setTag(null);
        this.mboundView14 = (com.sharedcode.widgets.CustomButton) bindings[14];
        this.mboundView14.setTag(null);
        this.mboundView2 = (com.sharedcode.widgets.CustomImageView) bindings[2];
        this.mboundView2.setTag(null);
        this.mboundView4 = (com.sharedcode.widgets.CustomImageView) bindings[4];
        this.mboundView4.setTag(null);
        this.mboundView5 = (com.sharedcode.widgets.CustomTextView) bindings[5];
        this.mboundView5.setTag(null);
        this.mboundView6 = (com.sharedcode.widgets.CustomTextView) bindings[6];
        this.mboundView6.setTag(null);
        this.mboundView7 = (android.widget.RelativeLayout) bindings[7];
        this.mboundView7.setTag(null);
        this.mboundView8 = (android.widget.LinearLayout) bindings[8];
        this.mboundView8.setTag(null);
        this.mboundView9 = (android.widget.ProgressBar) bindings[9];
        this.mboundView9.setTag(null);
        this.rightActionButton.setTag(null);
        this.subTitleRight.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x80000L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.vm == variableId) {
            setVm((com.pnb.thecodecrashers.widgets.alert.AlertViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setVm(@Nullable com.pnb.thecodecrashers.widgets.alert.AlertViewModel Vm) {
        this.mVm = Vm;
        synchronized(this) {
            mDirtyFlags |= 0x40000L;
        }
        notifyPropertyChanged(BR.vm);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeVmRActionTitle((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
            case 1 :
                return onChangeVmIndeterminateDrawable((androidx.lifecycle.MutableLiveData<android.graphics.drawable.Drawable>) object, fieldId);
            case 2 :
                return onChangeVmLoadingErrorButtonLabel((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
            case 3 :
                return onChangeVmIsCloseVisible((androidx.lifecycle.MutableLiveData<java.lang.Boolean>) object, fieldId);
            case 4 :
                return onChangeVmLActionTitle((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
            case 5 :
                return onChangeVmSubTitleRight((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
            case 6 :
                return onChangeVmLeftButtonTextColor((androidx.lifecycle.MutableLiveData<java.lang.Integer>) object, fieldId);
            case 7 :
                return onChangeVmTitle((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
            case 8 :
                return onChangeVmLoadingErrorMessage((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
            case 9 :
                return onChangeVmDisplayLoadingError((androidx.lifecycle.MutableLiveData<java.lang.Boolean>) object, fieldId);
            case 10 :
                return onChangeVmDisplayLoader((androidx.lifecycle.MutableLiveData<java.lang.Boolean>) object, fieldId);
            case 11 :
                return onChangeVmLoadingTitle((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
            case 12 :
                return onChangeVmLSelector((androidx.lifecycle.MutableLiveData<java.lang.Integer>) object, fieldId);
            case 13 :
                return onChangeVmLoadingInProgress((androidx.lifecycle.MutableLiveData<java.lang.Boolean>) object, fieldId);
            case 14 :
                return onChangeVmIconRes((androidx.lifecycle.MutableLiveData<java.lang.Integer>) object, fieldId);
            case 15 :
                return onChangeVmLoadingDescription((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
            case 16 :
                return onChangeVmRSelector((androidx.lifecycle.MutableLiveData<java.lang.Integer>) object, fieldId);
            case 17 :
                return onChangeVmRightButtonTextColor((androidx.lifecycle.MutableLiveData<java.lang.Integer>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeVmRActionTitle(androidx.lifecycle.MutableLiveData<java.lang.String> VmRActionTitle, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVmIndeterminateDrawable(androidx.lifecycle.MutableLiveData<android.graphics.drawable.Drawable> VmIndeterminateDrawable, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVmLoadingErrorButtonLabel(androidx.lifecycle.MutableLiveData<java.lang.String> VmLoadingErrorButtonLabel, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVmIsCloseVisible(androidx.lifecycle.MutableLiveData<java.lang.Boolean> VmIsCloseVisible, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVmLActionTitle(androidx.lifecycle.MutableLiveData<java.lang.String> VmLActionTitle, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x10L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVmSubTitleRight(androidx.lifecycle.MutableLiveData<java.lang.String> VmSubTitleRight, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x20L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVmLeftButtonTextColor(androidx.lifecycle.MutableLiveData<java.lang.Integer> VmLeftButtonTextColor, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x40L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVmTitle(androidx.lifecycle.MutableLiveData<java.lang.String> VmTitle, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x80L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVmLoadingErrorMessage(androidx.lifecycle.MutableLiveData<java.lang.String> VmLoadingErrorMessage, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x100L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVmDisplayLoadingError(androidx.lifecycle.MutableLiveData<java.lang.Boolean> VmDisplayLoadingError, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x200L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVmDisplayLoader(androidx.lifecycle.MutableLiveData<java.lang.Boolean> VmDisplayLoader, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x400L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVmLoadingTitle(androidx.lifecycle.MutableLiveData<java.lang.String> VmLoadingTitle, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x800L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVmLSelector(androidx.lifecycle.MutableLiveData<java.lang.Integer> VmLSelector, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1000L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVmLoadingInProgress(androidx.lifecycle.MutableLiveData<java.lang.Boolean> VmLoadingInProgress, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2000L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVmIconRes(androidx.lifecycle.MutableLiveData<java.lang.Integer> VmIconRes, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4000L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVmLoadingDescription(androidx.lifecycle.MutableLiveData<java.lang.String> VmLoadingDescription, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x8000L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVmRSelector(androidx.lifecycle.MutableLiveData<java.lang.Integer> VmRSelector, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x10000L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVmRightButtonTextColor(androidx.lifecycle.MutableLiveData<java.lang.Integer> VmRightButtonTextColor, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x20000L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        androidx.lifecycle.MutableLiveData<java.lang.String> vmRActionTitle = null;
        java.lang.Integer vmRightButtonTextColorGetValue = null;
        boolean vmDisplayLoader = false;
        java.lang.Integer vmIconResGetValue = null;
        androidx.lifecycle.MutableLiveData<android.graphics.drawable.Drawable> vmIndeterminateDrawable = null;
        java.lang.Boolean vmDisplayLoadingErrorGetValue = null;
        boolean androidxDatabindingViewDataBindingSafeUnboxVmLoadingInProgressVmDisplayLoadingErrorBooleanFalse = false;
        android.view.View.OnClickListener vmOnCloseAndroidViewViewOnClickListener = null;
        boolean vmLoadingInProgress = false;
        java.lang.String vmLoadingDescriptionGetValue = null;
        androidx.lifecycle.MutableLiveData<java.lang.String> vmLoadingErrorButtonLabel = null;
        boolean vmIconResInt0 = false;
        androidx.lifecycle.MutableLiveData<java.lang.Boolean> vmIsCloseVisible = null;
        java.lang.String vmLActionTitleGetValue = null;
        com.pnb.thecodecrashers.widgets.alert.AlertViewModel vm = mVm;
        boolean androidxDatabindingViewDataBindingSafeUnboxVmIsCloseVisibleGetValue = false;
        android.graphics.drawable.Drawable vmIndeterminateDrawableGetValue = null;
        boolean androidxDatabindingViewDataBindingSafeUnboxVmLoadingInProgressVmDisplayLoadingErrorBooleanFalseVmDisplayLoaderBooleanFalse = false;
        java.lang.String vmLoadingErrorButtonLabelGetValue = null;
        boolean vmLoadingInProgressBooleanTrueVmDisplayLoadingError = false;
        java.lang.Integer vmRSelectorGetValue = null;
        androidx.lifecycle.MutableLiveData<java.lang.String> vmLActionTitle = null;
        boolean textUtilsIsEmptyVmLoadingDescription = false;
        androidx.lifecycle.MutableLiveData<java.lang.String> vmSubTitleRight = null;
        android.view.View.OnClickListener vmOnRightClickAndroidViewViewOnClickListener = null;
        boolean textUtilsIsEmptyVmDescription = false;
        androidx.lifecycle.MutableLiveData<java.lang.Integer> vmLeftButtonTextColor = null;
        android.view.View.OnClickListener vmOnLoadingErrorButtonClickAndroidViewViewOnClickListener = null;
        boolean vmDisplayLoadingError = false;
        android.view.View.OnClickListener vmOnLeftClickAndroidViewViewOnClickListener = null;
        java.lang.String vmTitleGetValue = null;
        boolean androidxDatabindingViewDataBindingSafeUnboxVmLoadingInProgressGetValue = false;
        boolean vmLoadingInProgressBooleanTrueVmDisplayLoadingErrorBooleanTrueVmDisplayLoader = false;
        java.lang.Boolean vmIsCloseVisibleGetValue = null;
        androidx.lifecycle.MutableLiveData<java.lang.String> vmTitle = null;
        boolean vmLoadingInProgressVmDisplayLoadingErrorBooleanFalseVmDisplayLoaderBooleanFalseTextUtilsIsEmptyVmDescriptionBooleanFalse = false;
        java.lang.Boolean vmDisplayLoaderGetValue = null;
        androidx.lifecycle.MutableLiveData<java.lang.String> vmLoadingErrorMessage = null;
        boolean androidxDatabindingViewDataBindingSafeUnboxVmDisplayLoaderGetValue = false;
        androidx.lifecycle.MutableLiveData<java.lang.Boolean> VmDisplayLoadingError1 = null;
        androidx.lifecycle.MutableLiveData<java.lang.Boolean> VmDisplayLoader1 = null;
        boolean vmLoadingInProgressVmDisplayLoadingErrorBooleanFalse = false;
        boolean vmRActionTitleJavaLangObjectNull = false;
        int androidxDatabindingViewDataBindingSafeUnboxVmLeftButtonTextColorGetValue = 0;
        int androidxDatabindingViewDataBindingSafeUnboxVmRightButtonTextColorGetValue = 0;
        android.text.Spanned vmDescription = null;
        androidx.lifecycle.MutableLiveData<java.lang.String> vmLoadingTitle = null;
        int androidxDatabindingViewDataBindingSafeUnboxVmRSelectorGetValue = 0;
        boolean androidxDatabindingViewDataBindingSafeUnboxVmDisplayLoadingErrorGetValue = false;
        androidx.lifecycle.MutableLiveData<java.lang.Integer> vmLSelector = null;
        boolean TextUtilsIsEmptyVmDescription1 = false;
        java.lang.Boolean vmLoadingInProgressGetValue = null;
        java.lang.String vmLoadingTitleGetValue = null;
        androidx.lifecycle.MutableLiveData<java.lang.Boolean> VmLoadingInProgress1 = null;
        int androidxDatabindingViewDataBindingSafeUnboxVmIconResGetValue = 0;
        boolean vmLActionTitleJavaLangObjectNull = false;
        androidx.lifecycle.MutableLiveData<java.lang.Integer> vmIconRes = null;
        androidx.lifecycle.MutableLiveData<java.lang.String> vmLoadingDescription = null;
        java.lang.String vmSubTitleRightGetValue = null;
        int androidxDatabindingViewDataBindingSafeUnboxVmLSelectorGetValue = 0;
        androidx.lifecycle.MutableLiveData<java.lang.Integer> vmRSelector = null;
        boolean vmLoadingInProgressVmDisplayLoadingErrorBooleanFalseVmDisplayLoaderBooleanFalse = false;
        java.lang.String vmRActionTitleGetValue = null;
        boolean TextUtilsIsEmptyVmLoadingDescription1 = false;
        java.lang.String vmLoadingErrorMessageGetValue = null;
        androidx.lifecycle.MutableLiveData<java.lang.Integer> vmRightButtonTextColor = null;
        java.lang.Integer vmLeftButtonTextColorGetValue = null;
        java.lang.Integer vmLSelectorGetValue = null;

        if ((dirtyFlags & 0xfffffL) != 0) {


            if ((dirtyFlags & 0xc0001L) != 0) {

                    if (vm != null) {
                        // read vm.rActionTitle
                        vmRActionTitle = vm.rActionTitle;
                    }
                    updateLiveDataRegistration(0, vmRActionTitle);


                    if (vmRActionTitle != null) {
                        // read vm.rActionTitle.getValue()
                        vmRActionTitleGetValue = vmRActionTitle.getValue();
                    }


                    // read vm.rActionTitle.getValue() != null
                    vmRActionTitleJavaLangObjectNull = (vmRActionTitleGetValue) != (null);
            }
            if ((dirtyFlags & 0xc0002L) != 0) {

                    if (vm != null) {
                        // read vm.indeterminateDrawable
                        vmIndeterminateDrawable = vm.indeterminateDrawable;
                    }
                    updateLiveDataRegistration(1, vmIndeterminateDrawable);


                    if (vmIndeterminateDrawable != null) {
                        // read vm.indeterminateDrawable.getValue()
                        vmIndeterminateDrawableGetValue = vmIndeterminateDrawable.getValue();
                    }
            }
            if ((dirtyFlags & 0xc0000L) != 0) {

                    if (vm != null) {
                        // read vm::onClose
                        vmOnCloseAndroidViewViewOnClickListener = (((mVmOnCloseAndroidViewViewOnClickListener == null) ? (mVmOnCloseAndroidViewViewOnClickListener = new OnClickListenerImpl()) : mVmOnCloseAndroidViewViewOnClickListener).setValue(vm));
                        // read vm::onRightClick
                        vmOnRightClickAndroidViewViewOnClickListener = (((mVmOnRightClickAndroidViewViewOnClickListener == null) ? (mVmOnRightClickAndroidViewViewOnClickListener = new OnClickListenerImpl1()) : mVmOnRightClickAndroidViewViewOnClickListener).setValue(vm));
                        // read vm::onLoadingErrorButtonClick
                        vmOnLoadingErrorButtonClickAndroidViewViewOnClickListener = (((mVmOnLoadingErrorButtonClickAndroidViewViewOnClickListener == null) ? (mVmOnLoadingErrorButtonClickAndroidViewViewOnClickListener = new OnClickListenerImpl2()) : mVmOnLoadingErrorButtonClickAndroidViewViewOnClickListener).setValue(vm));
                        // read vm::onLeftClick
                        vmOnLeftClickAndroidViewViewOnClickListener = (((mVmOnLeftClickAndroidViewViewOnClickListener == null) ? (mVmOnLeftClickAndroidViewViewOnClickListener = new OnClickListenerImpl3()) : mVmOnLeftClickAndroidViewViewOnClickListener).setValue(vm));
                        // read vm.description
                        vmDescription = vm.description;
                    }
            }
            if ((dirtyFlags & 0xc0004L) != 0) {

                    if (vm != null) {
                        // read vm.loadingErrorButtonLabel
                        vmLoadingErrorButtonLabel = vm.loadingErrorButtonLabel;
                    }
                    updateLiveDataRegistration(2, vmLoadingErrorButtonLabel);


                    if (vmLoadingErrorButtonLabel != null) {
                        // read vm.loadingErrorButtonLabel.getValue()
                        vmLoadingErrorButtonLabelGetValue = vmLoadingErrorButtonLabel.getValue();
                    }
            }
            if ((dirtyFlags & 0xc0008L) != 0) {

                    if (vm != null) {
                        // read vm.isCloseVisible
                        vmIsCloseVisible = vm.isCloseVisible;
                    }
                    updateLiveDataRegistration(3, vmIsCloseVisible);


                    if (vmIsCloseVisible != null) {
                        // read vm.isCloseVisible.getValue()
                        vmIsCloseVisibleGetValue = vmIsCloseVisible.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(vm.isCloseVisible.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxVmIsCloseVisibleGetValue = androidx.databinding.ViewDataBinding.safeUnbox(vmIsCloseVisibleGetValue);
            }
            if ((dirtyFlags & 0xc0010L) != 0) {

                    if (vm != null) {
                        // read vm.lActionTitle
                        vmLActionTitle = vm.lActionTitle;
                    }
                    updateLiveDataRegistration(4, vmLActionTitle);


                    if (vmLActionTitle != null) {
                        // read vm.lActionTitle.getValue()
                        vmLActionTitleGetValue = vmLActionTitle.getValue();
                    }


                    // read vm.lActionTitle.getValue() != null
                    vmLActionTitleJavaLangObjectNull = (vmLActionTitleGetValue) != (null);
            }
            if ((dirtyFlags & 0xc0020L) != 0) {

                    if (vm != null) {
                        // read vm.subTitleRight
                        vmSubTitleRight = vm.subTitleRight;
                    }
                    updateLiveDataRegistration(5, vmSubTitleRight);


                    if (vmSubTitleRight != null) {
                        // read vm.subTitleRight.getValue()
                        vmSubTitleRightGetValue = vmSubTitleRight.getValue();
                    }
            }
            if ((dirtyFlags & 0xc0040L) != 0) {

                    if (vm != null) {
                        // read vm.leftButtonTextColor
                        vmLeftButtonTextColor = vm.leftButtonTextColor;
                    }
                    updateLiveDataRegistration(6, vmLeftButtonTextColor);


                    if (vmLeftButtonTextColor != null) {
                        // read vm.leftButtonTextColor.getValue()
                        vmLeftButtonTextColorGetValue = vmLeftButtonTextColor.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(vm.leftButtonTextColor.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxVmLeftButtonTextColorGetValue = androidx.databinding.ViewDataBinding.safeUnbox(vmLeftButtonTextColorGetValue);
            }
            if ((dirtyFlags & 0xc0080L) != 0) {

                    if (vm != null) {
                        // read vm.title
                        vmTitle = vm.title;
                    }
                    updateLiveDataRegistration(7, vmTitle);


                    if (vmTitle != null) {
                        // read vm.title.getValue()
                        vmTitleGetValue = vmTitle.getValue();
                    }
            }
            if ((dirtyFlags & 0xc0100L) != 0) {

                    if (vm != null) {
                        // read vm.loadingErrorMessage
                        vmLoadingErrorMessage = vm.loadingErrorMessage;
                    }
                    updateLiveDataRegistration(8, vmLoadingErrorMessage);


                    if (vmLoadingErrorMessage != null) {
                        // read vm.loadingErrorMessage.getValue()
                        vmLoadingErrorMessageGetValue = vmLoadingErrorMessage.getValue();
                    }
            }
            if ((dirtyFlags & 0xc0200L) != 0) {

                    if (vm != null) {
                        // read vm.displayLoadingError
                        VmDisplayLoadingError1 = vm.displayLoadingError;
                    }
                    updateLiveDataRegistration(9, VmDisplayLoadingError1);


                    if (VmDisplayLoadingError1 != null) {
                        // read vm.displayLoadingError.getValue()
                        vmDisplayLoadingErrorGetValue = VmDisplayLoadingError1.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(vm.displayLoadingError.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxVmDisplayLoadingErrorGetValue = androidx.databinding.ViewDataBinding.safeUnbox(vmDisplayLoadingErrorGetValue);
            }
            if ((dirtyFlags & 0xc0400L) != 0) {

                    if (vm != null) {
                        // read vm.displayLoader
                        VmDisplayLoader1 = vm.displayLoader;
                    }
                    updateLiveDataRegistration(10, VmDisplayLoader1);


                    if (VmDisplayLoader1 != null) {
                        // read vm.displayLoader.getValue()
                        vmDisplayLoaderGetValue = VmDisplayLoader1.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(vm.displayLoader.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxVmDisplayLoaderGetValue = androidx.databinding.ViewDataBinding.safeUnbox(vmDisplayLoaderGetValue);
            }
            if ((dirtyFlags & 0xc0800L) != 0) {

                    if (vm != null) {
                        // read vm.loadingTitle
                        vmLoadingTitle = vm.loadingTitle;
                    }
                    updateLiveDataRegistration(11, vmLoadingTitle);


                    if (vmLoadingTitle != null) {
                        // read vm.loadingTitle.getValue()
                        vmLoadingTitleGetValue = vmLoadingTitle.getValue();
                    }
            }
            if ((dirtyFlags & 0xc1000L) != 0) {

                    if (vm != null) {
                        // read vm.lSelector
                        vmLSelector = vm.lSelector;
                    }
                    updateLiveDataRegistration(12, vmLSelector);


                    if (vmLSelector != null) {
                        // read vm.lSelector.getValue()
                        vmLSelectorGetValue = vmLSelector.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(vm.lSelector.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxVmLSelectorGetValue = androidx.databinding.ViewDataBinding.safeUnbox(vmLSelectorGetValue);
            }
            if ((dirtyFlags & 0xc2600L) != 0) {

                    if (vm != null) {
                        // read vm.loadingInProgress
                        VmLoadingInProgress1 = vm.loadingInProgress;
                    }
                    updateLiveDataRegistration(13, VmLoadingInProgress1);


                    if (VmLoadingInProgress1 != null) {
                        // read vm.loadingInProgress.getValue()
                        vmLoadingInProgressGetValue = VmLoadingInProgress1.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(vm.loadingInProgress.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxVmLoadingInProgressGetValue = androidx.databinding.ViewDataBinding.safeUnbox(vmLoadingInProgressGetValue);
                if((dirtyFlags & 0xc2600L) != 0) {
                    if(androidxDatabindingViewDataBindingSafeUnboxVmLoadingInProgressGetValue) {
                            dirtyFlags |= 0x200000L;
                    }
                    else {
                            dirtyFlags |= 0x100000L;
                    }
                }


                    // read !androidx.databinding.ViewDataBinding.safeUnbox(vm.loadingInProgress.getValue())
                    vmLoadingInProgress = !androidxDatabindingViewDataBindingSafeUnboxVmLoadingInProgressGetValue;
                if((dirtyFlags & 0xc2600L) != 0) {
                    if(vmLoadingInProgress) {
                            dirtyFlags |= 0x8000000L;
                    }
                    else {
                            dirtyFlags |= 0x4000000L;
                    }
                }
            }
            if ((dirtyFlags & 0xc4000L) != 0) {

                    if (vm != null) {
                        // read vm.iconRes
                        vmIconRes = vm.iconRes;
                    }
                    updateLiveDataRegistration(14, vmIconRes);


                    if (vmIconRes != null) {
                        // read vm.iconRes.getValue()
                        vmIconResGetValue = vmIconRes.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(vm.iconRes.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxVmIconResGetValue = androidx.databinding.ViewDataBinding.safeUnbox(vmIconResGetValue);


                    // read androidx.databinding.ViewDataBinding.safeUnbox(vm.iconRes.getValue()) != 0
                    vmIconResInt0 = (androidxDatabindingViewDataBindingSafeUnboxVmIconResGetValue) != (0);
            }
            if ((dirtyFlags & 0xc8000L) != 0) {

                    if (vm != null) {
                        // read vm.loadingDescription
                        vmLoadingDescription = vm.loadingDescription;
                    }
                    updateLiveDataRegistration(15, vmLoadingDescription);


                    if (vmLoadingDescription != null) {
                        // read vm.loadingDescription.getValue()
                        vmLoadingDescriptionGetValue = vmLoadingDescription.getValue();
                    }


                    // read TextUtils.isEmpty(vm.loadingDescription.getValue())
                    textUtilsIsEmptyVmLoadingDescription = android.text.TextUtils.isEmpty(vmLoadingDescriptionGetValue);


                    // read !TextUtils.isEmpty(vm.loadingDescription.getValue())
                    TextUtilsIsEmptyVmLoadingDescription1 = !textUtilsIsEmptyVmLoadingDescription;
            }
            if ((dirtyFlags & 0xd0000L) != 0) {

                    if (vm != null) {
                        // read vm.rSelector
                        vmRSelector = vm.rSelector;
                    }
                    updateLiveDataRegistration(16, vmRSelector);


                    if (vmRSelector != null) {
                        // read vm.rSelector.getValue()
                        vmRSelectorGetValue = vmRSelector.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(vm.rSelector.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxVmRSelectorGetValue = androidx.databinding.ViewDataBinding.safeUnbox(vmRSelectorGetValue);
            }
            if ((dirtyFlags & 0xe0000L) != 0) {

                    if (vm != null) {
                        // read vm.rightButtonTextColor
                        vmRightButtonTextColor = vm.rightButtonTextColor;
                    }
                    updateLiveDataRegistration(17, vmRightButtonTextColor);


                    if (vmRightButtonTextColor != null) {
                        // read vm.rightButtonTextColor.getValue()
                        vmRightButtonTextColorGetValue = vmRightButtonTextColor.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(vm.rightButtonTextColor.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxVmRightButtonTextColorGetValue = androidx.databinding.ViewDataBinding.safeUnbox(vmRightButtonTextColorGetValue);
            }
        }
        // batch finished

        if ((dirtyFlags & 0x8100000L) != 0) {

                if (vm != null) {
                    // read vm.displayLoadingError
                    VmDisplayLoadingError1 = vm.displayLoadingError;
                }
                updateLiveDataRegistration(9, VmDisplayLoadingError1);


                if (VmDisplayLoadingError1 != null) {
                    // read vm.displayLoadingError.getValue()
                    vmDisplayLoadingErrorGetValue = VmDisplayLoadingError1.getValue();
                }


                // read androidx.databinding.ViewDataBinding.safeUnbox(vm.displayLoadingError.getValue())
                androidxDatabindingViewDataBindingSafeUnboxVmDisplayLoadingErrorGetValue = androidx.databinding.ViewDataBinding.safeUnbox(vmDisplayLoadingErrorGetValue);

            if ((dirtyFlags & 0x8000000L) != 0) {

                    // read !androidx.databinding.ViewDataBinding.safeUnbox(vm.displayLoadingError.getValue())
                    vmDisplayLoadingError = !androidxDatabindingViewDataBindingSafeUnboxVmDisplayLoadingErrorGetValue;
            }
        }

        if ((dirtyFlags & 0xc2600L) != 0) {

                // read androidx.databinding.ViewDataBinding.safeUnbox(vm.loadingInProgress.getValue()) ? true : androidx.databinding.ViewDataBinding.safeUnbox(vm.displayLoadingError.getValue())
                vmLoadingInProgressBooleanTrueVmDisplayLoadingError = ((androidxDatabindingViewDataBindingSafeUnboxVmLoadingInProgressGetValue) ? (true) : (androidxDatabindingViewDataBindingSafeUnboxVmDisplayLoadingErrorGetValue));
                // read !androidx.databinding.ViewDataBinding.safeUnbox(vm.loadingInProgress.getValue()) ? !androidx.databinding.ViewDataBinding.safeUnbox(vm.displayLoadingError.getValue()) : false
                vmLoadingInProgressVmDisplayLoadingErrorBooleanFalse = ((vmLoadingInProgress) ? (vmDisplayLoadingError) : (false));
            if((dirtyFlags & 0xc2600L) != 0) {
                if(vmLoadingInProgressBooleanTrueVmDisplayLoadingError) {
                        dirtyFlags |= 0x800000L;
                }
                else {
                        dirtyFlags |= 0x400000L;
                }
            }
            if((dirtyFlags & 0xc2600L) != 0) {
                if(vmLoadingInProgressVmDisplayLoadingErrorBooleanFalse) {
                        dirtyFlags |= 0x20000000L;
                }
                else {
                        dirtyFlags |= 0x10000000L;
                }
            }

            if ((dirtyFlags & 0xc2200L) != 0) {

                    // read androidx.databinding.ViewDataBinding.safeUnbox(!androidx.databinding.ViewDataBinding.safeUnbox(vm.loadingInProgress.getValue()) ? !androidx.databinding.ViewDataBinding.safeUnbox(vm.displayLoadingError.getValue()) : false)
                    androidxDatabindingViewDataBindingSafeUnboxVmLoadingInProgressVmDisplayLoadingErrorBooleanFalse = androidx.databinding.ViewDataBinding.safeUnbox(vmLoadingInProgressVmDisplayLoadingErrorBooleanFalse);
            }
        }
        // batch finished

        if ((dirtyFlags & 0x20400000L) != 0) {

                if (vm != null) {
                    // read vm.displayLoader
                    VmDisplayLoader1 = vm.displayLoader;
                }
                updateLiveDataRegistration(10, VmDisplayLoader1);


                if (VmDisplayLoader1 != null) {
                    // read vm.displayLoader.getValue()
                    vmDisplayLoaderGetValue = VmDisplayLoader1.getValue();
                }


                // read androidx.databinding.ViewDataBinding.safeUnbox(vm.displayLoader.getValue())
                androidxDatabindingViewDataBindingSafeUnboxVmDisplayLoaderGetValue = androidx.databinding.ViewDataBinding.safeUnbox(vmDisplayLoaderGetValue);

            if ((dirtyFlags & 0x20000000L) != 0) {

                    // read !androidx.databinding.ViewDataBinding.safeUnbox(vm.displayLoader.getValue())
                    vmDisplayLoader = !androidxDatabindingViewDataBindingSafeUnboxVmDisplayLoaderGetValue;
            }
        }

        if ((dirtyFlags & 0xc2600L) != 0) {

                // read androidx.databinding.ViewDataBinding.safeUnbox(vm.loadingInProgress.getValue()) ? true : androidx.databinding.ViewDataBinding.safeUnbox(vm.displayLoadingError.getValue()) ? true : androidx.databinding.ViewDataBinding.safeUnbox(vm.displayLoader.getValue())
                vmLoadingInProgressBooleanTrueVmDisplayLoadingErrorBooleanTrueVmDisplayLoader = ((vmLoadingInProgressBooleanTrueVmDisplayLoadingError) ? (true) : (androidxDatabindingViewDataBindingSafeUnboxVmDisplayLoaderGetValue));
                // read !androidx.databinding.ViewDataBinding.safeUnbox(vm.loadingInProgress.getValue()) ? !androidx.databinding.ViewDataBinding.safeUnbox(vm.displayLoadingError.getValue()) : false ? !androidx.databinding.ViewDataBinding.safeUnbox(vm.displayLoader.getValue()) : false
                vmLoadingInProgressVmDisplayLoadingErrorBooleanFalseVmDisplayLoaderBooleanFalse = ((vmLoadingInProgressVmDisplayLoadingErrorBooleanFalse) ? (vmDisplayLoader) : (false));
            if((dirtyFlags & 0xc2600L) != 0) {
                if(vmLoadingInProgressVmDisplayLoadingErrorBooleanFalseVmDisplayLoaderBooleanFalse) {
                        dirtyFlags |= 0x2000000L;
                }
                else {
                        dirtyFlags |= 0x1000000L;
                }
            }


                // read androidx.databinding.ViewDataBinding.safeUnbox(!androidx.databinding.ViewDataBinding.safeUnbox(vm.loadingInProgress.getValue()) ? !androidx.databinding.ViewDataBinding.safeUnbox(vm.displayLoadingError.getValue()) : false ? !androidx.databinding.ViewDataBinding.safeUnbox(vm.displayLoader.getValue()) : false)
                androidxDatabindingViewDataBindingSafeUnboxVmLoadingInProgressVmDisplayLoadingErrorBooleanFalseVmDisplayLoaderBooleanFalse = androidx.databinding.ViewDataBinding.safeUnbox(vmLoadingInProgressVmDisplayLoadingErrorBooleanFalseVmDisplayLoaderBooleanFalse);
        }
        // batch finished

        if ((dirtyFlags & 0x2000000L) != 0) {

                if (vm != null) {
                    // read vm.description
                    vmDescription = vm.description;
                }


                // read TextUtils.isEmpty(vm.description)
                textUtilsIsEmptyVmDescription = android.text.TextUtils.isEmpty(vmDescription);


                // read !TextUtils.isEmpty(vm.description)
                TextUtilsIsEmptyVmDescription1 = !textUtilsIsEmptyVmDescription;
        }

        if ((dirtyFlags & 0xc2600L) != 0) {

                // read !androidx.databinding.ViewDataBinding.safeUnbox(vm.loadingInProgress.getValue()) ? !androidx.databinding.ViewDataBinding.safeUnbox(vm.displayLoadingError.getValue()) : false ? !androidx.databinding.ViewDataBinding.safeUnbox(vm.displayLoader.getValue()) : false ? !TextUtils.isEmpty(vm.description) : false
                vmLoadingInProgressVmDisplayLoadingErrorBooleanFalseVmDisplayLoaderBooleanFalseTextUtilsIsEmptyVmDescriptionBooleanFalse = ((vmLoadingInProgressVmDisplayLoadingErrorBooleanFalseVmDisplayLoaderBooleanFalse) ? (TextUtilsIsEmptyVmDescription1) : (false));
        }
        // batch finished
        if ((dirtyFlags & 0xc2200L) != 0) {
            // api target 1

            com.pnb.thecodecrashers.utils.bindingUtils.BindingUtils.setVisibility(this.bottomLayout, androidxDatabindingViewDataBindingSafeUnboxVmLoadingInProgressVmDisplayLoadingErrorBooleanFalse);
        }
        if ((dirtyFlags & 0xc0000L) != 0) {
            // api target 1

            this.leftActionButton.setOnClickListener(vmOnLeftClickAndroidViewViewOnClickListener);
            this.mboundView14.setOnClickListener(vmOnLoadingErrorButtonClickAndroidViewViewOnClickListener);
            this.mboundView2.setOnClickListener(vmOnCloseAndroidViewViewOnClickListener);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView6, vmDescription);
            this.rightActionButton.setOnClickListener(vmOnRightClickAndroidViewViewOnClickListener);
        }
        if ((dirtyFlags & 0xc0010L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.leftActionButton, vmLActionTitleGetValue);
            com.pnb.thecodecrashers.utils.bindingUtils.BindingUtils.setVisibility(this.leftActionButton, vmLActionTitleJavaLangObjectNull);
        }
        if ((dirtyFlags & 0xc1000L) != 0) {
            // api target 1

            com.pnb.thecodecrashers.utils.bindingUtils.BindingUtils.bindBackgroud(this.leftActionButton, androidxDatabindingViewDataBindingSafeUnboxVmLSelectorGetValue);
        }
        if ((dirtyFlags & 0xc0040L) != 0) {
            // api target 1

            com.pnb.thecodecrashers.utils.bindingUtils.BindingUtils.setColor(this.leftActionButton, androidxDatabindingViewDataBindingSafeUnboxVmLeftButtonTextColorGetValue);
        }
        if ((dirtyFlags & 0xc2600L) != 0) {
            // api target 1

            com.pnb.thecodecrashers.utils.bindingUtils.BindingUtils.setVisibility(this.mboundView1, androidxDatabindingViewDataBindingSafeUnboxVmLoadingInProgressVmDisplayLoadingErrorBooleanFalseVmDisplayLoaderBooleanFalse);
            com.pnb.thecodecrashers.utils.bindingUtils.BindingUtils.setVisibility(this.mboundView6, vmLoadingInProgressVmDisplayLoadingErrorBooleanFalseVmDisplayLoaderBooleanFalseTextUtilsIsEmptyVmDescriptionBooleanFalse);
            com.pnb.thecodecrashers.utils.bindingUtils.BindingUtils.setVisibility(this.mboundView7, vmLoadingInProgressBooleanTrueVmDisplayLoadingErrorBooleanTrueVmDisplayLoader);
        }
        if ((dirtyFlags & 0xc0800L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView10, vmLoadingTitleGetValue);
        }
        if ((dirtyFlags & 0xc8000L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView11, vmLoadingDescriptionGetValue);
            com.pnb.thecodecrashers.utils.bindingUtils.BindingUtils.setVisibility(this.mboundView11, TextUtilsIsEmptyVmLoadingDescription1);
        }
        if ((dirtyFlags & 0xc0200L) != 0) {
            // api target 1

            com.pnb.thecodecrashers.utils.bindingUtils.BindingUtils.setVisibility(this.mboundView12, androidxDatabindingViewDataBindingSafeUnboxVmDisplayLoadingErrorGetValue);
        }
        if ((dirtyFlags & 0xc0100L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView13, vmLoadingErrorMessageGetValue);
        }
        if ((dirtyFlags & 0xc0004L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView14, vmLoadingErrorButtonLabelGetValue);
        }
        if ((dirtyFlags & 0xc0008L) != 0) {
            // api target 1

            com.pnb.thecodecrashers.utils.bindingUtils.BindingUtils.setVisibility(this.mboundView2, androidxDatabindingViewDataBindingSafeUnboxVmIsCloseVisibleGetValue);
        }
        if ((dirtyFlags & 0xc4000L) != 0) {
            // api target 1

            androidx.databinding.adapters.ImageViewBindingAdapter.setImageDrawable(this.mboundView4, androidx.databinding.adapters.Converters.convertColorToDrawable(vmIconResGetValue));
            com.pnb.thecodecrashers.utils.bindingUtils.BindingUtils.setVisibility(this.mboundView4, vmIconResInt0);
        }
        if ((dirtyFlags & 0xc0080L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView5, vmTitleGetValue);
        }
        if ((dirtyFlags & 0xc0400L) != 0) {
            // api target 1

            com.pnb.thecodecrashers.utils.bindingUtils.BindingUtils.setVisibility(this.mboundView8, androidxDatabindingViewDataBindingSafeUnboxVmDisplayLoaderGetValue);
        }
        if ((dirtyFlags & 0xc0002L) != 0) {
            // api target 1

            this.mboundView9.setIndeterminateDrawable(vmIndeterminateDrawableGetValue);
        }
        if ((dirtyFlags & 0xc0001L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.rightActionButton, vmRActionTitleGetValue);
            com.pnb.thecodecrashers.utils.bindingUtils.BindingUtils.setVisibility(this.rightActionButton, vmRActionTitleJavaLangObjectNull);
        }
        if ((dirtyFlags & 0xd0000L) != 0) {
            // api target 1

            com.pnb.thecodecrashers.utils.bindingUtils.BindingUtils.bindBackgroud(this.rightActionButton, androidxDatabindingViewDataBindingSafeUnboxVmRSelectorGetValue);
        }
        if ((dirtyFlags & 0xe0000L) != 0) {
            // api target 1

            com.pnb.thecodecrashers.utils.bindingUtils.BindingUtils.setColor(this.rightActionButton, androidxDatabindingViewDataBindingSafeUnboxVmRightButtonTextColorGetValue);
        }
        if ((dirtyFlags & 0xc0020L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.subTitleRight, vmSubTitleRightGetValue);
        }
    }
    // Listener Stub Implementations
    public static class OnClickListenerImpl implements android.view.View.OnClickListener{
        private com.pnb.thecodecrashers.widgets.alert.AlertViewModel value;
        public OnClickListenerImpl setValue(com.pnb.thecodecrashers.widgets.alert.AlertViewModel value) {
            this.value = value;
            return value == null ? null : this;
        }
        @Override
        public void onClick(android.view.View arg0) {
            this.value.onClose(arg0); 
        }
    }
    public static class OnClickListenerImpl1 implements android.view.View.OnClickListener{
        private com.pnb.thecodecrashers.widgets.alert.AlertViewModel value;
        public OnClickListenerImpl1 setValue(com.pnb.thecodecrashers.widgets.alert.AlertViewModel value) {
            this.value = value;
            return value == null ? null : this;
        }
        @Override
        public void onClick(android.view.View arg0) {
            this.value.onRightClick(arg0); 
        }
    }
    public static class OnClickListenerImpl2 implements android.view.View.OnClickListener{
        private com.pnb.thecodecrashers.widgets.alert.AlertViewModel value;
        public OnClickListenerImpl2 setValue(com.pnb.thecodecrashers.widgets.alert.AlertViewModel value) {
            this.value = value;
            return value == null ? null : this;
        }
        @Override
        public void onClick(android.view.View arg0) {
            this.value.onLoadingErrorButtonClick(arg0); 
        }
    }
    public static class OnClickListenerImpl3 implements android.view.View.OnClickListener{
        private com.pnb.thecodecrashers.widgets.alert.AlertViewModel value;
        public OnClickListenerImpl3 setValue(com.pnb.thecodecrashers.widgets.alert.AlertViewModel value) {
            this.value = value;
            return value == null ? null : this;
        }
        @Override
        public void onClick(android.view.View arg0) {
            this.value.onLeftClick(arg0); 
        }
    }
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): vm.rActionTitle
        flag 1 (0x2L): vm.indeterminateDrawable
        flag 2 (0x3L): vm.loadingErrorButtonLabel
        flag 3 (0x4L): vm.isCloseVisible
        flag 4 (0x5L): vm.lActionTitle
        flag 5 (0x6L): vm.subTitleRight
        flag 6 (0x7L): vm.leftButtonTextColor
        flag 7 (0x8L): vm.title
        flag 8 (0x9L): vm.loadingErrorMessage
        flag 9 (0xaL): vm.displayLoadingError
        flag 10 (0xbL): vm.displayLoader
        flag 11 (0xcL): vm.loadingTitle
        flag 12 (0xdL): vm.lSelector
        flag 13 (0xeL): vm.loadingInProgress
        flag 14 (0xfL): vm.iconRes
        flag 15 (0x10L): vm.loadingDescription
        flag 16 (0x11L): vm.rSelector
        flag 17 (0x12L): vm.rightButtonTextColor
        flag 18 (0x13L): vm
        flag 19 (0x14L): null
        flag 20 (0x15L): androidx.databinding.ViewDataBinding.safeUnbox(vm.loadingInProgress.getValue()) ? true : androidx.databinding.ViewDataBinding.safeUnbox(vm.displayLoadingError.getValue())
        flag 21 (0x16L): androidx.databinding.ViewDataBinding.safeUnbox(vm.loadingInProgress.getValue()) ? true : androidx.databinding.ViewDataBinding.safeUnbox(vm.displayLoadingError.getValue())
        flag 22 (0x17L): androidx.databinding.ViewDataBinding.safeUnbox(vm.loadingInProgress.getValue()) ? true : androidx.databinding.ViewDataBinding.safeUnbox(vm.displayLoadingError.getValue()) ? true : androidx.databinding.ViewDataBinding.safeUnbox(vm.displayLoader.getValue())
        flag 23 (0x18L): androidx.databinding.ViewDataBinding.safeUnbox(vm.loadingInProgress.getValue()) ? true : androidx.databinding.ViewDataBinding.safeUnbox(vm.displayLoadingError.getValue()) ? true : androidx.databinding.ViewDataBinding.safeUnbox(vm.displayLoader.getValue())
        flag 24 (0x19L): !androidx.databinding.ViewDataBinding.safeUnbox(vm.loadingInProgress.getValue()) ? !androidx.databinding.ViewDataBinding.safeUnbox(vm.displayLoadingError.getValue()) : false ? !androidx.databinding.ViewDataBinding.safeUnbox(vm.displayLoader.getValue()) : false ? !TextUtils.isEmpty(vm.description) : false
        flag 25 (0x1aL): !androidx.databinding.ViewDataBinding.safeUnbox(vm.loadingInProgress.getValue()) ? !androidx.databinding.ViewDataBinding.safeUnbox(vm.displayLoadingError.getValue()) : false ? !androidx.databinding.ViewDataBinding.safeUnbox(vm.displayLoader.getValue()) : false ? !TextUtils.isEmpty(vm.description) : false
        flag 26 (0x1bL): !androidx.databinding.ViewDataBinding.safeUnbox(vm.loadingInProgress.getValue()) ? !androidx.databinding.ViewDataBinding.safeUnbox(vm.displayLoadingError.getValue()) : false
        flag 27 (0x1cL): !androidx.databinding.ViewDataBinding.safeUnbox(vm.loadingInProgress.getValue()) ? !androidx.databinding.ViewDataBinding.safeUnbox(vm.displayLoadingError.getValue()) : false
        flag 28 (0x1dL): !androidx.databinding.ViewDataBinding.safeUnbox(vm.loadingInProgress.getValue()) ? !androidx.databinding.ViewDataBinding.safeUnbox(vm.displayLoadingError.getValue()) : false ? !androidx.databinding.ViewDataBinding.safeUnbox(vm.displayLoader.getValue()) : false
        flag 29 (0x1eL): !androidx.databinding.ViewDataBinding.safeUnbox(vm.loadingInProgress.getValue()) ? !androidx.databinding.ViewDataBinding.safeUnbox(vm.displayLoadingError.getValue()) : false ? !androidx.databinding.ViewDataBinding.safeUnbox(vm.displayLoader.getValue()) : false
    flag mapping end*/
    //end
}