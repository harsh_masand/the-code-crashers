package thecodecrashers.databinding;
import thecodecrashers.R;
import thecodecrashers.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class LayoutErrorsBindingImpl extends LayoutErrorsBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    @NonNull
    private final android.widget.LinearLayout mboundView0;
    @NonNull
    private final com.sharedcode.widgets.CustomProgressBar mboundView1;
    @NonNull
    private final com.sharedcode.widgets.CustomImageView mboundView2;
    @NonNull
    private final com.sharedcode.widgets.CustomTextView mboundView3;
    @NonNull
    private final com.sharedcode.widgets.CustomTextView mboundView4;
    @NonNull
    private final com.sharedcode.widgets.CustomTextView mboundView5;
    // variables
    // values
    // listeners
    private OnClickListenerImpl mVmOnRetryClickAndroidViewViewOnClickListener;
    // Inverse Binding Event Handlers

    public LayoutErrorsBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 6, sIncludes, sViewsWithIds));
    }
    private LayoutErrorsBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 11
            );
        this.mboundView0 = (android.widget.LinearLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (com.sharedcode.widgets.CustomProgressBar) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView2 = (com.sharedcode.widgets.CustomImageView) bindings[2];
        this.mboundView2.setTag(null);
        this.mboundView3 = (com.sharedcode.widgets.CustomTextView) bindings[3];
        this.mboundView3.setTag(null);
        this.mboundView4 = (com.sharedcode.widgets.CustomTextView) bindings[4];
        this.mboundView4.setTag(null);
        this.mboundView5 = (com.sharedcode.widgets.CustomTextView) bindings[5];
        this.mboundView5.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x1000L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.vm == variableId) {
            setVm((com.pnb.thecodecrashers.errorscreens.ErrorScreenViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setVm(@Nullable com.pnb.thecodecrashers.errorscreens.ErrorScreenViewModel Vm) {
        this.mVm = Vm;
        synchronized(this) {
            mDirtyFlags |= 0x800L;
        }
        notifyPropertyChanged(BR.vm);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeVmIsFirstMessageVisible((androidx.lifecycle.MutableLiveData<java.lang.Boolean>) object, fieldId);
            case 1 :
                return onChangeVmIsVisible((androidx.lifecycle.MutableLiveData<java.lang.Boolean>) object, fieldId);
            case 2 :
                return onChangeVmIsSecondMessageVisible((androidx.lifecycle.MutableLiveData<java.lang.Boolean>) object, fieldId);
            case 3 :
                return onChangeVmSecondMessage((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
            case 4 :
                return onChangeVmHeaderDrawable((androidx.lifecycle.MutableLiveData<android.graphics.drawable.Drawable>) object, fieldId);
            case 5 :
                return onChangeVmIsProgressBarVisible((androidx.lifecycle.MutableLiveData<java.lang.Boolean>) object, fieldId);
            case 6 :
                return onChangeVmIsRetryVisible((androidx.lifecycle.MutableLiveData<java.lang.Boolean>) object, fieldId);
            case 7 :
                return onChangeVmFirstMessage((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
            case 8 :
                return onChangeVmBackgroundColor((androidx.lifecycle.MutableLiveData<java.lang.Integer>) object, fieldId);
            case 9 :
                return onChangeVmIsHeaderDrawableVisible((androidx.lifecycle.MutableLiveData<java.lang.Boolean>) object, fieldId);
            case 10 :
                return onChangeVmIsBackgroundColor((androidx.lifecycle.MutableLiveData<java.lang.Boolean>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeVmIsFirstMessageVisible(androidx.lifecycle.MutableLiveData<java.lang.Boolean> VmIsFirstMessageVisible, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVmIsVisible(androidx.lifecycle.MutableLiveData<java.lang.Boolean> VmIsVisible, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVmIsSecondMessageVisible(androidx.lifecycle.MutableLiveData<java.lang.Boolean> VmIsSecondMessageVisible, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVmSecondMessage(androidx.lifecycle.MutableLiveData<java.lang.String> VmSecondMessage, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVmHeaderDrawable(androidx.lifecycle.MutableLiveData<android.graphics.drawable.Drawable> VmHeaderDrawable, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x10L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVmIsProgressBarVisible(androidx.lifecycle.MutableLiveData<java.lang.Boolean> VmIsProgressBarVisible, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x20L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVmIsRetryVisible(androidx.lifecycle.MutableLiveData<java.lang.Boolean> VmIsRetryVisible, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x40L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVmFirstMessage(androidx.lifecycle.MutableLiveData<java.lang.String> VmFirstMessage, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x80L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVmBackgroundColor(androidx.lifecycle.MutableLiveData<java.lang.Integer> VmBackgroundColor, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x100L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVmIsHeaderDrawableVisible(androidx.lifecycle.MutableLiveData<java.lang.Boolean> VmIsHeaderDrawableVisible, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x200L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVmIsBackgroundColor(androidx.lifecycle.MutableLiveData<java.lang.Boolean> VmIsBackgroundColor, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x400L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        androidx.lifecycle.MutableLiveData<java.lang.Boolean> vmIsFirstMessageVisible = null;
        java.lang.Boolean vmIsVisibleGetValue = null;
        boolean androidxDatabindingViewDataBindingSafeUnboxVmIsRetryVisibleGetValue = false;
        androidx.lifecycle.MutableLiveData<java.lang.Boolean> vmIsVisible = null;
        androidx.lifecycle.MutableLiveData<java.lang.Boolean> vmIsSecondMessageVisible = null;
        java.lang.Boolean vmIsBackgroundColorGetValue = null;
        boolean androidxDatabindingViewDataBindingSafeUnboxVmIsVisibleGetValue = false;
        androidx.lifecycle.MutableLiveData<java.lang.String> vmSecondMessage = null;
        androidx.lifecycle.MutableLiveData<android.graphics.drawable.Drawable> vmHeaderDrawable = null;
        android.view.View.OnClickListener vmOnRetryClickAndroidViewViewOnClickListener = null;
        java.lang.String vmSecondMessageGetValue = null;
        com.pnb.thecodecrashers.errorscreens.ErrorScreenViewModel vm = mVm;
        java.lang.Boolean vmIsProgressBarVisibleGetValue = null;
        boolean androidxDatabindingViewDataBindingSafeUnboxVmIsProgressBarVisibleGetValue = false;
        java.lang.String vmFirstMessageGetValue = null;
        boolean androidxDatabindingViewDataBindingSafeUnboxVmIsHeaderDrawableVisibleGetValue = false;
        java.lang.Boolean vmIsFirstMessageVisibleGetValue = null;
        androidx.lifecycle.MutableLiveData<java.lang.Boolean> vmIsProgressBarVisible = null;
        androidx.lifecycle.MutableLiveData<java.lang.Boolean> vmIsRetryVisible = null;
        java.lang.Boolean vmIsHeaderDrawableVisibleGetValue = null;
        androidx.lifecycle.MutableLiveData<java.lang.String> vmFirstMessage = null;
        java.lang.Integer vmIsBackgroundColorVmBackgroundColorMboundView0AndroidColorTransparent = null;
        java.lang.Boolean vmIsSecondMessageVisibleGetValue = null;
        boolean androidxDatabindingViewDataBindingSafeUnboxVmIsFirstMessageVisibleGetValue = false;
        boolean androidxDatabindingViewDataBindingSafeUnboxVmIsBackgroundColorGetValue = false;
        boolean androidxDatabindingViewDataBindingSafeUnboxVmIsSecondMessageVisibleGetValue = false;
        androidx.lifecycle.MutableLiveData<java.lang.Integer> vmBackgroundColor = null;
        java.lang.Integer vmBackgroundColorGetValue = null;
        androidx.lifecycle.MutableLiveData<java.lang.Boolean> vmIsHeaderDrawableVisible = null;
        androidx.lifecycle.MutableLiveData<java.lang.Boolean> vmIsBackgroundColor = null;
        android.graphics.drawable.Drawable vmHeaderDrawableGetValue = null;
        java.lang.Boolean vmIsRetryVisibleGetValue = null;

        if ((dirtyFlags & 0x1fffL) != 0) {


            if ((dirtyFlags & 0x1801L) != 0) {

                    if (vm != null) {
                        // read vm.isFirstMessageVisible
                        vmIsFirstMessageVisible = vm.isFirstMessageVisible;
                    }
                    updateLiveDataRegistration(0, vmIsFirstMessageVisible);


                    if (vmIsFirstMessageVisible != null) {
                        // read vm.isFirstMessageVisible.getValue()
                        vmIsFirstMessageVisibleGetValue = vmIsFirstMessageVisible.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(vm.isFirstMessageVisible.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxVmIsFirstMessageVisibleGetValue = androidx.databinding.ViewDataBinding.safeUnbox(vmIsFirstMessageVisibleGetValue);
            }
            if ((dirtyFlags & 0x1802L) != 0) {

                    if (vm != null) {
                        // read vm.isVisible
                        vmIsVisible = vm.isVisible;
                    }
                    updateLiveDataRegistration(1, vmIsVisible);


                    if (vmIsVisible != null) {
                        // read vm.isVisible.getValue()
                        vmIsVisibleGetValue = vmIsVisible.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(vm.isVisible.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxVmIsVisibleGetValue = androidx.databinding.ViewDataBinding.safeUnbox(vmIsVisibleGetValue);
            }
            if ((dirtyFlags & 0x1804L) != 0) {

                    if (vm != null) {
                        // read vm.isSecondMessageVisible
                        vmIsSecondMessageVisible = vm.isSecondMessageVisible;
                    }
                    updateLiveDataRegistration(2, vmIsSecondMessageVisible);


                    if (vmIsSecondMessageVisible != null) {
                        // read vm.isSecondMessageVisible.getValue()
                        vmIsSecondMessageVisibleGetValue = vmIsSecondMessageVisible.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(vm.isSecondMessageVisible.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxVmIsSecondMessageVisibleGetValue = androidx.databinding.ViewDataBinding.safeUnbox(vmIsSecondMessageVisibleGetValue);
            }
            if ((dirtyFlags & 0x1808L) != 0) {

                    if (vm != null) {
                        // read vm.secondMessage
                        vmSecondMessage = vm.secondMessage;
                    }
                    updateLiveDataRegistration(3, vmSecondMessage);


                    if (vmSecondMessage != null) {
                        // read vm.secondMessage.getValue()
                        vmSecondMessageGetValue = vmSecondMessage.getValue();
                    }
            }
            if ((dirtyFlags & 0x1810L) != 0) {

                    if (vm != null) {
                        // read vm.headerDrawable
                        vmHeaderDrawable = vm.headerDrawable;
                    }
                    updateLiveDataRegistration(4, vmHeaderDrawable);


                    if (vmHeaderDrawable != null) {
                        // read vm.headerDrawable.getValue()
                        vmHeaderDrawableGetValue = vmHeaderDrawable.getValue();
                    }
            }
            if ((dirtyFlags & 0x1800L) != 0) {

                    if (vm != null) {
                        // read vm::onRetryClick
                        vmOnRetryClickAndroidViewViewOnClickListener = (((mVmOnRetryClickAndroidViewViewOnClickListener == null) ? (mVmOnRetryClickAndroidViewViewOnClickListener = new OnClickListenerImpl()) : mVmOnRetryClickAndroidViewViewOnClickListener).setValue(vm));
                    }
            }
            if ((dirtyFlags & 0x1820L) != 0) {

                    if (vm != null) {
                        // read vm.isProgressBarVisible
                        vmIsProgressBarVisible = vm.isProgressBarVisible;
                    }
                    updateLiveDataRegistration(5, vmIsProgressBarVisible);


                    if (vmIsProgressBarVisible != null) {
                        // read vm.isProgressBarVisible.getValue()
                        vmIsProgressBarVisibleGetValue = vmIsProgressBarVisible.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(vm.isProgressBarVisible.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxVmIsProgressBarVisibleGetValue = androidx.databinding.ViewDataBinding.safeUnbox(vmIsProgressBarVisibleGetValue);
            }
            if ((dirtyFlags & 0x1840L) != 0) {

                    if (vm != null) {
                        // read vm.isRetryVisible
                        vmIsRetryVisible = vm.isRetryVisible;
                    }
                    updateLiveDataRegistration(6, vmIsRetryVisible);


                    if (vmIsRetryVisible != null) {
                        // read vm.isRetryVisible.getValue()
                        vmIsRetryVisibleGetValue = vmIsRetryVisible.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(vm.isRetryVisible.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxVmIsRetryVisibleGetValue = androidx.databinding.ViewDataBinding.safeUnbox(vmIsRetryVisibleGetValue);
            }
            if ((dirtyFlags & 0x1880L) != 0) {

                    if (vm != null) {
                        // read vm.firstMessage
                        vmFirstMessage = vm.firstMessage;
                    }
                    updateLiveDataRegistration(7, vmFirstMessage);


                    if (vmFirstMessage != null) {
                        // read vm.firstMessage.getValue()
                        vmFirstMessageGetValue = vmFirstMessage.getValue();
                    }
            }
            if ((dirtyFlags & 0x1a00L) != 0) {

                    if (vm != null) {
                        // read vm.isHeaderDrawableVisible
                        vmIsHeaderDrawableVisible = vm.isHeaderDrawableVisible;
                    }
                    updateLiveDataRegistration(9, vmIsHeaderDrawableVisible);


                    if (vmIsHeaderDrawableVisible != null) {
                        // read vm.isHeaderDrawableVisible.getValue()
                        vmIsHeaderDrawableVisibleGetValue = vmIsHeaderDrawableVisible.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(vm.isHeaderDrawableVisible.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxVmIsHeaderDrawableVisibleGetValue = androidx.databinding.ViewDataBinding.safeUnbox(vmIsHeaderDrawableVisibleGetValue);
            }
            if ((dirtyFlags & 0x1d00L) != 0) {

                    if (vm != null) {
                        // read vm.isBackgroundColor
                        vmIsBackgroundColor = vm.isBackgroundColor;
                    }
                    updateLiveDataRegistration(10, vmIsBackgroundColor);


                    if (vmIsBackgroundColor != null) {
                        // read vm.isBackgroundColor.getValue()
                        vmIsBackgroundColorGetValue = vmIsBackgroundColor.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(vm.isBackgroundColor.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxVmIsBackgroundColorGetValue = androidx.databinding.ViewDataBinding.safeUnbox(vmIsBackgroundColorGetValue);
                if((dirtyFlags & 0x1d00L) != 0) {
                    if(androidxDatabindingViewDataBindingSafeUnboxVmIsBackgroundColorGetValue) {
                            dirtyFlags |= 0x4000L;
                    }
                    else {
                            dirtyFlags |= 0x2000L;
                    }
                }
            }
        }
        // batch finished

        if ((dirtyFlags & 0x4000L) != 0) {

                if (vm != null) {
                    // read vm.backgroundColor
                    vmBackgroundColor = vm.backgroundColor;
                }
                updateLiveDataRegistration(8, vmBackgroundColor);


                if (vmBackgroundColor != null) {
                    // read vm.backgroundColor.getValue()
                    vmBackgroundColorGetValue = vmBackgroundColor.getValue();
                }
        }

        if ((dirtyFlags & 0x1d00L) != 0) {

                // read androidx.databinding.ViewDataBinding.safeUnbox(vm.isBackgroundColor.getValue()) ? vm.backgroundColor.getValue() : @android:color/transparent
                vmIsBackgroundColorVmBackgroundColorMboundView0AndroidColorTransparent = ((androidxDatabindingViewDataBindingSafeUnboxVmIsBackgroundColorGetValue) ? (vmBackgroundColorGetValue) : (getColorFromResource(mboundView0, android.R.color.transparent)));
        }
        // batch finished
        if ((dirtyFlags & 0x1d00L) != 0) {
            // api target 1

            androidx.databinding.adapters.ViewBindingAdapter.setBackground(this.mboundView0, androidx.databinding.adapters.Converters.convertColorToDrawable(vmIsBackgroundColorVmBackgroundColorMboundView0AndroidColorTransparent));
        }
        if ((dirtyFlags & 0x1802L) != 0) {
            // api target 1

            com.pnb.thecodecrashers.utils.bindingUtils.BindingUtils.setVisibility(this.mboundView0, androidxDatabindingViewDataBindingSafeUnboxVmIsVisibleGetValue);
        }
        if ((dirtyFlags & 0x1820L) != 0) {
            // api target 1

            com.pnb.thecodecrashers.utils.bindingUtils.BindingUtils.setVisibility(this.mboundView1, androidxDatabindingViewDataBindingSafeUnboxVmIsProgressBarVisibleGetValue);
        }
        if ((dirtyFlags & 0x1810L) != 0) {
            // api target 1

            androidx.databinding.adapters.ImageViewBindingAdapter.setImageDrawable(this.mboundView2, vmHeaderDrawableGetValue);
        }
        if ((dirtyFlags & 0x1a00L) != 0) {
            // api target 1

            com.pnb.thecodecrashers.utils.bindingUtils.BindingUtils.setVisibility(this.mboundView2, androidxDatabindingViewDataBindingSafeUnboxVmIsHeaderDrawableVisibleGetValue);
        }
        if ((dirtyFlags & 0x1880L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView3, vmFirstMessageGetValue);
        }
        if ((dirtyFlags & 0x1801L) != 0) {
            // api target 1

            com.pnb.thecodecrashers.utils.bindingUtils.BindingUtils.setVisibility(this.mboundView3, androidxDatabindingViewDataBindingSafeUnboxVmIsFirstMessageVisibleGetValue);
        }
        if ((dirtyFlags & 0x1808L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView4, vmSecondMessageGetValue);
        }
        if ((dirtyFlags & 0x1804L) != 0) {
            // api target 1

            com.pnb.thecodecrashers.utils.bindingUtils.BindingUtils.setVisibility(this.mboundView4, androidxDatabindingViewDataBindingSafeUnboxVmIsSecondMessageVisibleGetValue);
        }
        if ((dirtyFlags & 0x1800L) != 0) {
            // api target 1

            this.mboundView5.setOnClickListener(vmOnRetryClickAndroidViewViewOnClickListener);
        }
        if ((dirtyFlags & 0x1840L) != 0) {
            // api target 1

            com.pnb.thecodecrashers.utils.bindingUtils.BindingUtils.setVisibility(this.mboundView5, androidxDatabindingViewDataBindingSafeUnboxVmIsRetryVisibleGetValue);
        }
    }
    // Listener Stub Implementations
    public static class OnClickListenerImpl implements android.view.View.OnClickListener{
        private com.pnb.thecodecrashers.errorscreens.ErrorScreenViewModel value;
        public OnClickListenerImpl setValue(com.pnb.thecodecrashers.errorscreens.ErrorScreenViewModel value) {
            this.value = value;
            return value == null ? null : this;
        }
        @Override
        public void onClick(android.view.View arg0) {
            this.value.onRetryClick(arg0); 
        }
    }
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): vm.isFirstMessageVisible
        flag 1 (0x2L): vm.isVisible
        flag 2 (0x3L): vm.isSecondMessageVisible
        flag 3 (0x4L): vm.secondMessage
        flag 4 (0x5L): vm.headerDrawable
        flag 5 (0x6L): vm.isProgressBarVisible
        flag 6 (0x7L): vm.isRetryVisible
        flag 7 (0x8L): vm.firstMessage
        flag 8 (0x9L): vm.backgroundColor
        flag 9 (0xaL): vm.isHeaderDrawableVisible
        flag 10 (0xbL): vm.isBackgroundColor
        flag 11 (0xcL): vm
        flag 12 (0xdL): null
        flag 13 (0xeL): androidx.databinding.ViewDataBinding.safeUnbox(vm.isBackgroundColor.getValue()) ? vm.backgroundColor.getValue() : @android:color/transparent
        flag 14 (0xfL): androidx.databinding.ViewDataBinding.safeUnbox(vm.isBackgroundColor.getValue()) ? vm.backgroundColor.getValue() : @android:color/transparent
    flag mapping end*/
    //end
}