package com.pnb.thecodecrashers;

import android.content.Context;
import android.location.Address;
import android.net.Uri;
import android.text.TextUtils;
import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.Volley;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pnb.thecodecrashers.apiclient.CommonAppApis;
import com.pnb.thecodecrashers.controller.SessionManager;
import com.pnb.thecodecrashers.controller.SharedPreferencesManager;
import com.sharedcode.helpers.LogHelper;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import thecodecrashers.BuildConfig;

/**
 * Created by Akhil on 10/08/16.
 */
public class AppController extends MultiDexApplication {

    public static final String TAG = AppController.class
            .getSimpleName();
    private static AppController mInstance;
    private RequestQueue mRequestQueue;
    private Gson mGson;
    private Address currentSelectedLocation;
    private String mPhoneNumber;
    private Uri mDeepLinkData;
    private boolean mIsDeeplinkingRedirected;
    private boolean isNewUser;

    public static synchronized AppController getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        SharedPreferencesManager.init(this);
        SessionManager.init(this);
        CommonAppApis.appSettingsApi(this);
        LogHelper.setLog(!BuildConfig.IS_PROD || BuildConfig.DEBUG);
        VolleyLog.DEBUG = LogHelper.LOG_ENABLED;
        mIsDeeplinkingRedirected = false;
    }

    /**
     * @return returns Gson singleton object with LOWER_CASE_WITH_UNDERSCORES jsonKeys
     */
    public Gson getGson() {
        if (mGson == null) {
            mGson = new GsonBuilder()
                    .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                    .create();
        }
        return mGson;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext(), new HurlStack() {
                @Override
                protected HttpURLConnection createConnection(URL url) throws IOException {
                    HttpURLConnection connection = super.createConnection(url);
                    connection.setInstanceFollowRedirects(false);
                    return connection;
                }
            });
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelAllApi() {
        getRequestQueue().cancelAll(new RequestQueue.RequestFilter() {
            @Override
            public boolean apply(Request<?> request) {
                return true;
            }
        });
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    public String getPhoneNumber() {
        return mPhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        mPhoneNumber = phoneNumber;
    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public Address getCurrentSelectedLocation() {
        return currentSelectedLocation;
    }

    public void setCurrentSelectedLocation(Address currentSelectedLocation) {
        this.currentSelectedLocation = currentSelectedLocation;
    }

    public Uri getDeepLinkData() {
        return mDeepLinkData;
    }

    public void setDeepLinkData(Uri data) {
        mDeepLinkData = data;
        AppController.getInstance().setIsDeeplinkingRedirected(false);
    }

    public boolean isNewUser() {
        return isNewUser;
    }

    public void setNewUser(boolean newUser) {
        isNewUser = newUser;
    }

    public boolean isDeeplinkingRedirected() {
        return mIsDeeplinkingRedirected;
    }

    public void setIsDeeplinkingRedirected(boolean isDeeplinkingRedirected) {
        mIsDeeplinkingRedirected = isDeeplinkingRedirected;
    }
}