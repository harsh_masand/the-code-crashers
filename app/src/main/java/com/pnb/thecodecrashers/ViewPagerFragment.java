package com.pnb.thecodecrashers;


/**
 * Created by admin on 31/08/16.
 */
public abstract class ViewPagerFragment extends BaseFragment {

    public abstract String getTitle();

    public abstract TabViewModel getTab();
}
