package com.pnb.thecodecrashers.controller;

import android.content.Context;
import com.pnb.thecodecrashers.AppController;
import com.sharedcode.helpers.ObfuscatedSharedPreference;

public class SharedPreferencesManager {
    private static final String SECRET_KEY = "RANDOM";
    private static final String KEY_PREF_NAME = "SharedPreferencesManager";
    private static final String KEY_PREVIOUS_UPDATE_VERSION = "previous_update_version";
    private static final String KEY_NOTIFICATION_REG_ID = "regId";
    private static final String KEY_SESSION_TOKEN = "sessionToken";
    private static final String KEY_FCM_ID_SENT_TO_SERVER = "fcm_id_sent_to_server";

    private static ObfuscatedSharedPreference sSharedPreference;

    public static ObfuscatedSharedPreference getSharedPreference() {
        return sSharedPreference;
    }

    public static void init(AppController context) {
        sSharedPreference = new ObfuscatedSharedPreference(context, SECRET_KEY, context.getSharedPreferences(KEY_PREF_NAME, Context.MODE_PRIVATE));
    }

    public static String getPreviousAppVersionInPrefs() {
        return sSharedPreference.getString(KEY_PREVIOUS_UPDATE_VERSION, "0.0.0");
    }

    public static void setPreviousAppVersionInPrefs(String value) {
        sSharedPreference.edit().putString(KEY_PREVIOUS_UPDATE_VERSION, value)
                .apply();
    }

    public static String getNotificationRegId() {
        return sSharedPreference.getString(KEY_NOTIFICATION_REG_ID, "");
    }

    public static void setNotificationRegId(String value) {
        sSharedPreference.edit().putString(KEY_NOTIFICATION_REG_ID, value)
                .apply();
    }

    public static String getSessionToken() {
        return sSharedPreference.getString(KEY_SESSION_TOKEN, "");
    }

    public static void setSessionToken(String value) {
        sSharedPreference.edit().putString(KEY_SESSION_TOKEN, value)
                .apply();
    }

    public static boolean getFcmIdSentToServer() {
        return sSharedPreference.getBoolean(KEY_FCM_ID_SENT_TO_SERVER, false);
    }

    public static void setFcmIdSentToServer(boolean isSentToServer) {
        sSharedPreference.edit().putBoolean(KEY_FCM_ID_SENT_TO_SERVER, isSentToServer).apply();
    }

    public static void clearPreferences() {
    }

}