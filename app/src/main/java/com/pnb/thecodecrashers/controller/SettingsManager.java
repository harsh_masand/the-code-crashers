package com.pnb.thecodecrashers.controller;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import com.google.gson.Gson;
import com.pnb.thecodecrashers.models.AppSettingsModel;
import org.json.JSONObject;

public class SettingsManager {

    /**
     * Shared Preference Keys
     */
    private static final String PREF_NAME = "app_settings";
    private static final String SETTINGS_JSON_STRING = "settings_json";

    /**
     * Returns the settings shared prefs in multi process mode
     *
     * @param context
     * @return
     */
    public static SharedPreferences getSharedPrefs(Context context) {
        return context.getSharedPreferences(PREF_NAME,
                Context.MODE_PRIVATE);
    }

    /**
     * Save the settings jsonObject into pref in string form
     */
    public static void saveAppSettings(Context context, JSONObject settingsJson) {
        /* Storing the Setting String in Shared Preference */
        getSharedPrefs(context).edit()
                .putString(SETTINGS_JSON_STRING, settingsJson.toString()).commit();
    }

    /**
     * Get App settings model navigatedFrom pref
     */
    public static AppSettingsModel getAppSettings(Context context) {
        AppSettingsModel appSettings = null;
        /* Storing the Setting String in Shared Preference */
        String settingJson = getSharedPrefs(context).getString(
                SETTINGS_JSON_STRING, null);
        if (!TextUtils.isEmpty(settingJson)) {
            appSettings = new Gson().fromJson(settingJson, AppSettingsModel.class);
        }

        return (appSettings != null ? appSettings : new AppSettingsModel());
    }
}