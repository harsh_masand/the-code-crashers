package com.pnb.thecodecrashers.controller;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import androidx.annotation.IntDef;
import com.google.gson.Gson;
import com.pnb.thecodecrashers.AppController;
import com.pnb.thecodecrashers.AppNavigator;
import thecodecrashers.R;
import com.pnb.thecodecrashers.models.UserModel;
import com.sharedcode.helpers.LogHelper;
import com.sharedcode.utils.AppUtils;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by admin on 24/04/17.
 */

public class SessionManager {

    public static final String SAVED_TIMESTAMP = "saved_timestamp";
    public static final int NEW_USER = 2;
    public static final int DEFAULT = -1;
    // Shared pref file name
    private static final String PREF_NAME = "session_details";
    // Login Details
    private static final String SESSION_TOKEN = "SessionToken";
    private static final String DEBUG_TAG = "SessionManager";
    private static final String USER_PROFILE = "user_profile";
    private static final String REGISTERED = "is_registered";
    private static final String ORDER_FAILED_AFTER_PG = "order_failed_after_pg";
    public static String sessionToken;
    public static String deviceId;
    private static SessionManager sInstance;
    private static boolean isRegistered;
    // Shared Preferences
    private SharedPreferences mPref;
    // Editor for Shared Preferences
    private SharedPreferences.Editor mEditor;
    private Context mContext;
    @NavigationAfterLogin
    private int navigationAfterLoginFlag = DEFAULT;

    // Constructor
    private SessionManager(Context context) {
        if (context == null)
            throw new NullPointerException(
                    "Null context passed in session manager");
        this.mContext = context;
        mPref = mContext.getSharedPreferences(PREF_NAME,
                Context.MODE_MULTI_PROCESS);
        mEditor = mPref.edit();
        if (sessionToken == null)
            getSessionToken();
        if (deviceId == null)
            deviceId = AppUtils.getDeviceId(mContext);
    }

    public static void init(Context globalContext) {
        sInstance = new SessionManager(globalContext);
    }

    public static SessionManager getInstance() {
        return sInstance;
    }

    public boolean isLoggedIn() {
        return (!TextUtils.isEmpty(getSessionToken()));
    }

    public boolean getIsRegistered() {
        SessionManager.isRegistered = mPref.getBoolean(REGISTERED, false);
        return SessionManager.isRegistered;
    }

    public String getSessionToken() {
        return SharedPreferencesManager.getSessionToken();
    }

    public void setSessionToken(String sessionToken) {
        LogHelper.d("SessionManager", "Setting session token->" + String.valueOf(sessionToken));
        SharedPreferencesManager.setSessionToken(sessionToken);
    }

    public void setRedirectToChangePassword(boolean redirectToChangePassword) {
        LogHelper.d("SessionManager", "Redirect To-> " + String.valueOf(redirectToChangePassword));
    }

    private void clearDetails() {
        setPrefsDefault();
    }

    public void logOut(boolean isShowMsg) {
        AppController.getInstance().cancelAllApi();
        AppController.getInstance().setDeepLinkData(null);
        clearDetails();
        if (isShowMsg) {
            AppUtils.showToast(mContext, R.string.error_signed_out, false);
        }
        AppNavigator.navigateToLoginActivity();
    }

    public UserModel getProfile() {
        if (mPref.contains(USER_PROFILE)) {
            String profile = mPref.getString(USER_PROFILE, null);
            if (profile != null)
                return new Gson().fromJson(profile, UserModel.class);
            else {
                UserModel user = new UserModel();
                setProfile(user);
                return user;
            }
        } else {
            UserModel user = new UserModel();
            setProfile(user);
            return user;
        }
    }

    public void setProfile(UserModel user) {
        if (user == null) {
            mEditor.remove(USER_PROFILE);
        } else {
            mEditor.putString(USER_PROFILE, new Gson().toJson(user));
        }
        mEditor.commit();
    }

    public boolean getOrderFailedAfterPg() {
        return mPref.getBoolean(ORDER_FAILED_AFTER_PG, false);
    }

    public void setOrderFailedAfterPg(boolean status) {
        mEditor.putBoolean(ORDER_FAILED_AFTER_PG, status);
        mEditor.commit();
    }

    private void setPrefsDefault() {
        // Reset token
        setSessionToken(null);
        // Clear the profile
        setProfile(null);
        SharedPreferencesManager.setFcmIdSentToServer(false);
        SharedPreferencesManager.clearPreferences();
    }

    public void savedTimestamp(String time) {
        mEditor.putString(SAVED_TIMESTAMP, time);
        mEditor.commit();
    }

    public String getSavedTimestamp() {
        return mPref.getString(SAVED_TIMESTAMP, "");
    }

    @NavigationAfterLogin
    public int getNavigationAfterLoginFlag() {
        return navigationAfterLoginFlag;
    }

    public void setNavigationAfterLoginFlag(@NavigationAfterLogin int navigationAfterLoginFlag) {
        this.navigationAfterLoginFlag = navigationAfterLoginFlag;
        AppController.getInstance().setDeepLinkData(null);
    }

    @IntDef({DEFAULT, NEW_USER})
    @Retention(RetentionPolicy.SOURCE)
    public @interface NavigationAfterLogin {
    }
}

