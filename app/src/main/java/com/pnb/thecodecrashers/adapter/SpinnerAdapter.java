package com.pnb.thecodecrashers.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import thecodecrashers.BR;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Created by Akhil on 13/01/16.
 */
public abstract class SpinnerAdapter<T> extends ArrayAdapter<T> {


    protected List<T> mViewModels;
    LayoutInflater inflater;

    public SpinnerAdapter(Context context, int resId, List<T> viewModels) {
        super(context, resId);
        this.mViewModels = viewModels;
        onViewModelListChanged();
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {

        /********** Inflate spinner_rows.xml file for each row ( Defined below ) ************/
        ViewDataBinding binding = DataBindingUtil.inflate(inflater, getViewModelLayoutMap().get(getItem(position).getClass()), parent, false);
        /***** Get each Model object navigatedFrom Arraylist ********/
        binding.setVariable(BR.vm, getItem(position));


        return binding.getRoot();
    }

    @Override
    public int getViewTypeCount() {
        return super.getViewTypeCount();
    }

    @Override
    public int getItemViewType(int position) {
        return getViewModelLayoutMap().get(getItem(position).getClass());
    }


    @Override
    public int getCount() {
        return mViewModels == null ? 0 : mViewModels.size();
    }

    public abstract Map<Class, Integer> getViewModelLayoutMap();

    public void onViewModelListChanged() {
        notifyDataSetChanged();
    }

    @Nullable
    @Override
    public T getItem(int position) {
        return mViewModels.get(position);
    }

    public void clearAll() {
        this.mViewModels.clear();
        onViewModelListChanged();
    }

    @Override
    public void addAll(Collection<? extends T> collection) {
        this.mViewModels.clear();
        this.mViewModels.addAll(collection);
        onViewModelListChanged();
    }

    public void close() {
    }

    public void refreshList(List<T> list) {
        this.mViewModels.clear();
        this.mViewModels.addAll(list);
        onViewModelListChanged();
    }

}
