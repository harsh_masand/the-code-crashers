package com.pnb.thecodecrashers.adapter;

import android.os.Parcelable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import com.pnb.thecodecrashers.ViewPagerFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Akhil on 31/08/16.
 */
public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    private List<ViewPagerFragment> mFragmentList = new ArrayList<>();

    private ViewPagerAdapter(FragmentManager manager) {
        super(manager);
    }

    public ViewPagerAdapter(FragmentManager manager, List<ViewPagerFragment> fragmentList) {
        super(manager);
        this.mFragmentList = fragmentList;
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentList.get(position).getTitle();
    }

    public void updateList(List<ViewPagerFragment> fragmentList) {
        mFragmentList.clear();
        mFragmentList.addAll(fragmentList);
        notifyDataSetChanged();
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public Parcelable saveState() {
        return null;
    }
}