package com.pnb.thecodecrashers.adapter;

import android.graphics.Rect;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by shashank on 17/12/15.
 */
public class SpacingItemDecoration extends RecyclerView.ItemDecoration {

    private float mLeftSpace;
    private float mTopSpace;
    private float mRightSpace;
    private float mBottomSpace;

    public SpacingItemDecoration(float LeftSpace, float TopSpace, float RightSpace, float BottomSpace) {
        this.mLeftSpace = LeftSpace;
        this.mTopSpace = TopSpace;
        this.mRightSpace = RightSpace;
        this.mBottomSpace = BottomSpace;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        if (mLeftSpace != -1)
            outRect.left = (int) mLeftSpace;

        if (mRightSpace != -1)
            outRect.right = (int) mRightSpace;

        if (mBottomSpace != -1)
            outRect.bottom = (int) mBottomSpace;

        // Add top margin only for the first item to avoid double space between items
        if (parent.getChildPosition(view) == 0 && mTopSpace != -1)
            outRect.top = (int) mTopSpace;
    }
}
