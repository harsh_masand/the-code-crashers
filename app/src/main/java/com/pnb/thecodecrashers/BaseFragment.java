package com.pnb.thecodecrashers;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import com.pnb.thecodecrashers.utils.ViewUtils;

import thecodecrashers.R;


/**
 * Created by Akhil on 08/08/16.
 */
public class BaseFragment extends Fragment {

    public static final String KEY_CALL_BACK_PRESS = "call_back_press";
    public static final String KEY_FROM_NAV = "from_nav";
    protected int childFragmentContainedId = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewUtils.hideKeyboardImplicitly(getActivity());
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    public void replaceFragment(Fragment f, boolean addToBackStack) {
        replaceFragment(f, addToBackStack, BaseActivity.FragmentTransactionAnimationType.NONE);
    }

    /**
     * Used by child fragments to replace themselves with another fragment *
     **/
    public void replaceFragment(Fragment f, boolean addToBackStack, BaseActivity.FragmentTransactionAnimationType animationType) {
        if (childFragmentContainedId == 0)
            return;

        String currentFragmentName = "";
        Fragment fragment = getCurrentChildFragment();
        if (fragment != null)
            currentFragmentName = fragment.getClass().toString();
        if (f.getClass().toString().equalsIgnoreCase(currentFragmentName))
            return;

        FragmentManager manager = this.getChildFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        if (animationType == BaseActivity.FragmentTransactionAnimationType.FROM_RIGHT) {
            transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left,
                    R.anim.slide_out_right);
        } else if (animationType == BaseActivity.FragmentTransactionAnimationType.FROM_BOTTOM) {
            transaction.setCustomAnimations(R.anim.slide_in_down, 0, 0, R.anim.slide_out_down);
        } else if (animationType == BaseActivity.FragmentTransactionAnimationType.FROM_RIGHT_TO_BOTTOM) {
            transaction.setCustomAnimations(R.anim.slide_in_right, 0, 0, R.anim.slide_out_down);
        } else if (animationType == BaseActivity.FragmentTransactionAnimationType.FROM_TOP) {
            transaction.setCustomAnimations(R.anim.slide_in_up, 0, 0, R.anim.slide_out_up);
        } else if (animationType == BaseActivity.FragmentTransactionAnimationType.FADE_IN) {
            transaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
        } else {
            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
        }

        transaction.replace(childFragmentContainedId, f);
        if (addToBackStack) {
            transaction.addToBackStack(null);
        }
        transaction.commit();
    }

    public Fragment getCurrentChildFragment() {
        Fragment fragment = getChildFragmentManager().findFragmentById(childFragmentContainedId);
        return fragment;
    }

    public boolean hasChildFragmentsInBackstack() {
        return getChildFragmentManager().getBackStackEntryCount() > 0;
    }

    /**
     * If this function is overriden, make sure that super.onBackPressed() is
     * called if backPressed is not handled by the fragment
     *
     * @return true if backpress has been handled
     */
    public boolean onBackPressed() {

        FragmentManager manager = getChildFragmentManager();
        Fragment f = getCurrentChildFragment();
        // Call onBackPressed of child fragment
        if (f != null && f instanceof BaseFragment) {

            if (((BaseFragment) f).onBackPressed()) { // If handled return
                return true;
            }
        }
        // Child did not handle onBackPressed(). Pop backstack
        manager = getFragmentManager();
        if (manager != null && manager.getBackStackEntryCount() > 0) {
            manager.popBackStack();
            return true;
        } else if (manager != null && manager.getBackStackEntryCount() == 0) {
            getActivity().finish();
        }

        return false;
    }

    public void setChildFragmentContainedId(int childFragmentContainedId) {
        this.childFragmentContainedId = childFragmentContainedId;
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {

        /**
         * If the parent fragment is getting removed then we do not play any pop animation on its child fragment.
         */
        Fragment parentFragment = getParentFragment();
        if (!enter && parentFragment != null && parentFragment.isRemoving()) {
            return AnimationUtils.loadAnimation(getActivity(), R.anim.slide_dummy);
        }
        return super.onCreateAnimation(transit, enter, nextAnim);
    }


    @Override
    public void onResume() {
        super.onResume();
        ViewUtils.hideSoftKeyboard(getActivity());
    }
}
