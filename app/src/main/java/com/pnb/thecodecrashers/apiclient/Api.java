package com.pnb.thecodecrashers.apiclient;

import android.text.TextUtils;
import androidx.annotation.NonNull;
import java.util.ArrayList;

import thecodecrashers.BuildConfig;

public abstract class Api {

    public static final String SERVER = "Test";
    public final static String SERVER_HOST_NAME = BuildConfig.HOST;
    public final static String ENCODE_TYPE = "UTF-8";
    protected static final int STATUS_SUCCESS = 200;
    protected final static String VERSION_PREFIX = "/v1";
    protected final static String SERVICE_PREFIX_API = "/api";

    ArrayList<String> arrayList = new ArrayList<>();

    @Override
    public String toString() {
        return getClass().getSimpleName();
    }

    protected void createUrl(String key, String value) {
        if (!TextUtils.isEmpty(value)) {
            value = value.replaceAll(" ", "%20");
            if (arrayList.isEmpty()) {
                arrayList.add("?" + key + "=" + value);
            } else {
                arrayList.add("&" + key + "=" + value);
            }
        }
    }

    protected void addParams(@NonNull StringBuilder url) {
        for (String s : arrayList) {
            url.append(s);
        }
    }
}