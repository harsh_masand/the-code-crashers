package com.pnb.thecodecrashers.apiclient;

import androidx.annotation.NonNull;
import com.pnb.thecodecrashers.utils.ConversionUtils;
import org.json.JSONObject;


/**
 * Created by Akhil on 11/08/16.
 */
public class ApiError {
    ApiErrorType type;
    String message;
    Object data;
    int code;

    public ApiError(String type, String message) {
        ApiErrorType errorType = ConversionUtils.stringToEnum(ApiErrorType.class, type);
        if (errorType != null)
            this.type = errorType;
        else
            this.type = ApiErrorType.UNKNOWN;
        this.message = message;
    }

    public ApiError(String type, String message, int code) {
        ApiErrorType errorType = ConversionUtils.stringToEnum(ApiErrorType.class, type);
        if (errorType != null)
            this.type = errorType;
        else
            this.type = ApiErrorType.UNKNOWN;
        this.message = message;
        this.code = code;
    }

    public ApiError(@NonNull ApiErrorType type, String message) {
        this.type = type;
        this.message = message;
    }

    public ApiError(@NonNull ApiErrorType type, String message, int code) {
        this.type = type;
        this.message = message;
        this.code = code;
    }

    public boolean dataIsObject() {
        return (data != null && data instanceof JSONObject);
    }

    public JSONObject getDataAsObject() {
        if (this.dataIsObject()) {
            return (JSONObject) this.data;
        } else {
            return null;
        }
    }

    public ApiErrorType getType() {
        return type;
    }

    public void setType(ApiErrorType type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}