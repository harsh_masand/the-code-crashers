package com.pnb.thecodecrashers.apiclient;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyLog;

import ch.boye.httpclientandroidlib.HttpEntity;
import ch.boye.httpclientandroidlib.entity.ContentType;
import ch.boye.httpclientandroidlib.entity.mime.HttpMultipartMode;
import ch.boye.httpclientandroidlib.entity.mime.MultipartEntityBuilder;
import ch.boye.httpclientandroidlib.entity.mime.content.FileBody;
import ch.boye.httpclientandroidlib.util.CharsetUtils;
import ch.boye.httpclientandroidlib.util.TextUtils;

import java.io.*;
import java.util.Map;

/*
 Created b*y Akhil on 23/08/16.
 */


public class MultipartRequest extends Request<String> {

    private final Response.Listener<String> mListener;
    private final Map<String, String> mStringPart;
    private final Map<String, File> mFilePart;
    private final MultipartProgressListener multipartProgressListener;
    MultipartEntityBuilder entity = MultipartEntityBuilder.create();
    HttpEntity httpentity;
    private long fileLength = 0L;

    public MultipartRequest(int method, String url, Response.ErrorListener errorListener,
                            Response.Listener<String> listener, Map<String, File> mFilePart,
                            Map<String, String> mStringPart,
                            MultipartProgressListener progLitener) {
        super(method, url, errorListener);

        this.mListener = listener;
        this.mFilePart = mFilePart;
        this.mStringPart = mStringPart;
        this.multipartProgressListener = progLitener;
        long totalLength = 0;
        if (mFilePart != null) {
            for (Map.Entry<String, File> entry : mFilePart.entrySet()) {
                File file = entry.getValue();
                if (file != null)
                    totalLength = totalLength + file.length();
            }
        }
        this.fileLength = totalLength;

        entity.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        try {
            entity.setCharset(CharsetUtils.get("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        buildMultipartEntity();
        httpentity = entity.build();
    }

    public void addStringBody(String param, String value) {
        if (mStringPart != null) {
            mStringPart.put(param, value);
        }
    }

    private void buildMultipartEntity() {
        if (mFilePart != null) {
            for (Map.Entry<String, File> entry : mFilePart.entrySet()) {
                File file = entry.getValue();
                if (file != null)
                    entity.addPart(entry.getKey(), new FileBody(file, ContentType.create("image/jpg"), file.getName()));
            }
        }

        if (mStringPart != null) {
            for (Map.Entry<String, String> entry : mStringPart.entrySet()) {
                entity.addTextBody(entry.getKey(), TextUtils.isEmpty(entry.getValue()) ? "" : entry.getValue());
            }
        }
    }

    @Override
    public String getBodyContentType() {
        return httpentity.getContentType().getValue();
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            httpentity.writeTo(new CountingOutputStream(bos, fileLength,
                    multipartProgressListener));
        } catch (IOException e) {
            VolleyLog.e("IOException writing to ByteArrayOutputStream");
        }
        return bos.toByteArray();
    }


    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {

        try {
//          System.out.println("Network Response "+ new String(response.data, "UTF-8"));
            return Response.success(new String(response.data, "UTF-8"),
                    getCacheEntry());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            // fuck it, it should never happen though
            return Response.success(new String(response.data), getCacheEntry());
        }
    }

    @Override
    protected void deliverResponse(String response) {
        mListener.onResponse(response);
    }

//Override getHeaders() if you want to put anything in header

    public interface MultipartProgressListener {
        void transferred(long transfered, int progress);
    }

    public static class CountingOutputStream extends FilterOutputStream {
        private final MultipartProgressListener progListener;
        private long transferred;
        private long fileLength;

        public CountingOutputStream(final OutputStream out, long fileLength,
                                    final MultipartProgressListener listener) {
            super(out);
            this.fileLength = fileLength;
            this.progListener = listener;
            this.transferred = 0;
        }

        public void write(byte[] b, int off, int len) throws IOException {
            out.write(b, off, len);
            if (progListener != null && fileLength > 0) {
                this.transferred += len;
                int prog = (int) (transferred * 100 / fileLength);
                this.progListener.transferred(this.transferred, prog);
            }
        }

        public void write(int b) throws IOException {
            out.write(b);
            if (progListener != null && fileLength > 0) {
                this.transferred++;
                int prog = (int) (transferred * 100 / fileLength);
                this.progListener.transferred(this.transferred, prog);
            }
        }

    }
}
