package com.pnb.thecodecrashers.apiclient;

import android.text.TextUtils;
import ch.boye.httpclientandroidlib.HttpStatus;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.StringRequest;
import com.pnb.thecodecrashers.AppController;
import thecodecrashers.R;
import com.pnb.thecodecrashers.controller.SessionManager;
import com.sharedcode.helpers.LogHelper;
import com.sharedcode.security.AesBase64Wrapper;
import com.sharedcode.utils.AppUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;


/**
 * Created by Akhil on 09/08/16.
 */
public class ApiClient {
    private static final String TAG = "<<<<Api Response>>>>";
    private static final String REQUEST_TAG = "<<<< Request >>>>";
    private static final String REQUEST_HEADER = "<<<< RequestHeader >>>>";
    private static final int TIME_OUT_MILLIS = 35000;
    private static final int MAX_RETRIES = 0;
    private static final String KEY_DEVICE_ID = "DeviceId";
    private static ApiClient mInstance = new ApiClient();

    //Create a singleton
    public static synchronized ApiClient getInstance() {
        return mInstance;
    }

    private static HashMap<String, String> addAuthorization(HashMap<String, String> hashMap) {
        if (hashMap == null) {
            return hashMap;
        }
        if (!TextUtils.isEmpty(SessionManager.getInstance().getSessionToken())) {
            hashMap.put(JsonKeys.KEY_AUTHORIZATION, "Bearer " + SessionManager.getInstance().getSessionToken());
        }
        hashMap.put(KEY_DEVICE_ID, AppUtils.getDeviceId(AppController.getInstance()));
        hashMap.put(JsonKeys.KEY_PLATFORM, AppController.getInstance().getString(R.string.app_id));
        hashMap.put(JsonKeys.KEY_VERSION_ID, AppUtils.getAppVersion(AppController.getInstance()));
        return hashMap;
    }

    //Create a get request with the url to query, and a callback
    public void requestApiGet(String url, final ApiResponse<ApiResult> completion) {
        requestApiGet(url, completion, true);
    }

    //Create a get request with the url to query, and a callback
    public void requestApiGet(String url, ApiResponse<ApiResult> completion, boolean isAsync) {
        if (isAsync)
            requestApi(Request.Method.GET, url, null, completion);
        else
            requestApi(Request.Method.GET, url, null, completion, false);
    }

    //Create a get request with the url to query, and a callback
    public void requestApiPost(String url, final JSONObject params, ApiResponse<ApiResult> completion, boolean isAsync) {
        if (isAsync)
            requestApi(Request.Method.POST, url, params, completion);
        else
            requestApi(Request.Method.POST, url, params, completion, false);
    }

    private void requestApi(int method, String url, final JSONObject params, final ApiResponse<ApiResult> completion, boolean isAsync) {
        if (!AppUtils.isProxyEnable(AppController.getInstance())) {
            LogHelper.v("Performing request: ", url);
            AppController.getInstance().getRequestQueue().getCache().remove(url);
            RequestFuture<JSONObject> future = RequestFuture.newFuture();

            JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.GET, url, (String) null, future, future) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> hashMap = new HashMap<>();
                    addAuthorization(hashMap);
                    return hashMap;
                }
            };

            jsonRequest.setRetryPolicy(new DefaultRetryPolicy(TIME_OUT_MILLIS, MAX_RETRIES, 1f));
            AppController.getInstance().addToRequestQueue(jsonRequest);

            try {
                JSONObject response = future.get();
                parseResponse(response, completion);
            } catch (InterruptedException e) {
                e.toString();
            } catch (ExecutionException e) {
                if (e.getCause() instanceof VolleyError) {
                    //grab the volley error navigatedFrom the throwable and cast it back
                    VolleyError volleyError = (VolleyError) e.getCause();
                    parseErrorResponse(volleyError, completion);
                } else {
                    parseErrorResponse(new VolleyError(e), completion);
                }
            }
        } else {
            ApiResult apiResult = new ApiResult();
            apiResult.success = false;
            apiResult.setError(new ApiError(ApiErrorType.PROXY_ENABLE, AppController.getInstance().getString(R.string.error_proxy_enable)));
            completion.onCompletion(apiResult);
        }
    }

    /**
     * @param method     : type of request this is POST, PUT etc
     * @param url        : url to query
     * @param params     : parameters to pass in the post request
     * @param completion
     */

    private void requestApi(int method, String url, final JSONObject params, final ApiResponse<ApiResult> completion) {
        if (!AppUtils.isProxyEnable(AppController.getInstance())) {
            LogHelper.v("Performing request: ", url);
            AppController.getInstance().getRequestQueue().getCache().remove(url);
            if (params != null) {
                LogHelper.v(REQUEST_TAG, params.toString());
            }
            JsonObjectRequest jsonRequest = new JsonObjectRequest(method, url, params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            parseResponse(response, completion);
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    parseErrorResponse(error, completion);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> hashMap = new HashMap<>();
                    addAuthorization(hashMap);
                    LogHelper.v(REQUEST_HEADER, hashMap.toString());
                    return hashMap;
                }
            };
            jsonRequest.setRetryPolicy(new DefaultRetryPolicy(TIME_OUT_MILLIS, MAX_RETRIES, 1f));
            AppController.getInstance().addToRequestQueue(jsonRequest, url);
        } else {
            ApiResult apiResult = new ApiResult();
            apiResult.success = false;
            apiResult.setError(new ApiError(ApiErrorType.PROXY_ENABLE, AppController.getInstance().getString(R.string.error_proxy_enable)));
            completion.onCompletion(apiResult);
        }
    }

    /**
     * @param url        : url to query
     * @param params     : parameters to pass in the post request
     * @param completion
     */

    public void requestApiPost(String url, final JSONObject params, final ApiResponse<ApiResult> completion) {
        requestApi(Request.Method.POST, url, params, completion);
    }

    public void requestApiPost(boolean cancelPendingRequests, String url, final JSONObject params, final ApiResponse<ApiResult> completion) {
        if (cancelPendingRequests) {
            AppController.getInstance().cancelPendingRequests(url);
        }
        requestApi(Request.Method.POST, url, params, completion);
    }

    /**
     * @param url        : url to query
     * @param params     : parameters to pass in the post request
     * @param completion
     */

    public void requestApiPut(String url, final JSONObject params, final ApiResponse<ApiResult> completion) {
        requestApi(Request.Method.PUT, url, params, completion);
    }

    /**
     * @param url        : url to query
     * @param request    : string to pass in the post request
     * @param completion
     */
    public void encryptedRequestApiPost(String url, final String request, final ApiResponse<ApiResult> completion) {
        encryptedRequestApi(Request.Method.POST, url, request, completion);
    }

    /**
     * @param method     : type of request this is POST, PUT etc
     * @param url        : url to query
     * @param request    : parameters to pass in the post request
     * @param completion
     */
    private void encryptedRequestApi(int method, String url, final String request, final ApiResponse<ApiResult> completion) {
        LogHelper.v("Performing request: ", url);
        if (!TextUtils.isEmpty(request)) {
            LogHelper.v(REQUEST_TAG, request);
        }
        // enclosing with { } because server returns 400 bad request error otherwise
        String encodedRequest = AesBase64Wrapper.getInstance().encryptAndEncode(request);
        if (!TextUtils.isEmpty(encodedRequest)) {
            LogHelper.v("Encrypted  Request" + REQUEST_TAG, encodedRequest);
        }
        final String finalEncodedRequest = encodedRequest;
        StringRequest jsonRequest = new StringRequest(method, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                JSONObject obj = null;
                try {
                    obj = new JSONObject(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                parseResponse(obj, completion);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                parseErrorResponse(error, completion);
            }
        }) {

            @Override
            public byte[] getBody() throws AuthFailureError {
                return finalEncodedRequest.getBytes();
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> hashMap = new HashMap<>();
                addAuthorization(hashMap);
                return hashMap;
            }
        };
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(TIME_OUT_MILLIS, MAX_RETRIES, 1f));
        AppController.getInstance().addToRequestQueue(jsonRequest);
    }

    public void requestStringApi(int method, String url, final ApiResponse<ApiResult> completion) {
        LogHelper.v("Performing request: ", url);

        StringRequest stringRequest = new StringRequest(method, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseResponse(response, completion);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                parseErrorResponse(error, completion);
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(TIME_OUT_MILLIS, MAX_RETRIES, 1f));
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    public <T> void uploadFile(int method, final String url,
                               final Map<String, File> fileParams,
                               final Map<String, String> stringParams,
                               final ApiClient.ApiResponse<ApiClient.ApiResult> completion,
                               MultipartRequest.MultipartProgressListener progListener) {
        if (!AppUtils.isProxyEnable(AppController.getInstance())) {
            LogHelper.v("<<<<<<<Request>>>>>>", stringParams.toString());
            AppController.getInstance().getRequestQueue().getCache().remove(url);
            MultipartRequest mr = new MultipartRequest(method, url, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    parseErrorResponse(error, completion);
                }
            },
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject object = new JSONObject(response);
                                parseResponse(object, completion);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, fileParams, stringParams, progListener) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> hashMap = new HashMap<>();
                    addAuthorization(hashMap);
                    return hashMap;
                }
            };

            mr.setRetryPolicy(new DefaultRetryPolicy(TIME_OUT_MILLIS, MAX_RETRIES, 1f));
            AppController.getInstance().addToRequestQueue(mr);

        } else {
            ApiResult apiResult = new ApiResult();
            apiResult.success = false;
            apiResult.setError(new ApiError(ApiErrorType.PROXY_ENABLE, AppController.getInstance().getString(R.string.error_proxy_enable)));
            completion.onCompletion(apiResult);
        }
    }

    private ApiError getVolleyApiError(VolleyError error) {
        ApiError apiError = null;

        if (error instanceof TimeoutError) {
            apiError = new ApiError(ApiErrorType.TIMEOUT_ERROR, AppController.getInstance().getString(R.string.error_network_timeout));
        } else if (error instanceof AuthFailureError) {
            apiError = new ApiError(ApiErrorType.AUTH_FAILURE_ERROR, AppController.getInstance().getString(R.string.error_auth_failure));
//            SessionManager.getInstance().logOut(true);
        } else if (error instanceof NoConnectionError) {
            apiError = new ApiError(ApiErrorType.NO_CONNECTION_ERROR, AppController.getInstance().getString(R.string.error_no_internet_connection));
        } else if (error instanceof NetworkError) {
            apiError = new ApiError(ApiErrorType.NETWORK_ERROR, AppController.getInstance().getString(R.string.error_network));
        } else if (error instanceof ParseError) {
            apiError = new ApiError(ApiErrorType.PARSE_ERROR, AppController.getInstance().getString(R.string.error_parse));
        } else if (error instanceof ServerError && error.networkResponse != null && error.networkResponse.statusCode == HttpStatus.SC_MOVED_TEMPORARILY) {
            apiError = new ApiError(ApiErrorType.REDIRECT, AppController.getInstance().getString(R.string.error_redirect));
        } else if (error instanceof ServerError) {
            apiError = new ApiError(ApiErrorType.SERVER_ERROR, AppController.getInstance().getString(R.string.error_server));
        } else {
            apiError = new ApiError(ApiErrorType.UNKNOWN, AppController.getInstance().getString(R.string.error_unknown), ApiErrorCode.UNKNOWN_CODE);
        }

        return apiError;
    }

    private void parseErrorResponse(VolleyError error, ApiResponse<ApiResult> result) {
        ApiResult res = new ApiResult();
        res.success = false;
        if (error != null && error.networkResponse != null && error.networkResponse.data != null) {
            try {
                String errorStr = new String(error.networkResponse.data);
                LogHelper.e(TAG, errorStr);
                JSONObject errorObj = new JSONObject(errorStr);
                JSONObject errorJson = new JSONObject(errorObj.getString(JsonKeys.KEY_ERRORS));
                res.data = errorJson;
                res.error = new ApiError(errorJson.optString(JsonKeys.KEY_ERROR_TYPE, null), errorJson.optString(JsonKeys.KEY_ERROR_DESCRIPTION), errorJson.optInt(JsonKeys.KEY_ERROR_CODE, ApiErrorCode.UNKNOWN_CODE));
                res.error.data = errorJson;
                if (res.error.getType() != null && res.error.getType() == ApiErrorType.TOKEN_EXPIRED) {
                    SessionManager.getInstance().logOut(true);
                }
            } catch (JSONException e) {
                res.error = getVolleyApiError(error);
                LogHelper.v("exception catch", e.getMessage());
            }
        } else {
            res.error = getVolleyApiError(error);
        }
        result.onCompletion(res);
    }

    private void parseResponse(JSONObject response, ApiResponse<ApiResult> result) {
        ApiResult res = new ApiResult();
        if (response != null) {
            LogHelper.v(TAG, response.toString());
            try {
                try {
                    res.data = response.getJSONArray("data");
                } catch (JSONException e) {
                    LogHelper.v("exception catch", e.getMessage());
                    try {
                        res.data = new JSONObject(response.getString("data"));
                    } catch (JSONException x) {
                        LogHelper.v("exception catch w", x.getMessage());
                        res.data = response.getInt("data");
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
                res.data = response;
            }
            res.success = true;
        } else {
            res.message = "Object is Null";
        }
        result.onCompletion(res);
    }

    private void parseResponse(String response, ApiResponse<ApiResult> result) {
        ApiResult apiResult = new ApiResult();
        if (response != null) {
            LogHelper.v(TAG, response.toString());
            apiResult.data = response;
            apiResult.success = true;
            result.onCompletion(apiResult);
        }
    }

    /**
     * Callback interface for delivering parsed responses.
     */
    public interface Listener<T, U> {
        /**
         * Called when a response is received.
         */
        public void onResponse(T response);

        /**
         * Called when a response is received.
         */
        public void onError(U error);
    }

    //create an interface with a callback method
    public interface ApiResponse<T> {
        public void onCompletion(T result);
    }

    //Create an object to return the server's response
    public class ApiResult {
        public Boolean success;
        public String message;
        public Object data;
        public ApiError error;

        //cheøck what kind of data is returned in the json
        public boolean dataIsArray() {
            return (data != null && data instanceof JSONArray);
        }

        public boolean dataIsObject() {
            return (data != null && data instanceof JSONObject);
        }

        public boolean dataIsInteger() {
            return (data != null && data instanceof Integer);
        }

        public boolean dataIsString() {
            return (data != null && data instanceof String);
        }

        //return the data properly casted
        public JSONArray getDataAsArray() {
            if (this.dataIsArray()) {
                return (JSONArray) this.data;
            } else {
                return null;
            }
        }

        public JSONObject getDataAsObject() {
            if (this.dataIsObject()) {
                return (JSONObject) this.data;
            } else {
                return null;
            }
        }

        public String getDataAsString() {
            if (this.dataIsString()) {
                return (String) this.data;
            } else {
                return null;
            }
        }

        public Integer getDataAsInteger() {
            if (this.dataIsInteger()) {
                return (Integer) this.data;
            } else {
                return null;
            }
        }

        public ApiError getError() {
            return error;
        }

        public void setError(ApiError error) {
            this.error = error;
        }
    }
}