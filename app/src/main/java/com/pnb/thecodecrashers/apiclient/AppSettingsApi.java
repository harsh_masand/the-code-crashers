package com.pnb.thecodecrashers.apiclient;

import android.content.Context;
import org.json.JSONObject;

/**
 * Created by Kishor on 6/15/17.
 */

public class AppSettingsApi extends Api {
    private final static String KEY_VALUE = "bnb_customer_android";
    private final static String PATH_API = "/settings";
    private StringBuilder url = new StringBuilder(Api.SERVER_HOST_NAME + PATH_API);

    public AppSettingsApi(Context context) {
        createUrl(JsonKeys.KEY_PLATFORM.toLowerCase(), "" + KEY_VALUE);
        createUrl(JsonKeys.KEY_TIMESTAMP, "" + System.currentTimeMillis());
    }

    public void call(final ApiClient.Listener<JSONObject, ApiError> listener) {
        addParams(url);
        ApiClient.getInstance().requestApiGet(url.toString(), new ApiClient.ApiResponse<ApiClient.ApiResult>() {
            @Override
            public void onCompletion(ApiClient.ApiResult result) {
                if (result.success) {
                    listener.onResponse(result.getDataAsObject());
                } else {
                    listener.onError(result.getError());
                }
            }
        });
    }
}
