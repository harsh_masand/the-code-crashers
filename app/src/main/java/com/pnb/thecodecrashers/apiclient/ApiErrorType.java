package com.pnb.thecodecrashers.apiclient;

/**
 * Created by Akhil on 11/08/16.
 */
public enum ApiErrorType {

    INVALID_PARAMETER,
    INVALID_GRANT,
    TIMEOUT_ERROR,
    NO_CONNECTION_ERROR,
    AUTH_FAILURE_ERROR,
    NETWORK_ERROR,
    PARSE_ERROR,
    SERVER_ERROR,
    NOT_FOUND,
    UNKNOWN,
    VALIDATION,
    REDIRECT,
    TOKEN_EXPIRED,
    PROXY_ENABLE,
    NO_ITEMS_FOUND,
    SHOP_CLOSED,
    WRONG_ACTION,
    WALLET_ERROR,
    INVALID_REQUEST,
    INVALID_REFERRAL_CODE,
    ALREADY_REDEEMED,
    INVALID_ASSOCIATE;

    public String toEnum(String string) {
        return super.toString();
    }
}
