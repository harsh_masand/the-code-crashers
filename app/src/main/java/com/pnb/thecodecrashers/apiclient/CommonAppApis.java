package com.pnb.thecodecrashers.apiclient;

import android.content.Context;
import com.pnb.thecodecrashers.controller.SettingsManager;
import org.json.JSONObject;

/**
 * Created by Kishor on 6/15/17.
 */

public class CommonAppApis {
    public static void appSettingsApi(final Context context) {
        AppSettingsApi settingsApi = new AppSettingsApi(context);
        settingsApi.call(new ApiClient.Listener<JSONObject, ApiError>() {
            @Override
            public void onResponse(JSONObject response) {
                if (response != null) SettingsManager.saveAppSettings(context, response);
            }

            @Override
            public void onError(ApiError error) {
            }
        });

    }
}
