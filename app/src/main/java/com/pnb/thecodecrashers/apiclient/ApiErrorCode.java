package com.pnb.thecodecrashers.apiclient;

/**
 * Created by Prashant on 31/03/17.
 */

public class ApiErrorCode {
    public static final int INSUFFICIENT_RETAILER_BALANCE = 101;
    public static final int UNKNOWN_CODE = -1;
}
