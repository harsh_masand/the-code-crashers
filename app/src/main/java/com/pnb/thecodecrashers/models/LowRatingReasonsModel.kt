package com.pnb.thecodecrashers.models

class LowRatingReasonsModel(val reasonId: Int, val reason: String) {


    companion object {
        fun getDummyData(): ArrayList<LowRatingReasonsModel> {

            val list = ArrayList<LowRatingReasonsModel>()
            for (i in 1..5) {
                val lowRatingReasonsModel = LowRatingReasonsModel(i, "Order Delivery ")
                list.add(lowRatingReasonsModel)
            }

            return list
        }
    }

}