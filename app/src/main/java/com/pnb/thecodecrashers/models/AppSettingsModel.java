package com.pnb.thecodecrashers.models;

import com.google.gson.annotations.SerializedName;
import com.pnb.thecodecrashers.apiclient.JsonKeys;

import java.util.ArrayList;


/**
 * Created by Kishor on 6/15/17.
 */

public class AppSettingsModel {
    /**
     * Latest available version of the app
     */
    @SerializedName(JsonKeys.KEY_LATEST_VERSION)
    private String latestVersion;

    /**
     * Boolean to check if the current version of the app is deprecated
     */
    @SerializedName(JsonKeys.KEY_IS_DEPRECATED)
    private boolean isDeprecated;

    /**
     * To display the features available in the new app version to which we are asking the user to update
     */
    @SerializedName(JsonKeys.KEY_UPDATE_FEATURES)
    private String updateFeatures;

    /**
     * Server timestamp to validate if the device current time accuracy
     */
    @SerializedName(JsonKeys.KEY_SERVER_TIME_DIFFERENCE)
    private long serverTimeDifference;

    @SerializedName(JsonKeys.KEY_SYNC_PERIOD)
    private long syncPeriod;

    @SerializedName(JsonKeys.KEY_SUPPORT_EMAIL)
    private String supportEmail;

    @SerializedName(JsonKeys.KEY_SUPPORT_NUMBER)
    private String customerCareNumber;

    private ArrayList<LowRatingReasonsModel> lowRatingReasonsList;

    public String getLatestVersion() {
        return latestVersion;
    }

    public void setLatestVersion(String latestVersion) {
        this.latestVersion = latestVersion;
    }

    public boolean isDeprecated() {
        return isDeprecated;
    }

    public void setDeprecated(boolean deprecated) {
        isDeprecated = deprecated;
    }

    public String getUpdateFeatures() {
        return updateFeatures;
    }

    public void setUpdateFeatures(String updateFeatures) {
        this.updateFeatures = updateFeatures;
    }

    public long getServerTimeDifference() {
        return serverTimeDifference;
    }

    public void setServerTimeDifference(long serverTimeDifference) {
        this.serverTimeDifference = serverTimeDifference;
    }

    public long getSyncPeriod() {
        return syncPeriod;
    }

    public void setSyncPeriod(long syncPeriod) {
        this.syncPeriod = syncPeriod;
    }

    public String getSupportEmail() {
        return supportEmail;
    }

    public void setSupportEmail(String supportEmail) {
        this.supportEmail = supportEmail;
    }

    public String getCustomerCareNumber() {
        return customerCareNumber;
    }

    public void setCustomerCareNumber(String customerCareNumber) {
        this.customerCareNumber = customerCareNumber;
    }
}
