package com.pnb.thecodecrashers.models;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.TextView;
import com.google.gson.annotations.SerializedName;
import thecodecrashers.R;
import com.pnb.thecodecrashers.apiclient.JsonKeys;

import java.util.ArrayList;
import java.util.List;

public class DialogOption {

    @SerializedName(JsonKeys.KEY_ID)
    private Integer mOptionId;

    @SerializedName(JsonKeys.KEY_NAME)
    private String mOptionText;

    private int mPriority;
    private boolean mIsChecked;

    public DialogOption() {
    }

    public DialogOption(Integer id, String reason) {
        this.mOptionId = id;
        this.mOptionText = reason;
    }

    public static List<View> getRadioOptions(Context context, List<DialogOption> dialogOptionList) {
        List<View> radioOptions = new ArrayList<>();
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        for (DialogOption dialogOption : dialogOptionList) {
            RadioButton radioButton = (RadioButton) layoutInflater.inflate(R.layout.view_radio_button, null, false);
            radioButton.setChecked(dialogOption.isChecked());
            radioButton.setId(dialogOption.getOptionId());
            radioButton.setText(dialogOption.getOptionText());

            radioOptions.add(radioButton);
        }
        return radioOptions;
    }

    public static List<View> getCheckBoxOptions(Context context, List<DialogOption> dialogOptionList) {
        List<View> checkBoxOptions = new ArrayList<>();
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        for (DialogOption dialogOption : dialogOptionList) {
            CheckBox checkBox = (CheckBox) layoutInflater.inflate(R.layout.view_checkbox_layout, null, false);
            checkBox.setChecked(dialogOption.isChecked());
            checkBox.setId(dialogOption.getOptionId());
            checkBox.setText(dialogOption.getOptionText());

            checkBoxOptions.add(checkBox);
        }
        return checkBoxOptions;
    }

    public static List<View> getTextViewOptions(Context context, List<DialogOption> dialogOptionList) {
        List<View> options = new ArrayList<>();
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        for (DialogOption dialogOption : dialogOptionList) {
            TextView textView = (TextView) layoutInflater.inflate(R.layout.view_textview_layout, null, false);
            textView.setId(dialogOption.getOptionId());
            textView.setText(dialogOption.getOptionText());
            options.add(textView);
        }
        return options;
    }

    public Integer getOptionId() {
        return mOptionId;
    }

    public void setOptionId(Integer mOptionId) {
        this.mOptionId = mOptionId;
    }

    public String getOptionText() {
        return mOptionText;
    }

    public void setOptionText(String mOptionText) {
        this.mOptionText = mOptionText;
    }

    public boolean isChecked() {
        return mIsChecked;
    }

    public void setChecked(boolean checked) {
        mIsChecked = checked;
    }

    public int getPriority() {
        return mPriority;
    }

    public void setPriority(int mPriority) {
        this.mPriority = mPriority;
    }
}
