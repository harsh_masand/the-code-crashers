package com.pnb.thecodecrashers

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import thecodecrashers.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
