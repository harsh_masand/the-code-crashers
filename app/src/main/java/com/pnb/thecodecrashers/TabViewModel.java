package com.pnb.thecodecrashers;

import android.content.Context;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import thecodecrashers.R;

/**
 * Created by admin on 31/08/16.
 */
public class TabViewModel {

    public ObservableField<String> title = new ObservableField<>();
    public ObservableField<String> notificationCount = new ObservableField<>();
    public ObservableBoolean isNotification = new ObservableBoolean();
    public Integer imageRes;
    public Integer textSelector;


    public TabViewModel() {
    }

    public static TabViewModel getTab(int titleId, int imageResId) {
        Context context = AppController.getInstance();
        TabViewModel tabViewModel = new TabViewModel();
        tabViewModel.isNotification.set(false);
        tabViewModel.notificationCount.set("");
        tabViewModel.title.set(context.getString(titleId));
        tabViewModel.setImageRes(imageResId);
        tabViewModel.setTextSelector(R.color.c_dim_grey);
        return tabViewModel;
    }

    public static TabViewModel getTab(int titleId) {
        return getTab(titleId, 0);
    }

    public Integer getImageRes() {
        return imageRes;
    }

    public void setImageRes(Integer imageRes) {
        this.imageRes = imageRes;
    }

    public Integer getTextSelector() {
        return textSelector;
    }

    public void setTextSelector(Integer textSelector) {
        this.textSelector = textSelector;
    }
}
