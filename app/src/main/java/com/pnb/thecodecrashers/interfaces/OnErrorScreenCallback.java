package com.pnb.thecodecrashers.interfaces;


import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Kishor on 5/8/17.
 */

public interface OnErrorScreenCallback {
    int TYPE_RETRY = 0;

    void errorScreenCallback(@CallbackType int callbackType);

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({TYPE_RETRY})
    @interface CallbackType {
    }
}
