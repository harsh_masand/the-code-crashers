package com.pnb.thecodecrashers.interfaces;

/**
 * Created by Tanmay on 4/3/17.
 */

public interface AdapterChangeNotify<T> {
    void refresh(int listPosition, T obj);
}
