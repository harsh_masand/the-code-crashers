package com.pnb.thecodecrashers.interfaces;

/**
 * Created by admin on 05/05/17.
 */

public interface LoadMoreListener {
    void onLoadMore();
}
