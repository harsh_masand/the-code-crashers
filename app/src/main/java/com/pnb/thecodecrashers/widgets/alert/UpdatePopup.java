package com.pnb.thecodecrashers.widgets.alert;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.view.View;
import thecodecrashers.R;
import com.pnb.thecodecrashers.controller.SettingsManager;
import com.pnb.thecodecrashers.controller.SharedPreferencesManager;
import com.pnb.thecodecrashers.models.AppSettingsModel;
import com.sharedcode.utils.AppUtils;

/**
 * Created by Kishor on 6/15/17.
 */

public class UpdatePopup {
    private static UpdatePopup mUpdatePopup;
    private AlertHelper mAlertHelper = new AlertHelper();
    private Context mContext;

    private UpdatePopup(Context context) {
        this.mContext = context;
    }

    private UpdatePopup() {
    }

    public static UpdatePopup getInstance(Context context) {
        if (mUpdatePopup == null || context != mUpdatePopup.mContext) {
            mUpdatePopup = new UpdatePopup(context);
        }
        return mUpdatePopup;
    }

    private void showDialogForAppUpdate(boolean isForceUpdate, final AppSettingsModel appSettings) {
        mAlertHelper.setRightClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAlertHelper.dismissDialog();
                redirectToPlayStore(appSettings);
            }
        });
        mAlertHelper.setRightSelector(0);
        mAlertHelper.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (appSettings != null && !TextUtils.isEmpty(appSettings.getLatestVersion())) {
                    SharedPreferencesManager.
                            setPreviousAppVersionInPrefs(appSettings.getLatestVersion());
                }
            }
        });

        if (!isForceUpdate) {
            setUnforcedUpdateValues(mAlertHelper, appSettings);
        } else {
            setForceUpdateValues(mAlertHelper);
        }

        mAlertHelper.showAlert(mContext);
    }

    private void setUnforcedUpdateValues(final AlertHelper mAlertHelper, AppSettingsModel appSettings) {
        mAlertHelper.setLeftActionTitle(mContext.getString(R.string.txt_skip));
        mAlertHelper.setLeftClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAlertHelper.dismissDialog();
            }
        });
        mAlertHelper.setTitle(mContext.getResources().getString(R.string.txt_update_title, appSettings.getLatestVersion()));
        mAlertHelper.setDescription(appSettings.getUpdateFeatures() != null ? appSettings.getUpdateFeatures()
                : mContext.getResources().getString(R.string.txt_update_popup_description));
        mAlertHelper.setRightActionTitle(mContext.getString(R.string.txt_update));
        mAlertHelper.setLeftButtonTextColor(R.color.watermelon);
        mAlertHelper.setRightButtonTextColor(R.color.colorPrimary);
        mAlertHelper.setCancelOnOutsideTouch(false);
        mAlertHelper.setCancelable(true);
    }

    private void setForceUpdateValues(AlertHelper mAlertHelper) {
        mAlertHelper.setTitle(mContext.getResources().getString(R.string.txt_force_update_title));
        mAlertHelper.setDescription(mContext.getResources().getString(R.string.txt_update_popup_description));
        mAlertHelper.setRightActionTitle(mContext.getString(R.string.txt_update));
        mAlertHelper.setRightButtonTextColor(R.color.colorPrimary);
        mAlertHelper.setCancelable(false);
        mAlertHelper.setCancelOnOutsideTouch(false);
    }

    public void checkAndShow() {
        final AppSettingsModel appSettings = SettingsManager.getAppSettings(mContext);
        if (appSettings != null && !TextUtils.isEmpty(appSettings.getLatestVersion())) {

            final String latestVersion = appSettings.getLatestVersion();

            /* Getting Last Version for which the pop was shown */
            String lastVersionPopupShown = SharedPreferencesManager.getPreviousAppVersionInPrefs();

            /* Getting current app version deprecated */
            boolean isObsolete = appSettings.isDeprecated();

            /* Getting current App Version */
            final String currentVersion = AppUtils.getAppVersion(mContext);

            /*
             * Comparing current latest version and current version to check whether
             * latest version is available
             */
            boolean newVersionAvailable = AppUtils.compareVersions(currentVersion,
                    latestVersion) < 0;

            /* Checking the pop up for same latest version has been already shown */
            boolean doNotAskAgain = AppUtils.compareVersions(latestVersion,
                    lastVersionPopupShown) < 1;

            /*
             * If currentVersion is not deprecated and new Version is also not
             * available then do nothing
             */
            if (!isObsolete && !newVersionAvailable)
                return;
            if (!isObsolete && doNotAskAgain)
                return;

            showDialogForAppUpdate(isObsolete, appSettings);
        }
    }

    private void redirectToPlayStore(AppSettingsModel appSettings) {
        SharedPreferencesManager.
                setPreviousAppVersionInPrefs(appSettings
                        .getLatestVersion());

        try {
            mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + mContext.getPackageName())));
        } catch (android.content.ActivityNotFoundException exception) {
            mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + mContext.getPackageName())));
        }
    }
}
