package com.pnb.thecodecrashers.widgets.alert;

import android.graphics.drawable.Drawable;
import android.text.Spanned;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RadioGroup;
import androidx.lifecycle.MutableLiveData;
import com.pnb.thecodecrashers.interfaces.ViewModel;

import java.util.HashMap;

/**
 * Created by admin on 06/09/16.
 */
public class AlertViewModel implements ViewModel {


    /* ------------------------------------------------------------------------------------------- *
     ****************************************** TEXTS **********************************************
     * ------------------------------------------------------------------------------------------- */
    public static final String KEY_LOADING_TITLE = "loading_title";
    public static final String KEY_LOADING_DESCRIPTION = "loading_description";
    public static final String KEY_LOAD_ERROR_BUTTON_LABEL = "load_error_button_label";
    public static final String KEY_INDETERMINATE_DRAWABLE = "indeterminate_drawable";

    /* ------------------------------------------------------------------------------------------- *
     ********************************** CLICK LISTENERS ********************************************
     * ------------------------------------------------------------------------------------------- */
    public static final String KEY_LOAD_ERROR_BUTTON_CLICK_LISTENER = "load_error_click_listener";
    public static final String KEY_TITLE = "title";
    public static final String KEY_SUB_TITLE_RIGHT = "sub_title_right";
    public static final String KEY_DESCRIPTION = "description";
    public static final String KEY_RIGHT_ACTION_TITLE = "right_action_title";
    public static final String KEY_LEFT_ACTION_TITLE = "left_action_title";
    public static final String KEY_ICON_RESOURCE = "icon_resource";
    public static final String KEY_RIGHT_SELECTOR = "right_selector";
    public static final String KEY_LEFT_SELECTOR = "left_selector";
    public static final String KEY_RIGHT_BUTTON_TEXT_COLOR = "right_button_text_color";
    public static final String KEY_LEFT_BUTTON_TEXT_COLOR = "left_button_text_color";
    public static final String KEY_RIGHT_CLICK = "right_click";
    public static final String KEY_LEFT_CLICK = "left_click";
    public static final String KEY_CLOSE_CLICK = "close_click";
    public static final String KEY_DISPLAY_LOADER = "display_loader";
    public static final String KEY_DISPLAY_LOADING_ERROR = "display_loading_error";
    public static final String KEY_LOADING_MESSAGE = "loading_message";
    public static final String KEY_LOAD_ERROR_MESSAGE = "load_error_message";
    public static final String KEY_LOAD_ERROR_CLICK = "load_error_click";
    public MutableLiveData<String> loadingTitle = new MutableLiveData<>();
    public MutableLiveData<String> loadingDescription = new MutableLiveData<>();
    public MutableLiveData<String> loadingErrorButtonLabel = new MutableLiveData<>();
    public MutableLiveData<Boolean> displayLoader = new MutableLiveData<>();
    public MutableLiveData<Drawable> indeterminateDrawable = new MutableLiveData<>();

    public MutableLiveData<String> title = new MutableLiveData<>();
    public MutableLiveData<String> subTitleRight = new MutableLiveData<>();
    public MutableLiveData<String> rActionTitle = new MutableLiveData<>();
    public MutableLiveData<String> lActionTitle = new MutableLiveData<>();

    public OnClickListener rClick, lClick, closeClick, loadRetryClick;
    public MutableLiveData<Integer> iconRes = new MutableLiveData<>();
    public MutableLiveData<Integer> rSelector = new MutableLiveData<>();
    public MutableLiveData<Integer> lSelector = new MutableLiveData<>();
    public Spanned description;
    public MutableLiveData<Boolean> isCloseVisible = new MutableLiveData<>();
    public MutableLiveData<Integer> rightButtonTextColor = new MutableLiveData<>();
    public MutableLiveData<Integer> leftButtonTextColor = new MutableLiveData<>();
    public MutableLiveData<Boolean> loadingInProgress = new MutableLiveData<>();
    public MutableLiveData<Boolean> displayLoadingError = new MutableLiveData<>();
    public MutableLiveData<String> loadingMessage = new MutableLiveData<>();
    public MutableLiveData<String> loadingErrorMessage = new MutableLiveData<>();
    ///LOADING ALERT
    private OnClickListener mLoadErrorButtonClickListener;
    private Integer mSelectedRadioButtonId;

    public AlertViewModel(HashMap<String, Object> alertValues) {

        /* ---------------------------------------- TEXTS --------------------------------------- */
        if (alertValues.get(KEY_TITLE) != null) {
            title.setValue((String) alertValues.get(KEY_TITLE));
        }
        if (alertValues.get(KEY_SUB_TITLE_RIGHT) != null) {
            subTitleRight.setValue((String) alertValues.get(KEY_SUB_TITLE_RIGHT));
        }
        if (alertValues.get(KEY_DESCRIPTION) != null) {
            description = (Spanned) alertValues.get(KEY_DESCRIPTION);
        }
        if (alertValues.get(KEY_RIGHT_ACTION_TITLE) != null) {
            rActionTitle.setValue((String) alertValues.get(KEY_RIGHT_ACTION_TITLE));
        }
        if (alertValues.get(KEY_LEFT_ACTION_TITLE) != null) {
            lActionTitle.setValue((String) alertValues.get(KEY_LEFT_ACTION_TITLE));
        }

        if (alertValues.get(KEY_LOADING_MESSAGE) != null) {
            loadingMessage.setValue((String) alertValues.get(KEY_LOADING_MESSAGE));
        }

        if (alertValues.get(KEY_LOAD_ERROR_MESSAGE) != null) {
            loadingErrorMessage.setValue((String) alertValues.get(KEY_LOAD_ERROR_MESSAGE));
        }

        /* -------------------------------------- BOOLEANS -------------------------------------- */
        if (alertValues.get(KEY_DISPLAY_LOADER) != null) {
            loadingInProgress.setValue((Boolean) alertValues.get(KEY_DISPLAY_LOADER));
        }
        if (alertValues.get(KEY_DISPLAY_LOADING_ERROR) != null) {
            displayLoadingError.setValue((Boolean) alertValues.get(KEY_DISPLAY_LOADING_ERROR));
        }

        /* ------------------------------------- DRAWABLES -------------------------------------- */
        if (alertValues.get(KEY_ICON_RESOURCE) != null) {
            iconRes.setValue((Integer) alertValues.get(KEY_ICON_RESOURCE));
        }
        if (alertValues.get(KEY_RIGHT_SELECTOR) != null) {
            rSelector.setValue((Integer) alertValues.get(KEY_RIGHT_SELECTOR));
        }
        if (alertValues.get(KEY_LEFT_SELECTOR) != null) {
            lSelector.setValue((Integer) alertValues.get(KEY_LEFT_SELECTOR));
        }
        if (alertValues.get(KEY_RIGHT_BUTTON_TEXT_COLOR) != null) {
            rightButtonTextColor.setValue((Integer) alertValues.get(KEY_RIGHT_BUTTON_TEXT_COLOR));
        }
        if (alertValues.get(KEY_LEFT_BUTTON_TEXT_COLOR) != null) {
            leftButtonTextColor.setValue((Integer) alertValues.get(KEY_LEFT_BUTTON_TEXT_COLOR));
        }

        /* ------------------------------------ CLICK EVENTS ------------------------------------ */
        if (alertValues.get(KEY_RIGHT_CLICK) != null) {
            this.rClick = (OnClickListener) alertValues.get(KEY_RIGHT_CLICK);
        }
        if (alertValues.get(KEY_LEFT_CLICK) != null) {
            this.lClick = (OnClickListener) alertValues.get(KEY_LEFT_CLICK);
        }
        if (alertValues.get(KEY_CLOSE_CLICK) != null) {
            this.closeClick = (OnClickListener) alertValues.get(KEY_CLOSE_CLICK);
        }
        if (alertValues.get(KEY_LOAD_ERROR_CLICK) != null) {
            this.loadRetryClick = (OnClickListener) alertValues.get(KEY_LOAD_ERROR_CLICK);
        }

        /* ------------------------------------LOADING DIALOG-------------------------------------- */
        if (alertValues.get(KEY_LOADING_TITLE) != null) {
            loadingTitle.setValue((String) alertValues.get(KEY_LOADING_TITLE));
            displayLoadingError.setValue(true);
        }
        if (alertValues.get(KEY_LOADING_DESCRIPTION) != null) {
            loadingDescription.setValue((String) alertValues.get(KEY_LOADING_DESCRIPTION));
        }
        if (alertValues.get(KEY_LOAD_ERROR_MESSAGE) != null) {
            loadingErrorMessage.setValue((String) alertValues.get(KEY_LOAD_ERROR_MESSAGE));
        }

        /* ----------------------------- INDETERMINATE DRAWABLE --------------------------------- */
        if (alertValues.get(KEY_INDETERMINATE_DRAWABLE) != null) {
            indeterminateDrawable.setValue((Drawable) alertValues.get(KEY_INDETERMINATE_DRAWABLE));
        }

        /* ---------------------------------- CLICK LISTENERS ----------------------------------- */
        if (alertValues.get(KEY_LOAD_ERROR_BUTTON_CLICK_LISTENER) != null) {
            mLoadErrorButtonClickListener = (OnClickListener) alertValues.get(KEY_LOAD_ERROR_BUTTON_CLICK_LISTENER);
        }
    }

    public void onRightClick(View view) {
        if (rClick != null)
            rClick.onClick(view);
    }

    public void onLeftClick(View view) {
        if (lClick != null)
            lClick.onClick(view);
    }

    public void setIsCloseVisible(boolean isCloseVisible) {
        this.isCloseVisible.setValue(isCloseVisible);
    }

    public void onClose(View view) {
        if (closeClick != null)
            closeClick.onClick(view);
    }

    public void onLoadingRetryClick(View view) {
        if (loadRetryClick != null) {
            loadRetryClick.onClick(view);
        }
    }

    public void onLoadingErrorButtonClick(View view) {
        if (mLoadErrorButtonClickListener != null) {
            mLoadErrorButtonClickListener.onClick(view);
        }
    }

    @Override
    public void close() {
    }

    public void onRadioButtonCheckedChangeListener(RadioGroup radioGroup, int selectedOption) {
        mSelectedRadioButtonId = selectedOption;
    }

    public Integer getSelectedOption() {
        return mSelectedRadioButtonId;
    }


    public void setDisplayLoader(boolean displayLoader) {
        this.loadingInProgress.setValue(displayLoader);
        this.displayLoader.setValue(displayLoader);
    }

    public void setDisplayLoadingError(boolean displayError) {
        this.displayLoadingError.setValue(displayError);
    }

    public void setLoadingErrorMessage(String message) {
        this.loadingErrorMessage.setValue(message);
    }

    public void setActionButton(OnClickListener onClickListener, String buttonText) {
        this.loadingErrorButtonLabel.setValue(buttonText);
        this.mLoadErrorButtonClickListener = onClickListener;
    }

    public void setLoadingRetryClickListener(OnClickListener onClickListener) {
        this.mLoadErrorButtonClickListener = onClickListener;
    }
}