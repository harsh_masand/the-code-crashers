package com.pnb.thecodecrashers.widgets.alert;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.text.Spanned;
import android.view.*;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.databinding.DataBindingUtil;
import thecodecrashers.R;
import thecodecrashers.databinding.AlertViewBinding;

import com.pnb.thecodecrashers.models.DialogOption;
import com.sharedcode.helpers.LogHelper;
import com.sharedcode.utils.AppUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by admin on 06/09/16.
 */
public class AlertHelper {
    public AlertViewBinding mBinding;
    private String mTitle, mSubTitleRight = "", mRightActionTitle, mLeftActionTitle, mLoadingMessage, mLoadingErrorMessage;
    private View.OnClickListener mRightClickListener, mLeftClickListener, mCloseClickListener, mLoadingRetryClickListener;
    private int mIconResource, mRightSelector, mLeftSelector;
    private Dialog mDialog;
    private boolean mIsCloseVisible;
    private boolean mIsCancelable = true;
    private boolean mIsCancelOnOutsideTouch = true;
    private DialogInterface.OnDismissListener mOnDismissListener;
    private List<DialogOption> mDialogOptionList = new ArrayList<>();
    private AlertViewModel mAlertViewModel;
    private Spanned mDescription;
    private boolean mDisplayLoader, mDisplayLoadingError;
    private int mRightButtonTextColor = R.color.c_pink;
    private int mLeftButtonTextColor = R.color.c_greyish;
    private String mLoadingTitle, mLoadingDescription, mLoadingButtonLabel;
    private Drawable mIndeterminateDrawable;

    public void showAlert(Context context) {
        try {
            if (mDialog == null || (mDialog != null && !mDialog.isShowing())) {
                mDialog = new Dialog(context);
                mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                HashMap<String, Object> dialogValues = new HashMap<>();
                /* ---------------------------------------- TEXTS ----------------------------------- */
                dialogValues.put(AlertViewModel.KEY_TITLE, mTitle);
                dialogValues.put(AlertViewModel.KEY_SUB_TITLE_RIGHT, mSubTitleRight);
                dialogValues.put(AlertViewModel.KEY_DESCRIPTION, mDescription);
                dialogValues.put(AlertViewModel.KEY_RIGHT_ACTION_TITLE, mRightActionTitle);
                dialogValues.put(AlertViewModel.KEY_LEFT_ACTION_TITLE, mLeftActionTitle);
                dialogValues.put(AlertViewModel.KEY_LOADING_MESSAGE, mLoadingMessage);
                dialogValues.put(AlertViewModel.KEY_LOAD_ERROR_MESSAGE, mLoadingErrorMessage);

                /* ----------------------------------- BOOLEANS ------------------------------------ */
                dialogValues.put(AlertViewModel.KEY_DISPLAY_LOADER, mDisplayLoader);
                dialogValues.put(AlertViewModel.KEY_DISPLAY_LOADING_ERROR, mDisplayLoadingError);

                /* ----------------------------------- DRAWABLES ------------------------------------ */
                dialogValues.put(AlertViewModel.KEY_ICON_RESOURCE, mIconResource);
                dialogValues.put(AlertViewModel.KEY_RIGHT_SELECTOR, mRightSelector);
                dialogValues.put(AlertViewModel.KEY_LEFT_SELECTOR, mLeftSelector);
                dialogValues.put(AlertViewModel.KEY_RIGHT_BUTTON_TEXT_COLOR, mRightButtonTextColor);
                dialogValues.put(AlertViewModel.KEY_LEFT_BUTTON_TEXT_COLOR, mLeftButtonTextColor);

                /* ----------------------------------- CLICK EVENTS --------------------------------- */
                dialogValues.put(AlertViewModel.KEY_RIGHT_CLICK, mRightClickListener);
                dialogValues.put(AlertViewModel.KEY_LEFT_CLICK, mLeftClickListener);
                dialogValues.put(AlertViewModel.KEY_CLOSE_CLICK, mCloseClickListener);
                dialogValues.put(AlertViewModel.KEY_LOAD_ERROR_CLICK, mLoadingRetryClickListener);

                mAlertViewModel = new AlertViewModel(dialogValues);
                mAlertViewModel.setIsCloseVisible(mIsCloseVisible);
                setDialogAttributes();
            }
        } catch (Exception e) {
            LogHelper.e("AlertHelper", e.toString());
        }
    }

    private void setDialogAttributes() {
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(mDialog.getContext()), R.layout.alert_view, null, false);
        mBinding.setVm(mAlertViewModel);
        mDialog.setContentView(mBinding.getRoot());

        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(mDialog.getWindow().getAttributes());
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        layoutParams.gravity = Gravity.CENTER;

        mDialog.getWindow().setAttributes(layoutParams);
        mDialog.setCancelable(mIsCancelable);
        mDialog.setCanceledOnTouchOutside(mIsCancelOnOutsideTouch);
        mDialog.setOnDismissListener(mOnDismissListener);
        mDialog.show();
    }

    public void showLoadingAlert(Context context) {
        if (mDialog == null || (mDialog != null && !mDialog.isShowing())) {
            mDialog = new Dialog(context);
            mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            HashMap<String, Object> dialogValues = new HashMap<>();
            /* ---------------------------------------- TEXTS ----------------------------------- */
            dialogValues.put(AlertViewModel.KEY_LOADING_TITLE, mLoadingTitle);
            dialogValues.put(AlertViewModel.KEY_LOADING_DESCRIPTION, mLoadingDescription);
            dialogValues.put(AlertViewModel.KEY_LOAD_ERROR_MESSAGE, mLoadingErrorMessage);
            dialogValues.put(AlertViewModel.KEY_LOAD_ERROR_BUTTON_LABEL, mLoadingButtonLabel);

            /* ----------------------------------- CLICK EVENTS --------------------------------- */
            dialogValues.put(AlertViewModel.KEY_RIGHT_CLICK, mRightClickListener);
            dialogValues.put(AlertViewModel.KEY_LEFT_CLICK, mLeftClickListener);
            dialogValues.put(AlertViewModel.KEY_LOAD_ERROR_CLICK, mLoadingRetryClickListener);

            if (mIndeterminateDrawable == null) {
                mIndeterminateDrawable = AppCompatResources.getDrawable(context, R.drawable.drawable_circular_progress);
            }
            dialogValues.put(AlertViewModel.KEY_INDETERMINATE_DRAWABLE, mIndeterminateDrawable);

            mAlertViewModel = new AlertViewModel(dialogValues);
            setDialogAttributes();
        }
    }

    public void setLoadingTitle(String title) {
        mLoadingTitle = title;
    }

    public void setLoadingDescription(String description) {
        mLoadingDescription = description;
    }

    public void setLoadingErrorButtonLabel(String buttonLabel) {
        mLoadingButtonLabel = buttonLabel;
    }

    public void setIsCancelable(boolean isCancelable) {
        mIsCancelable = isCancelable;
    }

    public void setIsCancelOnOutsideTouch(boolean isCancelOnOutsideTouch) {
        mIsCancelOnOutsideTouch = isCancelOnOutsideTouch;
    }

    public void setIndeterminateDrawable(Drawable indeterminateDrawable) {
        this.mIndeterminateDrawable = indeterminateDrawable;
    }

    public Dialog getDialogue() {
        return mDialog;
    }

    public AlertHelper setTitle(String title) {
        this.mTitle = title;
        return this;
    }

    public AlertHelper setDescription(String description) {
        this.mDescription = AppUtils.fromHtml(description);
        return this;
    }

    public AlertHelper setLeftActionTitle(String leftActionTitle) {
        this.mLeftActionTitle = leftActionTitle;
        return this;
    }

    public AlertHelper setRightActionTitle(String rightActionTitle) {
        this.mRightActionTitle = rightActionTitle;
        return this;
    }

    public AlertHelper setCloseClickListener(View.OnClickListener closeClickListener) {
        this.mCloseClickListener = closeClickListener;
        return this;
    }

    public AlertHelper setRightClickListener(View.OnClickListener rightClickListener) {
        this.mRightClickListener = rightClickListener;
        return this;
    }

    public AlertHelper setLeftClickListener(View.OnClickListener leftClickListener) {
        this.mLeftClickListener = leftClickListener;
        return this;
    }

    public AlertHelper setIconResource(int iconResource) {
        this.mIconResource = iconResource;
        return this;
    }

    public AlertHelper setRightSelector(int rightSelector) {
        this.mRightSelector = rightSelector;
        return this;
    }

    public AlertHelper setLeftSelector(int lSelector) {
        this.mLeftSelector = lSelector;
        return this;
    }

    public AlertHelper setRightButtonTextColor(int rTextColor) {
        this.mRightButtonTextColor = rTextColor;
        return this;
    }

    public AlertHelper setLeftButtonTextColor(int lTextColor) {
        this.mLeftButtonTextColor = lTextColor;
        return this;
    }

    public AlertHelper setIsCloseVisible(boolean isVisible) {
        this.mIsCloseVisible = isVisible;
        return this;
    }

    public void dismissDialog() {
        if (mDialog != null) {
            mDialog.dismiss();
        }
    }

    public void setCancelable(boolean cancelable) {
        mIsCancelable = cancelable;
    }

    public void setCancelOnOutsideTouch(boolean cancelOnOutsideTouch) {
        mIsCancelOnOutsideTouch = cancelOnOutsideTouch;
    }

    public void setOnDismissListener(DialogInterface.OnDismissListener mOnDismissListener) {
        this.mOnDismissListener = mOnDismissListener;
    }

    public void setSubTitleRight(String subTitleRight) {
        mSubTitleRight = subTitleRight;
    }

    public Integer getSelectedOption() {
        return mAlertViewModel.getSelectedOption();
    }

    public View getDialogView() {
        return mBinding.getRoot();
    }

    public void setDisplayLoader(boolean displayLoader) {
        this.mDisplayLoader = displayLoader;
        if (mAlertViewModel != null) {
            mAlertViewModel.setDisplayLoader(displayLoader);
        }
    }

    public void setDisplayLoadingError(boolean displayLoadingError) {
        mDisplayLoadingError = displayLoadingError;
        if (mAlertViewModel != null) {
            mAlertViewModel.setDisplayLoadingError(displayLoadingError);
        }
    }

    public void setLoadingMessage(String loadingMessage) {
        mLoadingMessage = loadingMessage;
    }

    public void setLoadingErrorMessage(String errorMessage) {
        mLoadingErrorMessage = errorMessage;
        if (mAlertViewModel != null) {
            mAlertViewModel.setLoadingErrorMessage(errorMessage);
        }
    }

    public void setActionButtonListener(View.OnClickListener clickListener, String buttonText) {
        mLoadingRetryClickListener = clickListener;
        if (mAlertViewModel != null) {
            mAlertViewModel.setActionButton(clickListener, buttonText);
        }
    }

    public void setLoadingRetryClickListener(View.OnClickListener clickListener) {
        mLoadingRetryClickListener = clickListener;
        if (mAlertViewModel != null) {
            mAlertViewModel.setLoadingRetryClickListener(clickListener);
        }
    }
}
