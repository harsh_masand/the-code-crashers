package com.pnb.thecodecrashers;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import com.pnb.thecodecrashers.utils.ViewUtils;
import com.pnb.thecodecrashers.widgets.alert.UpdatePopup;
import pl.tajchert.nammu.Nammu;
import thecodecrashers.R;

/**
 * Created by Akhil on 08/08/16.
 */
public abstract class BaseActivity extends com.sharedcode.baseclasses.BaseActivity {

    public static final String TRANSACTION_DEFAULT = "default";
    public static final String TRANSACTION_POPUP = "popup";
    private boolean mPaused;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!isTaskRoot()) {
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }
        mPaused = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPaused = false;
        UpdatePopup.getInstance(this).checkAndShow();
        ViewUtils.hideSoftKeyboard(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mPaused = true;
    }

    public abstract Fragment getCurrentFragment();

    public void replaceAsFirst(Fragment f) {
        replaceFragment(f, false, FragmentTransactionAnimationType.NONE, TRANSACTION_DEFAULT);
    }

    public void replaceFragment(Fragment f,
                                FragmentTransactionAnimationType animationType) {
        replaceFragment(f, true, animationType, TRANSACTION_DEFAULT);
    }

    public void replaceFragment(Fragment f, boolean addToStack,
                                FragmentTransactionAnimationType animationType) {
        replaceFragment(f, addToStack, animationType, TRANSACTION_DEFAULT);
    }

    public void replaceFragment(Fragment f,
                                FragmentTransactionAnimationType animationType,
                                boolean isPopup) {
        replaceFragment(f, true, animationType, isPopup ? TRANSACTION_POPUP
                : TRANSACTION_DEFAULT);
    }

    public void replaceFragment(Fragment f, boolean addToStack,
                                FragmentTransactionAnimationType animationType,
                                boolean isPopup) {
        replaceFragment(f, addToStack, animationType, isPopup ? TRANSACTION_POPUP
                : TRANSACTION_DEFAULT);
    }

    /**
     * This method replaces the current fragment with the param fragment and sets a tag to this transaction.
     *
     * @param f             The new fragment
     * @param animationType
     * @param tag           The tag to set to the transaction.
     */
    public void replaceFragment(Fragment f, FragmentTransactionAnimationType animationType, String tag) {
        replaceFragment(f, true, animationType, tag);
    }

    @SuppressLint("NewApi")
    public void replaceFragment(Fragment f, boolean addToStack,
                                FragmentTransactionAnimationType animationType,
                                String transactionType) {
        String currentFragmentName = "";
        Fragment fragment = getCurrentFragment();
        if (fragment != null)
            currentFragmentName = fragment.getClass().toString();
        if (f.getClass().toString().equalsIgnoreCase(currentFragmentName))
            return;

        if (!mPaused) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();

            if (addToStack)
                transaction.addToBackStack(transactionType);

            if (animationType == FragmentTransactionAnimationType.FROM_RIGHT) {
                transaction.setCustomAnimations(R.anim.slide_in_right,
                        R.anim.slide_out_left, R.anim.slide_in_left,
                        R.anim.slide_out_right);
            } else if (animationType == FragmentTransactionAnimationType.FROM_BOTTOM) {
                transaction.setCustomAnimations(R.anim.slide_in_down, 0, 0,
                        R.anim.slide_out_down);
            } else if (animationType == FragmentTransactionAnimationType.FROM_RIGHT_TO_BOTTOM) {
                transaction.setCustomAnimations(R.anim.slide_in_right, 0, 0,
                        R.anim.slide_out_down);
            } else if (animationType == FragmentTransactionAnimationType.FROM_TOP) {
                transaction.setCustomAnimations(R.anim.slide_in_up, 0, 0,
                        R.anim.slide_out_up);
            } else if (animationType == FragmentTransactionAnimationType.FADE_IN) {
                transaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
            } else {
                transaction
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
            }

            transaction.replace(R.id.content_main, f);
            transaction.commit();
        } else {
            //Do not commit transaction as activity has lost it's state.
        }
    }

    @SuppressLint("NewApi")
    public void addFragment(Fragment f, boolean addToStack,
                            FragmentTransactionAnimationType animationType) {
        addFragment(f, addToStack, animationType, TRANSACTION_DEFAULT);
    }

    @SuppressLint("NewApi")
    public void addFragment(Fragment f, boolean addToStack,
                            FragmentTransactionAnimationType animationType,
                            String tag) {
        String currentFragmentName = "";
        Fragment fragment = getCurrentFragment();
        if (fragment != null)
            currentFragmentName = fragment.getClass().toString();
        if (f.getClass().toString().equalsIgnoreCase(currentFragmentName))
            return;

        if (!mPaused) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();

            if (addToStack)
                transaction.addToBackStack(tag);

            if (animationType == FragmentTransactionAnimationType.FROM_RIGHT) {
                transaction.setCustomAnimations(R.anim.slide_in_right,
                        R.anim.slide_out_left, R.anim.slide_in_left,
                        R.anim.slide_out_right);
            } else if (animationType == FragmentTransactionAnimationType.FROM_BOTTOM) {
                transaction.setCustomAnimations(R.anim.slide_in_down, 0, 0,
                        R.anim.slide_out_down);
            } else if (animationType == FragmentTransactionAnimationType.FROM_RIGHT_TO_BOTTOM) {
                transaction.setCustomAnimations(R.anim.slide_in_right, 0, 0,
                        R.anim.slide_out_down);
            } else if (animationType == FragmentTransactionAnimationType.FROM_TOP) {
                transaction.setCustomAnimations(R.anim.slide_in_up, 0, 0,
                        R.anim.slide_out_up);
            } else if (animationType == FragmentTransactionAnimationType.FADE_IN) {
                transaction.setCustomAnimations(R.anim.fade_in, 0, 0,
                        R.anim.fade_out);
            } else {
                transaction
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
            }

            transaction.add(R.id.content_main, f);
            transaction.commit();
        } else {
            //Do not commit transaction as activity has lost it's state.
        }
    }

    public void clearFragmentBackStack() {
        FragmentManager manager = getSupportFragmentManager();
        if (manager.getBackStackEntryCount() > 0) {
            FragmentManager.BackStackEntry first = manager
                    .getBackStackEntryAt(0);
            manager.popBackStackImmediate(first.getId(),
                    FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    @Override
    public void onBackPressed() {


        Fragment f = getCurrentFragment();
        if (f != null && f instanceof BaseFragment && ((BaseFragment) f).onBackPressed()) {
            return;
        }
        FragmentManager manager = getSupportFragmentManager();
        // First entry in backstack is when MainActivity inserts HomepageFragment instance in onCreate.
        if (manager.getBackStackEntryCount() == 1) {
            // If that's the only entry left, finish the app.
            this.finish();
            return;
        }
        if (manager.getBackStackEntryCount() > 0) {
            // Pop all entries till a non POPUP state
            boolean popped = manager.popBackStackImmediate(TRANSACTION_DEFAULT,
                    0);
            if (popped)
                return;
        }
        super.onBackPressed();
    }

    public void popUptoFirst() {
        FragmentManager manager = getSupportFragmentManager();
        if (manager.getBackStackEntryCount() > 0) {
            FragmentManager.BackStackEntry firstFragmentEntry = manager
                    .getBackStackEntryAt(0);
            manager.popBackStackImmediate(firstFragmentEntry.getId(),
                    FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    public void popUptoNumber(int i) {
        FragmentManager manager = getSupportFragmentManager();
        if (manager.getBackStackEntryCount() > 0) {
            FragmentManager.BackStackEntry firstFragmentEntry = manager
                    .getBackStackEntryAt(manager.getBackStackEntryCount() - i);
            manager.popBackStackImmediate(firstFragmentEntry.getId(),
                    FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    public void popUpLast() {
        FragmentManager manager = getSupportFragmentManager();
        if (manager.getBackStackEntryCount() > 0) {
            FragmentManager.BackStackEntry firstFragmentEntry = manager
                    .getBackStackEntryAt(manager.getBackStackEntryCount() - 1);
            manager.popBackStackImmediate(firstFragmentEntry.getId(),
                    FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    public void popToTag(String tag) {
        FragmentManager manager = getSupportFragmentManager();
        manager.popBackStack(tag, 0);
    }

    public void addDialogFragment(DialogFragment dialogFragment, String TAG) {
        getSupportFragmentManager()
                .beginTransaction()
                .add(dialogFragment, TAG)
                .commit();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Nammu.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Fragment fragment = getCurrentFragment();
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public enum FragmentTransactionAnimationType {
        NONE, FROM_RIGHT, FROM_BOTTOM, FROM_TOP, FROM_RIGHT_TO_BOTTOM, FADE_IN
    }
}