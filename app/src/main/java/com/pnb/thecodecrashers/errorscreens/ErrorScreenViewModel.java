package com.pnb.thecodecrashers.errorscreens;

import android.graphics.drawable.Drawable;
import android.view.View;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.pnb.thecodecrashers.interfaces.OnErrorScreenCallback;

import java.util.HashMap;

import static com.pnb.thecodecrashers.interfaces.OnErrorScreenCallback.TYPE_RETRY;


/**
 * Created by Kishor on 5/9/17.
 */

public class ErrorScreenViewModel extends ViewModel {

    public static final String KEY_PROGRESS_DRAWABLE = "progressDrawable";
    public static final String KEY_HEADER_DRAWABLE = "headerDrawable";
    public static final String KEY_FIRST_MESSAGE = "firstMessage";
    public static final String KEY_SECOND_MESSAGE = "secondMessage";
    public static final String KEY_RETRY = "retry";
    public static final String KEY_LAYOUT_BACKGROUND = "layoutBackground";

    public MutableLiveData<Boolean> isVisible = new MutableLiveData<>();

    public MutableLiveData<Boolean> isProgressBarVisible = new MutableLiveData<>();
    public MutableLiveData<Drawable> progressBarDrawable = new MutableLiveData<>();

    public MutableLiveData<Boolean> isHeaderDrawableVisible = new MutableLiveData<>();
    public MutableLiveData<Drawable> headerDrawable = new MutableLiveData<>();

    public MutableLiveData<Boolean> isFirstMessageVisible = new MutableLiveData<>();
    public MutableLiveData<String> firstMessage = new MutableLiveData<>();

    public MutableLiveData<Boolean> isSecondMessageVisible = new MutableLiveData<>();
    public MutableLiveData<String> secondMessage = new MutableLiveData<>();

    public MutableLiveData<Boolean> isRetryVisible = new MutableLiveData<>();

    public MutableLiveData<Boolean> isBackgroundColor = new MutableLiveData<>();
    public MutableLiveData<Integer> backgroundColor = new MutableLiveData<>();

    private OnErrorScreenCallback mOnErrorScreenCallback;

    public ErrorScreenViewModel() {
    }

    public void setValues(HashMap<String, Object> values) {
        resetValues();
        if (values.containsKey(KEY_PROGRESS_DRAWABLE)) {
            isProgressBarVisible.setValue(true);
        }
        if (values.containsKey(KEY_HEADER_DRAWABLE)) {
            isHeaderDrawableVisible.setValue(true);
            headerDrawable.setValue((Drawable) values.get(KEY_HEADER_DRAWABLE));
        }
        if (values.containsKey(KEY_FIRST_MESSAGE)) {
            isFirstMessageVisible.setValue(true);
            firstMessage.setValue((String) values.get(KEY_FIRST_MESSAGE));
        }
        if (values.containsKey(KEY_SECOND_MESSAGE)) {
            isSecondMessageVisible.setValue(true);
            secondMessage.setValue((String) values.get(KEY_SECOND_MESSAGE));
        }

        if (values.containsKey(KEY_RETRY)) {
            isRetryVisible.setValue((Boolean) values.get(KEY_RETRY));
        }

        if (values.containsKey(KEY_LAYOUT_BACKGROUND)) {
            isBackgroundColor.setValue(true);
            backgroundColor.setValue((Integer) values.get(KEY_LAYOUT_BACKGROUND));
        }
    }

    public void resetValues() {
        isProgressBarVisible.setValue(false);
        progressBarDrawable.setValue(null);
        isHeaderDrawableVisible.setValue(false);
        headerDrawable.setValue(null);
        isFirstMessageVisible.setValue(false);
        firstMessage.setValue("");
        isSecondMessageVisible.setValue(false);
        secondMessage.setValue("");
        isRetryVisible.setValue(false);
    }

    public void setOnErrorScreenCallback(OnErrorScreenCallback onErrorScreenCallback) {
        mOnErrorScreenCallback = onErrorScreenCallback;
    }

    public void onRetryClick(View view) {
        if (mOnErrorScreenCallback != null)
            mOnErrorScreenCallback.errorScreenCallback(TYPE_RETRY);
    }

    public void show() {
        isVisible.setValue(true);
    }

    public void dismiss() {
        isVisible.setValue(false);
    }

}