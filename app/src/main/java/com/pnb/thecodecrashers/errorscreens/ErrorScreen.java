package com.pnb.thecodecrashers.errorscreens;

import android.content.Context;
import androidx.annotation.IntDef;
import androidx.appcompat.content.res.AppCompatResources;
import thecodecrashers.R;
import com.pnb.thecodecrashers.interfaces.OnErrorScreenCallback;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.HashMap;


/**
 * Created by Kishor on 5/8/17.
 */

public class ErrorScreen {

    public static final int TYPE_EMPTY_DATA = 1;
    public static final int TYPE_DATA_LOADING = 2;
    public static final int TYPE_ERROR = 3;
    public static final int TYPE_NETWORK_ERROR = 4;
    public static final int TYPE_SERVER_ERROR = 6;
    public static final int TYPE_EMPTY_SEARCH_RESULTS = 7;
    public static int ERROR_TYPE_VISIBLE;
    public ErrorScreenViewModel errorScreenViewModel;
    private Context mContext;
    private HashMap<String, Object> errorScreenValues = new HashMap<>();

    public ErrorScreen(Context context) {
        mContext = context;
        errorScreenViewModel = new ErrorScreenViewModel();
    }

    public ErrorScreen(Context context, OnErrorScreenCallback callback) {
        mContext = context;
        errorScreenViewModel = new ErrorScreenViewModel();
        errorScreenViewModel.setOnErrorScreenCallback(callback);
    }

    public void dismiss() {
        errorScreenViewModel.dismiss();
    }

    private void show() {
        errorScreenViewModel.setValues(errorScreenValues);
        errorScreenViewModel.show();
    }

    public void showType(@ScreenType int screenType) {
        ERROR_TYPE_VISIBLE = screenType;
        errorScreenValues.clear();
        switch (screenType) {
            case TYPE_EMPTY_DATA:
                errorScreenValues.put(ErrorScreenViewModel.KEY_HEADER_DRAWABLE, AppCompatResources.getDrawable(mContext, R.drawable.ic_loader_box));
                errorScreenValues.put(ErrorScreenViewModel.KEY_FIRST_MESSAGE, mContext.getString(R.string.txt_no_results));
                break;
            case TYPE_EMPTY_SEARCH_RESULTS:
                errorScreenValues.put(ErrorScreenViewModel.KEY_HEADER_DRAWABLE, AppCompatResources.getDrawable(mContext, R.drawable.ic_loader_box));
                errorScreenValues.put(ErrorScreenViewModel.KEY_FIRST_MESSAGE, mContext.getString(R.string.txt_search_no_results));
                break;
            case TYPE_DATA_LOADING:
                errorScreenValues.put(ErrorScreenViewModel.KEY_PROGRESS_DRAWABLE, true);
                errorScreenValues.put(ErrorScreenViewModel.KEY_FIRST_MESSAGE, mContext.getString(R.string.txt_loading));
                break;
            case TYPE_ERROR:
                errorScreenValues.put(ErrorScreenViewModel.KEY_HEADER_DRAWABLE, AppCompatResources.getDrawable(mContext, R.drawable.ic_loader_box));
                errorScreenValues.put(ErrorScreenViewModel.KEY_FIRST_MESSAGE, mContext.getString(R.string.err_fetching_data));
                errorScreenValues.put(ErrorScreenViewModel.KEY_RETRY, true);
                break;
            case TYPE_SERVER_ERROR:
                errorScreenValues.put(ErrorScreenViewModel.KEY_HEADER_DRAWABLE, AppCompatResources.getDrawable(mContext, R.drawable.ic_loader_box));
                errorScreenValues.put(ErrorScreenViewModel.KEY_FIRST_MESSAGE, mContext.getString(R.string.error_general_try_again));
                errorScreenValues.put(ErrorScreenViewModel.KEY_RETRY, true);
                break;
            case TYPE_NETWORK_ERROR:
                errorScreenValues.put(ErrorScreenViewModel.KEY_HEADER_DRAWABLE, AppCompatResources.getDrawable(mContext, R.drawable.ic_loader_box));
                errorScreenValues.put(ErrorScreenViewModel.KEY_FIRST_MESSAGE, mContext.getString(R.string.error_no_internet_connection));
                errorScreenValues.put(ErrorScreenViewModel.KEY_RETRY, true);
                break;
            default:
        }
        show();
    }

    public void showErrorMessage(String message, boolean showRetry) {
        errorScreenValues.clear();
        errorScreenValues.put(ErrorScreenViewModel.KEY_FIRST_MESSAGE, message);
        errorScreenValues.put(ErrorScreenViewModel.KEY_RETRY, showRetry);
        show();
    }

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({TYPE_EMPTY_DATA,
            TYPE_DATA_LOADING, TYPE_ERROR, TYPE_NETWORK_ERROR, TYPE_SERVER_ERROR,
            TYPE_EMPTY_SEARCH_RESULTS})
    public @interface ScreenType {
    }
}