package com.pnb.thecodecrashers.utils.bindingUtils;

import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.pnb.thecodecrashers.adapter.DataBindingRecyclerViewAdapter;


/**
 * Created by admin on 4/3/17.
 */

public class RecyclerViewBindingUtils {

    public final static String ADAPTER = "recyclerViewAdapter";
    public final static String ORIENTATION = "orientation";
    public final static String DIVIDER = "divider";

    @BindingAdapter({ADAPTER})
    public static void bindRecyclerViewAdapter(RecyclerView view, RecyclerView.Adapter adapter) {
        view.setAdapter(adapter);
    }

    @BindingAdapter({ADAPTER, ORIENTATION})
    public static void bindRecyclerAdapter(RecyclerView recyclerView, DataBindingRecyclerViewAdapter recyclerViewAdapter, int orientation) {
        recyclerView.setAdapter(recyclerViewAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), orientation, false));
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
    }

    @BindingAdapter({ADAPTER})
    public static void bindRecyclerAdapter(RecyclerView recyclerView, DataBindingRecyclerViewAdapter recyclerViewAdapter) {
        recyclerView.setAdapter(recyclerViewAdapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
    }

    @BindingAdapter({DIVIDER})
    public static void addDivider(RecyclerView recyclerView, RecyclerView.ItemDecoration itemDecoration) {
        recyclerView.addItemDecoration(itemDecoration);
    }

}
