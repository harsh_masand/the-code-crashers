package com.pnb.thecodecrashers.utils.bindingUtils;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.databinding.BindingAdapter;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.pnb.thecodecrashers.AppController;
import com.pnb.thecodecrashers.BaseActivity;
import com.sharedcode.widgets.CustomButton;
import com.sharedcode.widgets.CustomEditText;
import com.sharedcode.widgets.PinEntryView;
import com.sharedcode.widgets.StateRelativeLayout;

import java.util.List;

/**
 * Created by admin on 4/3/17.
 */

public class BindingUtils {
    public final static String EXPANDABLE_LIST_VIEW_ADAPTER = "expandableListViewAdapter";
    public static final String ADD_VIEWS = "addViews";
    private static final String VISIBILITY = "android:visibility";
    private static final String IN_VISIBILITY = "inVisibility";
    private static final String SELECTED = "selected";
    private static final String ENABLE = "enabled";
    private static final String ANIMATION = "animation";
    private static final String VISIBILITY_ANIMATION = "visibility_animation";
    private static final String START_ANIMATION = "startAnimation";
    private static final String ADD_TEXT_CHANGED_LISTENER = "addTextChangedListener";
    private static final String ADD_FOCUS_CHANGE_LISTENER = "addFocusChangedListener";
    private static final String ADD_EDITOR_ACTION_LISTENER = "addEditorActionListener";
    private static final String STATE_RELATIVE_VIEW_TYPE = "stateType";
    private static final String FRAGMENT = "fragment";
    private static final String VIEW_ID = "viewId";
    private static final String FOCUS_CHANGE_LISTENER = "focusChangeListener";
    private static final String ADD_TAB = "addTab";
    private static final String SET_ADAPTER = "setViewPagerAdapter";
    private static final String REQUEST_FOCUS = "request_focus";
    private static final String SET_SELECTION = "setSelection";
    private static final String SET_COLOR = "setColor";
    private static final String RESET_FOCUS = "resetFocus";
    private static final String IS_SELECTED = "isSelected";
    private static final String BACKGROUND = "background";
    private static final String TEXT_COLOR = "setTextColor";
    private static final String SET_CHECKED = "setChecked";
    private static final String SET_ERROR = "setError";
    private static final String SET_PROGRESS = "setProgress";


    @BindingAdapter({ADD_VIEWS})
    public static void addView(LinearLayout linearLayout, List<View> viewList) {
        linearLayout.removeAllViews();
        for (View view : viewList) {
            if (view.getParent() != null) {
                ((ViewGroup) view.getParent()).removeView(view); // <- fix
            }
            linearLayout.addView(view);
        }
    }

    @BindingAdapter({EXPANDABLE_LIST_VIEW_ADAPTER})
    public static void bindexpandableListViewAdapter(ExpandableListView view, BaseExpandableListAdapter adapter) {
        view.setAdapter(adapter);
    }

    @BindingAdapter({VISIBILITY, VISIBILITY_ANIMATION})
    public static void setAnimationVisibility(View view, boolean isVisible, Animation animation) {
        view.setAnimation(animation);
        view.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    @BindingAdapter({VISIBILITY})
    public static void setVisibility(View view, boolean isVisible) {
        view.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    @BindingAdapter(IN_VISIBILITY)
    public static void setInVisibility(View view, boolean isVisible) {
        view.setVisibility(isVisible ? View.VISIBLE : View.INVISIBLE);
    }

    @BindingAdapter(SELECTED)
    public static void setIsSelected(View view, boolean isSelected) {
        view.setSelected(isSelected);
    }

    @BindingAdapter(ENABLE)
    public static void setIsENABLE(View view, boolean isEnable) {
        view.setEnabled(isEnable);
    }

    @BindingAdapter({START_ANIMATION, ANIMATION})
    public static void setAnimation(View view, boolean isAnimation, Animation animation) {
        if (isAnimation) {
            view.startAnimation(animation);
        }
    }

    @BindingAdapter(SET_COLOR)
    public static void setColor(TextView textView, int res) {
        try {
            textView.setTextColor(ContextCompat.getColor(textView.getContext(), res));
        } catch (Resources.NotFoundException nfe) {
            nfe.printStackTrace();
        }
    }

    @BindingAdapter({ADD_TEXT_CHANGED_LISTENER})
    public static void addTextChangedListener(View view, TextWatcher watcher) {
        if (view instanceof EditText) {
            ((EditText) view).removeTextChangedListener(watcher);
            ((EditText) view).addTextChangedListener(watcher);
        } else if (view instanceof PinEntryView) {
            ((PinEntryView) view).addTextChangedListener(watcher);
        }
    }

    @BindingAdapter(ADD_FOCUS_CHANGE_LISTENER)
    public static void addFocusChangeListner(View view, View.OnFocusChangeListener onFocusChangeListener) {
        ((EditText) view).setOnFocusChangeListener(onFocusChangeListener);
    }

    @BindingAdapter({ADD_EDITOR_ACTION_LISTENER})
    public static void addEditorActionListener(View view, EditText.OnEditorActionListener editorActionListener) {
        if (view instanceof CustomEditText) {
            ((CustomEditText) view).setOnEditorActionListener(editorActionListener);
        } else if (view instanceof EditText) {
            ((EditText) view).setOnEditorActionListener(editorActionListener);
        }
    }

    @BindingAdapter({STATE_RELATIVE_VIEW_TYPE})
    public static void displayState(StateRelativeLayout stateRelativeLayout, @StateRelativeLayout.StateType int stateType) {
        stateRelativeLayout.displayStateView(stateType);
    }

    @BindingAdapter({VIEW_ID, FRAGMENT})
    public static void setFragment(FrameLayout frameLayout, int viewId, Fragment fragment) {
        if (fragment != null)
            ((BaseActivity) frameLayout.getContext()).getSupportFragmentManager().beginTransaction().replace(viewId, fragment).commit();
    }

    @BindingAdapter({FOCUS_CHANGE_LISTENER})
    public static void setFocusChangeListener(EditText editText, EditText.OnFocusChangeListener focusChangeListener) {
        editText.setOnFocusChangeListener(focusChangeListener);
    }

    @BindingAdapter(ADD_TAB)
    public static void addTab(TabLayout tabLayout, List<String> tabs) {
        if (tabs != null)
            for (String tab : tabs) {
                tabLayout.addTab(tabLayout.newTab().setText(tab));
            }
    }

    @BindingAdapter(SET_ADAPTER)
    public static void setAdapter(ViewPager viewPager, FragmentStatePagerAdapter fragmentStatePagerAdapter) {
        if (fragmentStatePagerAdapter != null)
            viewPager.setAdapter(fragmentStatePagerAdapter);
    }


    @BindingAdapter({REQUEST_FOCUS})
    public static void bindRequestFocus(TextInputEditText editText, boolean isFocusRequested) {
        if (isFocusRequested) {
            editText.requestFocus();
            editText.setSelection(editText.getText().length());
            InputMethodManager imm = (InputMethodManager) AppController.getInstance().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    @BindingAdapter({SET_SELECTION})
    public static void setSelection(View view, int position) {
        if (view instanceof EditText) {
            EditText editText = (EditText) view;
            if (editText.getText().length() > position) {
                editText.setSelection(position);
            } else {
                editText.setSelection(editText.getText().length());
            }
        }
    }


    @BindingAdapter({SET_SELECTION})
    public static void setSelection(CustomButton customButton, boolean isSelected) {
        customButton.setSelected(isSelected);
    }

    @BindingAdapter({TEXT_COLOR})
    public static void bindTextColor(Button button, Integer resId) {
        try {
            if (resId != null) {
                button.setTextColor(button.getContext().getResources().getColor(resId));
            }
        } catch (Resources.NotFoundException nfe) {
            nfe.printStackTrace();
        }
    }

    @BindingAdapter({BACKGROUND})
    public static void bindBackgroud(View view, int res) {
        view.setBackgroundResource(res);
    }

    @BindingAdapter(RESET_FOCUS)
    public static void resetFocus(EditText editText, boolean isFocusReset) {
        if (isFocusReset) {
            editText.clearFocus();
        }
    }

    @BindingAdapter({IS_SELECTED})
    public static void setSelected(View view, Boolean isSelected) {
        view.setSelected(isSelected);
    }

    @BindingAdapter({SET_CHECKED})
    public static void setChecked(CompoundButton compoundButton, boolean isChecked) {
        compoundButton.setChecked(isChecked);
    }

    @BindingAdapter({SET_ERROR})
    public static void setError(TextInputLayout textInputLayout, String error) {
        if (!TextUtils.isEmpty(error)) {
            textInputLayout.setError(error);
        }
    }

    @BindingAdapter({SET_PROGRESS})
    public static void setProgress(ProgressBar view, int marginLeft) {
        view.setProgress(marginLeft);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private static void revealAnimation(View view, int cx, int cy, long duration) {
        view.setVisibility(View.VISIBLE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            float radius = Math.max(view.getWidth(), view.getHeight()) * 2.0f;
            Animator reveal = ViewAnimationUtils.createCircularReveal(view, cx, cy, 0f, radius);
            reveal.setDuration(duration);
            reveal.start();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private static void revealBackAnimation(final View view, int cx, int cy, long duration) {
        if (view.getVisibility() == View.VISIBLE && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            float radius = Math.max(view.getWidth(), view.getHeight()) * 2.0f;
            Animator reveal = ViewAnimationUtils.createCircularReveal(view, cx, cy, radius, 0f);
            reveal.setDuration(duration);
            reveal.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    view.setVisibility(View.INVISIBLE);
                }
            });
            reveal.start();
        } else {
            view.setVisibility(View.INVISIBLE);
        }
    }
}
