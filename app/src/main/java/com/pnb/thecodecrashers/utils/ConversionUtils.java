package com.pnb.thecodecrashers.utils;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class ConversionUtils {

    public static <T extends Enum<T>> T stringToEnum(Class<T> enumClass,
                                                     String value) {
        if (value == null)
            return null;
        try {
            return Enum.valueOf(enumClass, value.toUpperCase());
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        return null;
    }

    // Creates comma separated string
    public static String stringListToString(List<String> stringList) {
        String result = "";
        for (int i = 0; i < stringList.size(); i++) {
            if (i > 0)
                result += ",";
            result += stringList.get(i);
        }
        return result;
    }

    // Parses navigatedFrom string
    public static List<String> stringToStringList(String str) {
        List<String> stringList = new ArrayList<String>();
        if (str != null) {
            String[] arr = str.split(",");
            for (String a : arr) {
                if (!a.equals(""))
                    stringList.add(a);
            }
        }
        return stringList;
    }

    // Creates comma separated string
    public static String jsonArrayToString(JSONArray arr) {
        String result = "";
        for (int i = 0; i < arr.length(); i++) {
            if (i > 0)
                result += ",";
            try {
                result += arr.getString(i);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    // Creates jsonArray
    public static JSONArray stringListToJsonArray(List<String> stringList) {
        JSONArray arr = new JSONArray();
        for (String str : stringList) {
            arr.put(str);
        }
        return arr;
    }

    public static List<String> jsonArrayToStringList(JSONArray jsonArray)
            throws JSONException {
        List<String> stringList = new ArrayList<String>();
        for (int i = 0; i < jsonArray.length(); i++) {
            stringList.add(jsonArray.getString(i));
        }
        return stringList;
    }
}
