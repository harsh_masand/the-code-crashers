package com.pnb.thecodecrashers.utils.bindingUtils;

import android.content.res.Resources;
import android.graphics.Paint;
import android.text.*;
import android.text.style.ForegroundColorSpan;
import android.widget.TextView;
import androidx.core.content.ContextCompat;
import androidx.databinding.BindingAdapter;
import thecodecrashers.R;
import com.sharedcode.widgets.CustomImageButtonIconRight;
import com.sharedcode.widgets.CustomTextView;

/**
 * Created by admin on 4/3/17.
 */

public class TextViewBindingUtils {

    public final static String TEXT_COLOR = "setTextColor";
    public final static String TEXT_COLOR_STATE = "colorState";
    public static final String SET_HTML_TEXT = "setHtmlText";
    public static final String SET_SPAN_POSITION = "setSpanPosition";
    private static final String DRAWABLE_WIDTH = "drawableWidth";
    private static final String DRAWABLE_HEIGHT = "drawableHeight";
    private static final String SET_TEXT = "setText";
    private static final String STRIKE_OUT = "strikeOut";
    private static final String SET_SPANNED_TEXT = "setSpannedText";
    private static final String SPANNABLE_TEXT = "spannableText";
    private static final String UNDERLINE_BENEATH = "setUnderline";

    @BindingAdapter(TEXT_COLOR)
    public static void setTextColor(TextView textView, int resource) {
        textView.setTextColor(ContextCompat.getColor(textView.getContext(), resource));
    }

    @BindingAdapter({TEXT_COLOR_STATE})
    public static void bindingTextColor(TextView textView, int resource) {
        try {
            textView.setTextColor(ContextCompat.getColor(textView.getContext(), resource));
        } catch (Resources.NotFoundException nfe) {
            nfe.printStackTrace();
        }
    }

    @BindingAdapter({DRAWABLE_WIDTH, DRAWABLE_HEIGHT})
    public static void bindImageSrc(TextView textView, Integer width, Integer height) {
        if (textView instanceof CustomTextView) {
            ((CustomTextView) textView).scaleDrawable(width, height);
        }
    }

    @BindingAdapter({SET_TEXT})
    public static void setText(CustomImageButtonIconRight customImageButtonIconRight, String text) {
        customImageButtonIconRight.setText(text);
    }

    @BindingAdapter({STRIKE_OUT})
    public static void setStrikeOut(TextView textView, boolean canStrikeOut) {
        if (canStrikeOut) {
            textView.setPaintFlags(textView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }
    }

    @BindingAdapter({SET_TEXT, SET_SPAN_POSITION})
    public static void setSpanText(CustomTextView customTextView, String text, int position) {
        if (!TextUtils.isEmpty(text)) {
            Spannable wordtoSpan = new SpannableString(text);
            wordtoSpan.setSpan(new ForegroundColorSpan(customTextView.getContext().getResources().getColor(R.color.watermelon)), 0, position, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            customTextView.setText(wordtoSpan);
        }
    }

    @BindingAdapter({SET_SPANNED_TEXT})
    public static void setSpannedText(CustomTextView customTextView, SpannableString spanned) {
        if (spanned != null) {
            customTextView.setText(spanned);
        }
    }

    @BindingAdapter(SET_HTML_TEXT)
    public static void setTextColor(CustomTextView customTextView, String text) {
        if (!TextUtils.isEmpty(text)) {
            customTextView.setText(Html.fromHtml(text));
        }
    }

    @BindingAdapter(SET_HTML_TEXT)
    public static void setTextColor(CustomTextView customTextView, int resId) {
        if (!TextUtils.isEmpty(customTextView.getContext().getString(resId))) {
            customTextView.setText(Html.fromHtml(customTextView.getContext().getString(resId)));
        }
    }

    @BindingAdapter({SPANNABLE_TEXT})
    public static void setSpannableText(CustomTextView textView, SpannableStringBuilder spannableString) {
        textView.setText(spannableString);
    }

    @BindingAdapter({UNDERLINE_BENEATH})
    public static void setLineBeneath(CustomTextView textView, boolean setUnderline) {
        if (setUnderline) {
            textView.setPaintFlags(textView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        }
    }

}
