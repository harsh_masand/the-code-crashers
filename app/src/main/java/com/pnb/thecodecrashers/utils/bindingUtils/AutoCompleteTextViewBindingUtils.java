package com.pnb.thecodecrashers.utils.bindingUtils;

import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import androidx.databinding.BindingAdapter;
import com.pnb.thecodecrashers.adapter.SpinnerAdapter;
import com.sharedcode.widgets.CustomAutoCompleteTextView;

/**
 * Created by Vishal on 01/02/17.
 */

public class AutoCompleteTextViewBindingUtils {
    public static final String SET_ADAPTER = "adapter";
    public static final String SET_ITEM_CLICK_LISTENER = "itemClickListener";
    public static final String SET_TEXT_WATCHER = "textWatcher";
    public static final String SET_ON_TOUCH_LISTENER = "onTouchListener";
    private static final String TAG = "CustomAutoCompleteTextViewUtils";

    @BindingAdapter({SET_ADAPTER})
    public static void bindAdapter(CustomAutoCompleteTextView customAutoCompleteTextView, SpinnerAdapter spinnerAdapter) {
        customAutoCompleteTextView.setAdapter(spinnerAdapter);
    }

    @BindingAdapter({SET_ITEM_CLICK_LISTENER})
    public static void bindItemClickListener(CustomAutoCompleteTextView customAutoCompleteTextView, AdapterView.OnItemClickListener OnItemClickListener) {
        if (OnItemClickListener != null)
            customAutoCompleteTextView.setOnItemClickListener(OnItemClickListener);
    }

    @BindingAdapter({SET_TEXT_WATCHER})
    public static void bindTextWatcher(CustomAutoCompleteTextView customAutoCompleteTextView, TextWatcher textWatcher) {
        if (textWatcher != null)
            customAutoCompleteTextView.addTextChangedListener(textWatcher);
    }

    @BindingAdapter({SET_ON_TOUCH_LISTENER})
    public static void bindTextWatcher(CustomAutoCompleteTextView customAutoCompleteTextView, View.OnTouchListener touchListener) {
        if (touchListener != null)
            customAutoCompleteTextView.setOnTouchListener(touchListener);
    }
}
