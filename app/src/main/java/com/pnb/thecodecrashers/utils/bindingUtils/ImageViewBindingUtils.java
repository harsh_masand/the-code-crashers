package com.pnb.thecodecrashers.utils.bindingUtils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableField;
import com.pnb.thecodecrashers.utils.RoundedCornersTransform;
import com.sharedcode.utils.AppUtils;
import com.sharedcode.widgets.CustomImageButtonIconRight;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

/**
 * Created by admin on 4/10/17.
 */

public class ImageViewBindingUtils {

    public static final String SHOP_IMAGE_URL = "shopImageUrl";
    private static final String IMAGE_URL = "imageUrl";
    private static final String IMAGE_URL_WITH_CURVE = "imageUrlWithCurve";
    private static final String PLACE_HOLDER = "placeHolder";
    private static final String SRC = "src";
    private static final String ANDROID_SRC = "android:src";
    private static final String LOAD_BIT_MAP = "loadBitMap";
    private static final String SET_ICON = "setIcon";
    private static final String CONVERT_TO_GRY = "convertToGery";
    private static final String INVALIDATE_CACHE = "invalidateCache";

    private static final int SHOP_IMAGE_SIZE = 60;


    @BindingAdapter({IMAGE_URL, PLACE_HOLDER})
    public static void loadCircleImage(ImageView view, String url, int placeholder) {
        if (!TextUtils.isEmpty(url)) {
            Picasso.with(view.getContext()).load(url).placeholder(placeholder).into(view);
        } else {
            Picasso.with(view.getContext()).load("save").placeholder(placeholder).into(view);
        }
    }

    @BindingAdapter({IMAGE_URL, INVALIDATE_CACHE})
    public static void loadImage(ImageView view, String url, boolean isInValidateCache) {
        if (!TextUtils.isEmpty(url)) {
            if (!isInValidateCache) {
                Picasso.with(view.getContext()).load(url).into(view);
            } else {
                Picasso.with(view.getContext()).load(url).memoryPolicy(MemoryPolicy.NO_CACHE)
                        .networkPolicy(NetworkPolicy.NO_CACHE).into(view);
            }
        } else {
            Picasso.with(view.getContext()).load("save").into(view);
        }
    }

    @BindingAdapter({IMAGE_URL, PLACE_HOLDER, INVALIDATE_CACHE})
    public static void loadImage(ImageView view, String url, int placeholder, boolean isInValidateCache) {
        if (!TextUtils.isEmpty(url)) {
            if (!isInValidateCache) {
                Picasso.with(view.getContext()).load(url).placeholder(placeholder).into(view);
            } else {
                Picasso.with(view.getContext()).load(url)
                        .networkPolicy(NetworkPolicy.NO_CACHE).placeholder(placeholder).into(view);
            }
        } else {
            Picasso.with(view.getContext()).load("save").placeholder(placeholder).into(view);
        }
    }

    @BindingAdapter({IMAGE_URL_WITH_CURVE, PLACE_HOLDER})
    public static void loadCurveImage(ImageView view, String url, int placeholder) {
        if (!TextUtils.isEmpty(url)) {
            int px = AppUtils.convertDpToPixel(80, view.getContext());
            Picasso.with(view.getContext())
                    .load(url)
                    .resize(px, px)
                    .transform(new RoundedCornersTransform(10, 10))
                    .placeholder(placeholder)
                    .into(view);
        } else {
            Picasso.with(view.getContext()).load("save").placeholder(placeholder).into(view);
        }
    }

    @BindingAdapter({IMAGE_URL})
    public static void loadImage(ImageView view, String url) {
        if (!TextUtils.isEmpty(url))
            Picasso.with(view.getContext()).load(url).into(view);
    }

    @BindingAdapter({SRC})
    public static void bindImageSrc(ImageView imageView, Integer res) {
        if (res == null) {
            imageView.setImageResource(0);
        } else {
            imageView.setImageResource(res);
        }
    }

    @BindingAdapter({LOAD_BIT_MAP})
    public static void loadBitMap(ImageView imageView, Bitmap bitmap) {
        imageView.setImageBitmap(bitmap);
    }

    @BindingAdapter({SET_ICON})
    public static void setIcon(CustomImageButtonIconRight customImageButtonIconRight, Drawable img) {
        if (img != null)
            customImageButtonIconRight.setIcon(img);
    }

    @BindingAdapter({SET_ICON})
    public static void setIcon(ImageView view, Integer drawable) {
        view.setImageResource(drawable);
    }

    @BindingAdapter({ANDROID_SRC})
    public static void bindImageSrc(ImageView imageView, Bitmap bitmap) {
        if (bitmap != null)
            imageView.setImageBitmap(bitmap);
    }

    @BindingAdapter({ANDROID_SRC})
    public static void bindImageSrc(ImageView imageView, ObservableField<Bitmap> bitmap) {
        if (bitmap == null) {
            imageView.setImageBitmap(null);
            imageView.setVisibility(View.GONE);
        } else {
            imageView.setImageBitmap(bitmap.get());
            imageView.setVisibility(bitmap.get() == null ? View.GONE : View.VISIBLE);

        }
    }

    @BindingAdapter({CONVERT_TO_GRY})
    public static void convertToGray(ImageView imageView, boolean convertToGry) {
        if (convertToGry) {
            ColorMatrix matrix = new ColorMatrix();
            matrix.setSaturation(0);

            ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
            imageView.setColorFilter(filter);
        } else {
            ColorMatrix matrix = new ColorMatrix();
            matrix.setSaturation(1);

            ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
            imageView.setColorFilter(filter);
        }
    }

    @BindingAdapter({SHOP_IMAGE_URL, PLACE_HOLDER})
    public static void resizeShopImage(ImageView imageView, String imageUrl, int placeHolderId) {
        Context context = imageView.getContext();
        imageView.getLayoutParams().height = dpToPx(context, SHOP_IMAGE_SIZE);
        imageView.getLayoutParams().width = dpToPx(context, SHOP_IMAGE_SIZE);
        imageView.requestLayout();
        if (!TextUtils.isEmpty(imageUrl)) {
            Picasso.with(context).load(imageUrl).error(placeHolderId).resize(dpToPx(context, SHOP_IMAGE_SIZE), dpToPx(context, SHOP_IMAGE_SIZE)).centerCrop().into(imageView);
        } else {
            Picasso.with(context).load("save").placeholder(placeHolderId).into(imageView);
        }
    }

    public static int dpToPx(Context context, int dp) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }
}
