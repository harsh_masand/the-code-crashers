package com.pnb.thecodecrashers.utils.bindingUtils;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import androidx.databinding.BindingAdapter;

/**
 * Created by admin on 4/21/17.
 */

public class SpinnerViewBindingUtils {

    public final static String SPINNER_ADAPTER = "spinnerAdapter";
    public final static String ITEM_LISTENER = "itemListener";
    public final static String SET_ITEM = "setItem";

    @BindingAdapter({SPINNER_ADAPTER})
    public static void bindSpinnerAdapter(Spinner spinner, ArrayAdapter adapter) {
        spinner.setAdapter(adapter);
    }

    @BindingAdapter({SET_ITEM})
    public static void setItem(Spinner spinner, int position) {
        spinner.setSelection(position);
    }

    @BindingAdapter({ITEM_LISTENER})
    public static void bindSpinnerAdapter(Spinner spinner, AdapterView.OnItemSelectedListener listener) {
        if (listener != null) {
            spinner.setOnItemSelectedListener(listener);
        }
    }

}
